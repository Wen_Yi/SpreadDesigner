﻿using System.Windows.Forms;
using System;
namespace SpreadDesigner
{
  partial class SpreadDesigner
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpreadDesigner));
            this.c1Ribbon1 = new C1.Win.C1Ribbon.C1Ribbon();
            this.ribbonApplicationMenu1 = new C1.Win.C1Ribbon.RibbonApplicationMenu();
            this.ribbonConfigToolBar1 = new C1.Win.C1Ribbon.RibbonConfigToolBar();
            this.ribbonQat1 = new C1.Win.C1Ribbon.RibbonQat();
            this.tbnUndo = new C1.Win.C1Ribbon.RibbonButton();
            this.tbnRedo = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonSeparator22 = new C1.Win.C1Ribbon.RibbonSeparator();
            this.tbnNew = new C1.Win.C1Ribbon.RibbonButton();
            this.tbnOpen = new C1.Win.C1Ribbon.RibbonButton();
            this.tbnSave = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonSeparator21 = new C1.Win.C1Ribbon.RibbonSeparator();
            this.tbnPrint = new C1.Win.C1Ribbon.RibbonButton();
            this.rtHome = new C1.Win.C1Ribbon.RibbonTab();
            this.rpClipboard = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbPaste = new C1.Win.C1Ribbon.RibbonSplitButton();
            this.rbPasteAll = new C1.Win.C1Ribbon.RibbonButton();
            this.rbPasteValues = new C1.Win.C1Ribbon.RibbonButton();
            this.rbPasteFormatting = new C1.Win.C1Ribbon.RibbonButton();
            this.rbPasteFormulas = new C1.Win.C1Ribbon.RibbonButton();
            this.rbPasteAsLink = new C1.Win.C1Ribbon.RibbonButton();
            this.rbPasteAsString = new C1.Win.C1Ribbon.RibbonButton();
            this.rbPasteAsShape = new C1.Win.C1Ribbon.RibbonButton();
            this.rbPasteAsChart = new C1.Win.C1Ribbon.RibbonButton();
            this.rbCut = new C1.Win.C1Ribbon.RibbonButton();
            this.rbCopy = new C1.Win.C1Ribbon.RibbonButton();
            this.rpFont = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonToolBar3 = new C1.Win.C1Ribbon.RibbonToolBar();
            this.rcbFont = new C1.Win.C1Ribbon.RibbonFontComboBox();
            this.rcbFontSize = new C1.Win.C1Ribbon.RibbonComboBox();
            this.rbIncreaseFontSize = new C1.Win.C1Ribbon.RibbonButton();
            this.rbDecreaseFontSize = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonToolBar2 = new C1.Win.C1Ribbon.RibbonToolBar();
            this.rbBold = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbItalic = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbUnderLine = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.ribbonSeparator1 = new C1.Win.C1Ribbon.RibbonSeparator();
            this.ribbonSeparator2 = new C1.Win.C1Ribbon.RibbonSeparator();
            this.rccFillColor = new C1.Win.C1Ribbon.RibbonColorPicker();
            this.rccFontColor = new C1.Win.C1Ribbon.RibbonColorPicker();
            this.rbCellBorder = new C1.Win.C1Ribbon.RibbonMenu();
            this.rbBottomBorder = new C1.Win.C1Ribbon.RibbonButton();
            this.rbTopBorder = new C1.Win.C1Ribbon.RibbonButton();
            this.rbLeftBorder = new C1.Win.C1Ribbon.RibbonButton();
            this.rbRightBorder = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonSeparator11 = new C1.Win.C1Ribbon.RibbonSeparator();
            this.rbNoBorder = new C1.Win.C1Ribbon.RibbonButton();
            this.rbAllBorder = new C1.Win.C1Ribbon.RibbonButton();
            this.rbOutsideBorder = new C1.Win.C1Ribbon.RibbonButton();
            this.rbThickBoxBorder = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonSeparator17 = new C1.Win.C1Ribbon.RibbonSeparator();
            this.rbBottomDoubleBorder = new C1.Win.C1Ribbon.RibbonButton();
            this.rbThickBottomBorder = new C1.Win.C1Ribbon.RibbonButton();
            this.rbTopAndBottomBorder = new C1.Win.C1Ribbon.RibbonButton();
            this.rbTopAndThickBottomBorder = new C1.Win.C1Ribbon.RibbonButton();
            this.rbTopAndDoubleBottomBorder = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonSeparator18 = new C1.Win.C1Ribbon.RibbonSeparator();
            this.rbMoreBorder = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonToolBar1 = new C1.Win.C1Ribbon.RibbonToolBar();
            this.ribbonToolBar5 = new C1.Win.C1Ribbon.RibbonToolBar();
            this.ribbonToolBar6 = new C1.Win.C1Ribbon.RibbonToolBar();
            this.rpAlignment = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonToolBar4 = new C1.Win.C1Ribbon.RibbonToolBar();
            this.rbTopAlign = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbMiddleAlign = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbBottomAlign = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbVerticalJustify = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbVerticalDistributed = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.ribbonToolBar7 = new C1.Win.C1Ribbon.RibbonToolBar();
            this.rbAlignTextLeft = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbAlignTextMiddle = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbAlignTextRight = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbHorizontalJustify = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbHorizontalDistributed = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbDecreaseIndent = new C1.Win.C1Ribbon.RibbonButton();
            this.rbIncreaseIndent = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonSeparator6 = new C1.Win.C1Ribbon.RibbonSeparator();
            this.rbWrapText = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbMerge = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rpCellType = new C1.Win.C1Ribbon.RibbonGroup();
            this.rcbCellType = new C1.Win.C1Ribbon.RibbonGallery();
            this.rbdwGeneral = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbdwText = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbdwNumber = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbdwPercent = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbdwCurrency = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbdwRegularExpression = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbdwBarCode = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbdwButton = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbdwCheckBox = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbdwMultiOption = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbdwComboBox = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbdwMultiColumnComboBox = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbdwListBox = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbdwRichText = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbdwDateTime = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbdwPicture = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbdwColorPicker = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbdwProgress = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbdwSlider = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbdwMask = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbdwHyperLink = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbdwGcDate = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbdwGcTextBox = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbClearCellType = new C1.Win.C1Ribbon.RibbonButton();
            this.rpStyle = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbConditionalFormat = new C1.Win.C1Ribbon.RibbonSplitButton();
            this.rbCFHighLightCellsRules = new C1.Win.C1Ribbon.RibbonMenu();
            this.rbGreaterThan = new C1.Win.C1Ribbon.RibbonButton();
            this.rbLessThan = new C1.Win.C1Ribbon.RibbonButton();
            this.rbBetween = new C1.Win.C1Ribbon.RibbonButton();
            this.rbCFEqualTo = new C1.Win.C1Ribbon.RibbonButton();
            this.rbCFTextThatContains = new C1.Win.C1Ribbon.RibbonButton();
            this.rbCFADateOccurring = new C1.Win.C1Ribbon.RibbonButton();
            this.rbCFDuplicateValues = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonSeparator20 = new C1.Win.C1Ribbon.RibbonSeparator();
            this.rbCFHighLightMoreRules = new C1.Win.C1Ribbon.RibbonButton();
            this.rbCFTopBottomRules = new C1.Win.C1Ribbon.RibbonMenu();
            this.rbCFTop10Items = new C1.Win.C1Ribbon.RibbonButton();
            this.rbCFTop10Per = new C1.Win.C1Ribbon.RibbonButton();
            this.rbCFBottom10Items = new C1.Win.C1Ribbon.RibbonButton();
            this.rbCFBottom10Per = new C1.Win.C1Ribbon.RibbonButton();
            this.rbCFAboveAverage = new C1.Win.C1Ribbon.RibbonButton();
            this.rbCFBelowAverage = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonSeparator9 = new C1.Win.C1Ribbon.RibbonSeparator();
            this.rbCFTopBottomMoreRules = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonSeparator8 = new C1.Win.C1Ribbon.RibbonSeparator();
            this.rbCFDataBars = new C1.Win.C1Ribbon.RibbonGallery();
            this.rbCFGradientBlue = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCFGradientGreen = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCFGradientRed = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCFGradientOrange = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCFGradientLightBlue = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCFGradientPurple = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCFSolidBlue = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCFSolidGreen = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCFSolidRed = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCFSolidOrange = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCFSolidLightBlue = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCFSolidPurple = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCFDataBarMoreRules = new C1.Win.C1Ribbon.RibbonButton();
            this.rbCFColorScales = new C1.Win.C1Ribbon.RibbonGallery();
            this.rbCFGreenYellowRed = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ItemRedYellowGreenScale = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ItemGreenWhiteRedScale = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ItemRedWhiteGreenScale = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ItemBlueWhiteRedScale = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ItemRedWhiteBlueScale = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ItemWhiteRedScale = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ItemRedWhiteScale = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ItemGreenWihte = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ItemWhiteGreen = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ItemGreenYellowScale = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ItemYellowGreenScale = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCFColorScaleMoreRule = new C1.Win.C1Ribbon.RibbonButton();
            this.rbCFIconSet = new C1.Win.C1Ribbon.RibbonGallery();
            this.rbCF3ArrowsColored = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCF3ArrowsGray = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCF3Triangles = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCF4ArrowsGray = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCF4ArrowsColored = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCF5ArrowsGray = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCF5Colored = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ribbonGalleryItem113 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCF3TrafficLightsUnRimmmed = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCF3TrafficLightsRimmmed = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCF3Sign = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCF4TracfficLightsUnRimmed = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCFRedToBlack = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ribbonGalleryItem122 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCF3SymbolsCircled = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCF3SymbolsUnCircled = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCF3Flags = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ribbonGalleryItem126 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCFStars = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCF4Ratings = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCFQuarsters = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCF5Ratings = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCF5Boxs = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbCFIconSetsMoreRule = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonSeparator10 = new C1.Win.C1Ribbon.RibbonSeparator();
            this.rbCFNewRule = new C1.Win.C1Ribbon.RibbonButton();
            this.rbCFClearRules = new C1.Win.C1Ribbon.RibbonMenu();
            this.rbCFClearFromSelectedCell = new C1.Win.C1Ribbon.RibbonButton();
            this.rbCFClearEntireSheet = new C1.Win.C1Ribbon.RibbonButton();
            this.rbCFManagerRules = new C1.Win.C1Ribbon.RibbonButton();
            this.rpEditing = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbLock = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbClearAll = new C1.Win.C1Ribbon.RibbonButton();
            this.rbSelectAll = new C1.Win.C1Ribbon.RibbonSplitButton();
            this.rbSelectAllSheet = new C1.Win.C1Ribbon.RibbonButton();
            this.rbSelectAllCells = new C1.Win.C1Ribbon.RibbonButton();
            this.rbSelectAllData = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonSeparator7 = new C1.Win.C1Ribbon.RibbonSeparator();
            this.rbSortFilter = new C1.Win.C1Ribbon.RibbonSplitButton();
            this.rbSortAToZ = new C1.Win.C1Ribbon.RibbonButton();
            this.rbSortZToA = new C1.Win.C1Ribbon.RibbonButton();
            this.rbCustomSort = new C1.Win.C1Ribbon.RibbonButton();
            this.rbFind2 = new C1.Win.C1Ribbon.RibbonSplitButton();
            this.rbFind = new C1.Win.C1Ribbon.RibbonButton();
            this.rbGoto = new C1.Win.C1Ribbon.RibbonButton();
            this.rtInsert = new C1.Win.C1Ribbon.RibbonTab();
            this.rpTables = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbTable = new C1.Win.C1Ribbon.RibbonButton();
            this.rpText = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbSymbol = new C1.Win.C1Ribbon.RibbonButton();
            this.rbWordArt = new C1.Win.C1Ribbon.RibbonButton();
            this.rplllustrations = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbPicture = new C1.Win.C1Ribbon.RibbonButton();
            this.rbShapes = new C1.Win.C1Ribbon.RibbonGallery();
            this.Shape11 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape12 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ribbonGalleryItem143 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ribbonGalleryItem144 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ribbonGalleryItem145 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ribbonGalleryItem146 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ribbonGalleryItem147 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape21 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape22 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape23 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape24 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape25 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape26 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape27 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape31 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape32 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape33 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape34 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape35 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape36 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape37 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape41 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape42 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape43 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape44 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ribbonGalleryItem165 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ribbonGalleryItem166 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ribbonGalleryItem167 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape51 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape52 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape53 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape54 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape55 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape56 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape57 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape61 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape62 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape63 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape64 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Shape65 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbAnnotactionMode = new C1.Win.C1Ribbon.RibbonButton();
            this.rpChart = new C1.Win.C1Ribbon.RibbonGroup();
            this.RibbonButtonChartColumn = new C1.Win.C1Ribbon.RibbonGallery();
            this.ClusteredColumnChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.StackedColumnChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Stacked100ColumnChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.HighLowColumnChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ClusteredColumnIn3DChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.StackedColumnIn3DChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Stacked100ColumnIn3DChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ColumnIn3DChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ClusteredCylinderChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.StackedCylinderChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Stacked100CylinderChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.CylinderIn3DChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ClusteredFullConeChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.StackedFullConeChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Stacked100FullConeChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.FullConeIn3DChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ClusteredFullPyramidChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.StackedFullPyramidChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Stacked100FullPyramidChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.FullPyramidIn3DChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.RibbonButton10 = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButtonChartLine = new C1.Win.C1Ribbon.RibbonGallery();
            this.LineIn2DChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.StackedLineChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Stacked100LineChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.LineWithMarkersChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.StackedLineWithMarkersChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Stacked100LineWithMarkersChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.LineIn3DChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.RibbonButton11 = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButtonChartPie = new C1.Win.C1Ribbon.RibbonGallery();
            this.PieIn2DChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ExplodedPieIn2DChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.PieIn3DChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ExplodedPieIn3DChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.RibbonButton12 = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButtonChartBar = new C1.Win.C1Ribbon.RibbonGallery();
            this.ClusteredBarChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.StackedBarChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Stacked100BarChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ClusteredBarIn3DChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.StackedBarIn3DChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Stacked100BarIn3DChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ClusteredHorizontalCylinderChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.StackedHorizontalCylinderChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Stacked100HorizontalCylinderChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ClusteredHorizontalFullConeChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.StackedHorizontalFullConeChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Stacked100HorizontalFullConeChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ClusteredHorizontalFullPyramidChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Stacked100HorizontalFullPyramidChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.HighLowBarFullPyramidChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.RibbonButton13 = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButtonChartArea = new C1.Win.C1Ribbon.RibbonGallery();
            this.AreaIn2DChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.StackedAreaChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Stacked100AreaChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.AreaIn3DChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.StackedAreaIn3DChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.Stacked100AreaIn3DChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.RibbonButton14 = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButtonChartScatter = new C1.Win.C1Ribbon.RibbonGallery();
            this.XYPointChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.XYLineChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.XYLineWithMarkerChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.RibbonButton15 = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButtonChartOthers = new C1.Win.C1Ribbon.RibbonGallery();
            this.HighLowCloseChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.OpenHighLowCloseChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.CandlestickChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.XYZSurfaceChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ribbonGalleryItem19 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ribbonGalleryItem20 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.DoughnutChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ExplodedDoughnutChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ribbonGalleryItem23 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.XYBubbleIn2DChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.XYBubbleIn3DChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.ribbonGalleryItem26 = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.RadarLineChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.RadarLineWithMarkerChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.RadarAreaChart = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.RibbonButton50 = new C1.Win.C1Ribbon.RibbonButton();
            this.rpDelete = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbDeleteShape = new C1.Win.C1Ribbon.RibbonSplitButton();
            this.rbDeleteActiveShape = new C1.Win.C1Ribbon.RibbonButton();
            this.rbDeleteAllShape = new C1.Win.C1Ribbon.RibbonButton();
            this.rpSparkline = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbSparklineLine = new C1.Win.C1Ribbon.RibbonButton();
            this.rbSparklineColumn = new C1.Win.C1Ribbon.RibbonButton();
            this.rbSparklineWinLoss = new C1.Win.C1Ribbon.RibbonButton();
            this.rpCameraShape = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbCameraShape = new C1.Win.C1Ribbon.RibbonButton();
            this.rtpageLayout = new C1.Win.C1Ribbon.RibbonTab();
            this.rpPageSetup = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbMarginsTemp = new C1.Win.C1Ribbon.RibbonButton();
            this.rbMargins = new C1.Win.C1Ribbon.RibbonGallery();
            this.rbMarginNormal = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbMarginNarrow = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.rbMarginWide = new C1.Win.C1Ribbon.RibbonGalleryItem();
            this.btnCustomMargins = new C1.Win.C1Ribbon.RibbonButton();
            this.rbOrientation = new C1.Win.C1Ribbon.RibbonSplitButton();
            this.rbOrientationNormal = new C1.Win.C1Ribbon.RibbonButton();
            this.rbOrientationPortraint = new C1.Win.C1Ribbon.RibbonButton();
            this.rbOrientationLandscape = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButton263 = new C1.Win.C1Ribbon.RibbonSplitButton();
            this.rbPrintAreaSet = new C1.Win.C1Ribbon.RibbonButton();
            this.rbPrintAreaClear = new C1.Win.C1Ribbon.RibbonButton();
            this.rbBackGround = new C1.Win.C1Ribbon.RibbonButton();
            this.rbPrintTitles = new C1.Win.C1Ribbon.RibbonButton();
            this.rbSmartPrint = new C1.Win.C1Ribbon.RibbonButton();
            this.rtData = new C1.Win.C1Ribbon.RibbonTab();
            this.rpOutline = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbGroup = new C1.Win.C1Ribbon.RibbonButton();
            this.rbUnGroup = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonSeparator3 = new C1.Win.C1Ribbon.RibbonSeparator();
            this.rbExpandGroup = new C1.Win.C1Ribbon.RibbonButton();
            this.rbCollapseGroup = new C1.Win.C1Ribbon.RibbonButton();
            this.rpSort = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbSortAZ = new C1.Win.C1Ribbon.RibbonButton();
            this.rbSortZA = new C1.Win.C1Ribbon.RibbonButton();
            this.rbSort = new C1.Win.C1Ribbon.RibbonButton();
            this.rpCustomName = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbAddCustomname = new C1.Win.C1Ribbon.RibbonButton();
            this.rtView = new C1.Win.C1Ribbon.RibbonTab();
            this.rpShowHide = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbRowHeader = new C1.Win.C1Ribbon.RibbonCheckBox();
            this.rbColumnHeader = new C1.Win.C1Ribbon.RibbonCheckBox();
            this.ribbonSeparator4 = new C1.Win.C1Ribbon.RibbonSeparator();
            this.rbVerticalGridLine = new C1.Win.C1Ribbon.RibbonCheckBox();
            this.rbHorizontalGridLine = new C1.Win.C1Ribbon.RibbonCheckBox();
            this.ribbonSeparator5 = new C1.Win.C1Ribbon.RibbonSeparator();
            this.rbFormulaBar = new C1.Win.C1Ribbon.RibbonCheckBox();
            this.rpZoom = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbZoom = new C1.Win.C1Ribbon.RibbonButton();
            this.rb100Percent = new C1.Win.C1Ribbon.RibbonButton();
            this.rbZoomToSelection = new C1.Win.C1Ribbon.RibbonButton();
            this.rpWindow = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbFreezePanes = new C1.Win.C1Ribbon.RibbonSplitButton();
            this.rbFreezeRowsAndColumns = new C1.Win.C1Ribbon.RibbonButton();
            this.rbFreezeTopRow = new C1.Win.C1Ribbon.RibbonButton();
            this.rbFreezeFirstColumn = new C1.Win.C1Ribbon.RibbonButton();
            this.rbFreezeTrailingRowsAndColumns = new C1.Win.C1Ribbon.RibbonButton();
            this.rtSetting = new C1.Win.C1Ribbon.RibbonTab();
            this.rpSpreadSettings = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbSpreadSettingGeneral = new C1.Win.C1Ribbon.RibbonButton();
            this.rbSpreadSettingEdit = new C1.Win.C1Ribbon.RibbonButton();
            this.rbSpreadSettingScrollBar = new C1.Win.C1Ribbon.RibbonButton();
            this.rbSpreadSettingSplitBox = new C1.Win.C1Ribbon.RibbonButton();
            this.rbSpreadSettingView = new C1.Win.C1Ribbon.RibbonButton();
            this.rbSpreadSettingTitles = new C1.Win.C1Ribbon.RibbonButton();
            this.rpSheetSettings = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbSheetSettingGeneral = new C1.Win.C1Ribbon.RibbonButton();
            this.rbSheetSettingColor = new C1.Win.C1Ribbon.RibbonButton();
            this.rbSheetSettingHeaders = new C1.Win.C1Ribbon.RibbonButton();
            this.rbSheetSettingGridLines = new C1.Win.C1Ribbon.RibbonButton();
            this.rbSheetSettingCalculation = new C1.Win.C1Ribbon.RibbonButton();
            this.rbSheetSettingFonts = new C1.Win.C1Ribbon.RibbonButton();
            this.rpAppearanceSettings = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbSpreadSkin = new C1.Win.C1Ribbon.RibbonButton();
            this.rbSheetSkin = new C1.Win.C1Ribbon.RibbonButton();
            this.rbFocusIndicator = new C1.Win.C1Ribbon.RibbonButton();
            this.rbStyle = new C1.Win.C1Ribbon.RibbonButton();
            this.rbTabStrip = new C1.Win.C1Ribbon.RibbonButton();
            this.rbAlternatingRow = new C1.Win.C1Ribbon.RibbonButton();
            this.rpOtherSettings = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbGroupInfo = new C1.Win.C1Ribbon.RibbonButton();
            this.rbCellColumnsandRows = new C1.Win.C1Ribbon.RibbonButton();
            this.rbInputMap = new C1.Win.C1Ribbon.RibbonButton();
            this.rpDesignerSettings = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbPreferences = new C1.Win.C1Ribbon.RibbonSplitButton();
            this.rbShowStart = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbAllowDrag = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbAutoLaunch = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbAutomticSelectContextMenu = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbShowPropertyGrid = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbShowAllConditionalFormat = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rtDrawingTools = new C1.Win.C1Ribbon.RibbonTab();
            this.RibbonPanel13 = new C1.Win.C1Ribbon.RibbonGroup();
            this.RibbonColorChooser1 = new C1.Win.C1Ribbon.RibbonColorPicker();
            this.RibbonColorChooser2 = new C1.Win.C1Ribbon.RibbonColorPicker();
            this.RibbonColorChooser3 = new C1.Win.C1Ribbon.RibbonColorPicker();
            this.ribbonSeparator12 = new C1.Win.C1Ribbon.RibbonSeparator();
            this.RibbonButton110 = new C1.Win.C1Ribbon.RibbonMenu();
            this.rbShapeThickNone = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButton159 = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButton160 = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButton161 = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButton162 = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButton163 = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButton164 = new C1.Win.C1Ribbon.RibbonButton();
            this.rbShapeThickCustom = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButton111 = new C1.Win.C1Ribbon.RibbonMenu();
            this.rbShapeOutlineSolid = new C1.Win.C1Ribbon.RibbonButton();
            this.rbShapeOutlineDash = new C1.Win.C1Ribbon.RibbonButton();
            this.rbShapeOutlineDot = new C1.Win.C1Ribbon.RibbonButton();
            this.rbShapeOutlineDashDot = new C1.Win.C1Ribbon.RibbonButton();
            this.rbShapeOutlineDashDotDot = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButton137 = new C1.Win.C1Ribbon.RibbonMenu();
            this.rbShapeShadowNone = new C1.Win.C1Ribbon.RibbonButton();
            this.rbShapeShadowRight = new C1.Win.C1Ribbon.RibbonButton();
            this.rbShapeShadowBottomRight = new C1.Win.C1Ribbon.RibbonButton();
            this.rbShapeShadowBottom = new C1.Win.C1Ribbon.RibbonButton();
            this.rbShapeShadowBottomLeft = new C1.Win.C1Ribbon.RibbonButton();
            this.rbShapeShadowLeft = new C1.Win.C1Ribbon.RibbonButton();
            this.rbShapeShadowTopLeft = new C1.Win.C1Ribbon.RibbonButton();
            this.rbShapeShadowTop = new C1.Win.C1Ribbon.RibbonButton();
            this.rbShapeShadowTopRight = new C1.Win.C1Ribbon.RibbonButton();
            this.rbShapeShadowCustom = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonPanel14 = new C1.Win.C1Ribbon.RibbonGroup();
            this.RibbonButton138 = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButton139 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonSeparator13 = new C1.Win.C1Ribbon.RibbonSeparator();
            this.RibbonButton140 = new C1.Win.C1Ribbon.RibbonMenu();
            this.RibbonButton180 = new C1.Win.C1Ribbon.RibbonButton();
            this.btnRotate1 = new C1.Win.C1Ribbon.RibbonButton();
            this.btnRotate5 = new C1.Win.C1Ribbon.RibbonButton();
            this.btnRotate45 = new C1.Win.C1Ribbon.RibbonButton();
            this.btnRotate90 = new C1.Win.C1Ribbon.RibbonButton();
            this.btnRotate180 = new C1.Win.C1Ribbon.RibbonButton();
            this.btnRotate270 = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButton187 = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButton141 = new C1.Win.C1Ribbon.RibbonMenu();
            this.RibbonButton188 = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButton189 = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButton142 = new C1.Win.C1Ribbon.RibbonMenu();
            this.btnScale10 = new C1.Win.C1Ribbon.RibbonButton();
            this.btnScale25 = new C1.Win.C1Ribbon.RibbonButton();
            this.btnScale50 = new C1.Win.C1Ribbon.RibbonButton();
            this.btnScale75 = new C1.Win.C1Ribbon.RibbonButton();
            this.btnScale150 = new C1.Win.C1Ribbon.RibbonButton();
            this.btnScale200 = new C1.Win.C1Ribbon.RibbonButton();
            this.btnScale300 = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButton197 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonSeparator14 = new C1.Win.C1Ribbon.RibbonSeparator();
            this.rbSetPicture = new C1.Win.C1Ribbon.RibbonButton();
            this.rbClearPicture = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonSeparator15 = new C1.Win.C1Ribbon.RibbonSeparator();
            this.RibbonButton144 = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButton145 = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButton146 = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButton147 = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButton148 = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButton149 = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButton150 = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButton151 = new C1.Win.C1Ribbon.RibbonButton();
            this.RibbonButton152 = new C1.Win.C1Ribbon.RibbonButton();
            this.rtChartTools = new C1.Win.C1Ribbon.RibbonTab();
            this.rpChartType = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbChartChangeChartType = new C1.Win.C1Ribbon.RibbonButton();
            this.rpChartData = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbChartSwitchRowColumn = new C1.Win.C1Ribbon.RibbonButton();
            this.rbChartSelectData = new C1.Win.C1Ribbon.RibbonButton();
            this.rpChartArrange = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbChartSendToBack = new C1.Win.C1Ribbon.RibbonButton();
            this.rbChartBringToFront = new C1.Win.C1Ribbon.RibbonButton();
            this.rpChartLocation = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbMoveChart = new C1.Win.C1Ribbon.RibbonButton();
            this.rpChartMove = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbChartMove = new C1.Win.C1Ribbon.RibbonComboBox();
            this.rbChartMoveHV = new C1.Win.C1Ribbon.RibbonButton();
            this.rbChartMoveH = new C1.Win.C1Ribbon.RibbonButton();
            this.rbChartMoveV = new C1.Win.C1Ribbon.RibbonButton();
            this.rbChartMoveNone = new C1.Win.C1Ribbon.RibbonButton();
            this.rbChartSize = new C1.Win.C1Ribbon.RibbonComboBox();
            this.rbChartSizeBoth = new C1.Win.C1Ribbon.RibbonButton();
            this.rbChartSizeWidth = new C1.Win.C1Ribbon.RibbonButton();
            this.rbChartSizeHeight = new C1.Win.C1Ribbon.RibbonButton();
            this.rbChartSizeNone = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonSeparator16 = new C1.Win.C1Ribbon.RibbonSeparator();
            this.rbChartMoveWithCell = new C1.Win.C1Ribbon.RibbonCheckBox();
            this.rbChartSizeWithCell = new C1.Win.C1Ribbon.RibbonCheckBox();
            this.rtSparkline = new C1.Win.C1Ribbon.RibbonTab();
            this.rpSparklineEditData = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbSparklineEditData = new C1.Win.C1Ribbon.RibbonSplitButton();
            this.rbSparklineEditDataGroup = new C1.Win.C1Ribbon.RibbonButton();
            this.rbSparklineEditDataSingle = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonSeparator19 = new C1.Win.C1Ribbon.RibbonSeparator();
            this.rbSparklineEditDataHidden = new C1.Win.C1Ribbon.RibbonButton();
            this.rbSparklineEditDataSwitch = new C1.Win.C1Ribbon.RibbonButton();
            this.rpSparklineType = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbSparklineTypeLine = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbSparklineTypeColumn = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbSparklineTypeWinLoss = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rpSparklineShow = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbSparklineHighPoint = new C1.Win.C1Ribbon.RibbonCheckBox();
            this.rbSparklineLowPoint = new C1.Win.C1Ribbon.RibbonCheckBox();
            this.rbSparklineMarkerColorNegativePoint = new C1.Win.C1Ribbon.RibbonCheckBox();
            this.rbSparklineFirstPoint = new C1.Win.C1Ribbon.RibbonCheckBox();
            this.rbSparklineLastPoint = new C1.Win.C1Ribbon.RibbonCheckBox();
            this.rbSparklineMarkerColorMarker = new C1.Win.C1Ribbon.RibbonCheckBox();
            this.rpSparklineStyle = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbSparklineColor = new C1.Win.C1Ribbon.RibbonColorPicker();
            this.rbSparklineWeight = new C1.Win.C1Ribbon.RibbonButton();
            this.rbSparklineMarkerColor = new C1.Win.C1Ribbon.RibbonMenu();
            this.rbSparklineNegativePoints = new C1.Win.C1Ribbon.RibbonColorPicker();
            this.rbSparklineMarkers = new C1.Win.C1Ribbon.RibbonColorPicker();
            this.rbSparklineMarkerColorHighPoint = new C1.Win.C1Ribbon.RibbonColorPicker();
            this.rbSparklineMarkerColorLowPoint = new C1.Win.C1Ribbon.RibbonColorPicker();
            this.rbSparklineMarkerColorFirstPoint = new C1.Win.C1Ribbon.RibbonColorPicker();
            this.rbSparklineMarkerColorLastPoint = new C1.Win.C1Ribbon.RibbonColorPicker();
            this.rpSparklineGroup = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbSparklineAxis = new C1.Win.C1Ribbon.RibbonSplitButton();
            this.rbSparklineAxisSubCaption1 = new C1.Win.C1Ribbon.RibbonLabel();
            this.rbSparklineAxisSub1 = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbSparklineAxisSub2 = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbSparklineAxisSub3 = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbSparklineAxisSub4 = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbSparklineAxisSubCaption2 = new C1.Win.C1Ribbon.RibbonLabel();
            this.rbSparklineAxisSub5 = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbSparklineAxisSub6 = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbSparklineAxisSub7 = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbSparklineAxisSubCaption3 = new C1.Win.C1Ribbon.RibbonLabel();
            this.rbSparklineAxisSub8 = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbSparklineAxisSub9 = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbSparklineAxisSub10 = new C1.Win.C1Ribbon.RibbonToggleButton();
            this.rbSparklineGroup = new C1.Win.C1Ribbon.RibbonButton();
            this.rbSparklineUngroup = new C1.Win.C1Ribbon.RibbonButton();
            this.rbSparklineClear = new C1.Win.C1Ribbon.RibbonMenu();
            this.rbSparklineClearSingle = new C1.Win.C1Ribbon.RibbonButton();
            this.rbSparklineClearGroup = new C1.Win.C1Ribbon.RibbonButton();
            this.rtTable = new C1.Win.C1Ribbon.RibbonTab();
            this.rgProperties = new C1.Win.C1Ribbon.RibbonGroup();
            this.rlTableName = new C1.Win.C1Ribbon.RibbonLabel();
            this.rtbTableName = new C1.Win.C1Ribbon.RibbonTextBox();
            this.rbTableResize = new C1.Win.C1Ribbon.RibbonButton();
            this.rgTools = new C1.Win.C1Ribbon.RibbonGroup();
            this.rbTableConvertToRange = new C1.Win.C1Ribbon.RibbonButton();
            this.rgTableStyleOptions = new C1.Win.C1Ribbon.RibbonGroup();
            this.rcTableHeaderRow = new C1.Win.C1Ribbon.RibbonCheckBox();
            this.rcTableTotalRow = new C1.Win.C1Ribbon.RibbonCheckBox();
            this.rcTableBandedRow = new C1.Win.C1Ribbon.RibbonCheckBox();
            this.rcTableFirstColumn = new C1.Win.C1Ribbon.RibbonCheckBox();
            this.rcTableLastColumn = new C1.Win.C1Ribbon.RibbonCheckBox();
            this.rcTableBandedColumn = new C1.Win.C1Ribbon.RibbonCheckBox();
            this.rcTableFilterButton = new C1.Win.C1Ribbon.RibbonCheckBox();
            this.rgTableStyles = new C1.Win.C1Ribbon.RibbonGroup();
            this.rglTableStyles = new C1.Win.C1Ribbon.RibbonGallery();
            this.rbTableStyleClear = new C1.Win.C1Ribbon.RibbonButton();
            this.fpSpread1 = new FarPoint.Win.Spread.FpSpread();
            this.fpSpread1_Sheet1 = new FarPoint.Win.Spread.SheetView();
            this.formulaTextBox1 = new FarPoint.Win.Spread.FormulaTextBox();
            this.ribbonButton19 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonButton20 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonButton21 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonButton22 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonButton23 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonButton24 = new C1.Win.C1Ribbon.RibbonButton();
            this.FormulaPanel = new System.Windows.Forms.Panel();
            this.nameBox1 = new FarPoint.Win.Spread.NameBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnFormulaEdit = new System.Windows.Forms.Button();
            this.btnFormulaCancel = new System.Windows.Forms.Button();
            this.btnFormulaSet = new System.Windows.Forms.Button();
            this.btnSheetSkins1 = new C1.Win.C1Ribbon.RibbonButton();
            this.menuContext_Celltypes = new System.Windows.Forms.MenuItem();
            this.menuContext_Button = new System.Windows.Forms.MenuItem();
            this.menuContext_Checkbox = new System.Windows.Forms.MenuItem();
            this.menuContext_Combobox = new System.Windows.Forms.MenuItem();
            this.menuContext_Currency = new System.Windows.Forms.MenuItem();
            this.menuContext_DateTime = new System.Windows.Forms.MenuItem();
            this.menuContext_Text = new System.Windows.Forms.MenuItem();
            this.menuContext_Hyperlink = new System.Windows.Forms.MenuItem();
            this.menuContext_Label = new System.Windows.Forms.MenuItem();
            this.menuContext_Mask = new System.Windows.Forms.MenuItem();
            this.menuContext_Number = new System.Windows.Forms.MenuItem();
            this.menuContext_Percent = new System.Windows.Forms.MenuItem();
            this.menuContext_MultiOption = new System.Windows.Forms.MenuItem();
            this.menuContext_Progress = new System.Windows.Forms.MenuItem();
            this.menuContext_Slider = new System.Windows.Forms.MenuItem();
            this.menuContext_Image = new System.Windows.Forms.MenuItem();
            this.menuContext_General = new System.Windows.Forms.MenuItem();
            this.menuContext_GcTextBox = new System.Windows.Forms.MenuItem();
            this.menuContext_GcDate = new System.Windows.Forms.MenuItem();
            this.menuContext_Sep1 = new System.Windows.Forms.MenuItem();
            this.menuContext_Cut = new System.Windows.Forms.MenuItem();
            this.menuContext_Copy = new System.Windows.Forms.MenuItem();
            this.menuContext_Paste = new System.Windows.Forms.MenuItem();
            this.menuContext_PasteAll = new System.Windows.Forms.MenuItem();
            this.menuContext_PasteData = new System.Windows.Forms.MenuItem();
            this.menuContext_PasteFormatting = new System.Windows.Forms.MenuItem();
            this.menuContext_PasteFormulas = new System.Windows.Forms.MenuItem();
            this.menuContext_PasteAsLink = new System.Windows.Forms.MenuItem();
            this.menuContext_PasteAsString = new System.Windows.Forms.MenuItem();
            this.menuContext_Sep2 = new System.Windows.Forms.MenuItem();
            this.menuContext_Clear = new System.Windows.Forms.MenuItem();
            this.menuContext_SelectAll = new System.Windows.Forms.MenuItem();
            this.menuContext_SelectAllSheet = new System.Windows.Forms.MenuItem();
            this.menuContext_SelectAllCells = new System.Windows.Forms.MenuItem();
            this.menuContext_SelectAllData = new System.Windows.Forms.MenuItem();
            this.menuContext_Sep3 = new System.Windows.Forms.MenuItem();
            this.menuContext_Sparkline = new System.Windows.Forms.MenuItem();
            this.menuContext_SparklineEditGroup = new System.Windows.Forms.MenuItem();
            this.menuContext_SparklineEditSingle = new System.Windows.Forms.MenuItem();
            this.menuContext_SparklineGroup = new System.Windows.Forms.MenuItem();
            this.menuContext_SparklineUngroup = new System.Windows.Forms.MenuItem();
            this.menuContext_SparklineClear = new System.Windows.Forms.MenuItem();
            this.menuContext_SparklineClearGroup = new System.Windows.Forms.MenuItem();
            this.menuContext_SparklineSwitch = new System.Windows.Forms.MenuItem();
            this.menuContext_Span = new System.Windows.Forms.MenuItem();
            this.menuContext_Lock = new System.Windows.Forms.MenuItem();
            this.menuContext_Borders = new System.Windows.Forms.MenuItem();
            this.menuContext_Insert = new System.Windows.Forms.MenuItem();
            this.menuContext_Delete = new System.Windows.Forms.MenuItem();
            this.menuContext_Headers = new System.Windows.Forms.MenuItem();
            this.menuContext_ColumnWidth = new System.Windows.Forms.MenuItem();
            this.menuContext_RowHeight = new System.Windows.Forms.MenuItem();
            this.menuContext_Hide = new System.Windows.Forms.MenuItem();
            this.menuContext_UnHide = new System.Windows.Forms.MenuItem();
            this.menuContext_UnHideSelection = new System.Windows.Forms.MenuItem();
            this.menuContext_UnHideAllHidden = new System.Windows.Forms.MenuItem();
            this.menuContext_UnHideSpecific = new System.Windows.Forms.MenuItem();
            this.menuContext_ClearCellType = new System.Windows.Forms.MenuItem();
            this.menuContext_Sep10 = new System.Windows.Forms.MenuItem();
            this.menuContext_SheetSettings = new System.Windows.Forms.MenuItem();
            this.menuContext_SheetSkinDesigner = new System.Windows.Forms.MenuItem();
            this.menuContext_SpreadSkinDesigner = new System.Windows.Forms.MenuItem();
            this.menuContext_SheetPrintOptions = new System.Windows.Forms.MenuItem();
            this.menuContext_ListBox = new System.Windows.Forms.MenuItem();
            this.menuContext_MultiColumnComboBox = new System.Windows.Forms.MenuItem();
            this.menuContext_Barcode = new System.Windows.Forms.MenuItem();
            this.menuContext_ColorPicker = new System.Windows.Forms.MenuItem();
            this.menuContext_AutoFit = new System.Windows.Forms.MenuItem();
            this.shapeContextMenu = new System.Windows.Forms.ContextMenu();
            this.shapeContextDelete = new System.Windows.Forms.MenuItem();
            this.shapeContextProperties = new System.Windows.Forms.MenuItem();
            this.shapeContextFormula = new System.Windows.Forms.MenuItem();
            this.shapeContextSep1 = new System.Windows.Forms.MenuItem();
            this.shapeContextLocked = new System.Windows.Forms.MenuItem();
            this.shapeContextSep2 = new System.Windows.Forms.MenuItem();
            this.shapeContextCut = new System.Windows.Forms.MenuItem();
            this.shapeContextCopy = new System.Windows.Forms.MenuItem();
            this.shapeContextPaste = new System.Windows.Forms.MenuItem();
            this.menuContext_RichText = new System.Windows.Forms.MenuItem();
            this.menuContext_RegEx = new System.Windows.Forms.MenuItem();
            this.menuContext_PasteShape = new System.Windows.Forms.MenuItem();
            this.menuContext_PasteChart = new System.Windows.Forms.MenuItem();
            this.sbpZoom = new System.Windows.Forms.StatusBarPanel();
            this.noteContextMenu = new System.Windows.Forms.ContextMenu();
            this.menuContext_EditNote = new System.Windows.Forms.MenuItem();
            this.menuContext_DeleteNote = new System.Windows.Forms.MenuItem();
            this.menuContext_Sep4 = new System.Windows.Forms.MenuItem();
            this.menuContext_DeleteSheet = new System.Windows.Forms.MenuItem();
            this.contextMenu1 = new System.Windows.Forms.ContextMenu();
            this.sheetTabContextMenu = new System.Windows.Forms.ContextMenu();
            this.c1StatusBar1 = new C1.Win.C1Ribbon.C1StatusBar();
            this.RenameTextBox = new System.Windows.Forms.TextBox();
            this.ribbonTab1 = new C1.Win.C1Ribbon.RibbonTab();
            this.ribbonGroup1 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton1List = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonButton2SaveCloud = new C1.Win.C1Ribbon.RibbonButton();
            ((System.ComponentModel.ISupportInitialize)(this.c1Ribbon1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fpSpread1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fpSpread1_Sheet1)).BeginInit();
            this.FormulaPanel.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sbpZoom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1StatusBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // c1Ribbon1
            // 
            this.c1Ribbon1.ApplicationMenuHolder = this.ribbonApplicationMenu1;
            this.c1Ribbon1.ConfigToolBarHolder = this.ribbonConfigToolBar1;
            this.c1Ribbon1.Location = new System.Drawing.Point(0, 0);
            this.c1Ribbon1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.c1Ribbon1.Name = "c1Ribbon1";
            this.c1Ribbon1.QatHolder = this.ribbonQat1;
            this.c1Ribbon1.QatItemsHolder.Add(this.tbnUndo);
            this.c1Ribbon1.QatItemsHolder.Add(this.tbnRedo);
            this.c1Ribbon1.QatItemsHolder.Add(this.tbnNew);
            this.c1Ribbon1.QatItemsHolder.Add(this.tbnOpen);
            this.c1Ribbon1.QatItemsHolder.Add(this.tbnSave);
            this.c1Ribbon1.QatItemsHolder.Add(this.ribbonSeparator21);
            this.c1Ribbon1.QatItemsHolder.Add(this.tbnPrint);
            this.c1Ribbon1.QatItemsHolder.Add(this.ribbonSeparator22);
            this.c1Ribbon1.Size = new System.Drawing.Size(1285, 153);
            this.c1Ribbon1.Tabs.Add(this.rtHome);
            this.c1Ribbon1.Tabs.Add(this.rtInsert);
            this.c1Ribbon1.Tabs.Add(this.rtpageLayout);
            this.c1Ribbon1.Tabs.Add(this.rtData);
            this.c1Ribbon1.Tabs.Add(this.rtView);
            this.c1Ribbon1.Tabs.Add(this.rtSetting);
            this.c1Ribbon1.Tabs.Add(this.rtDrawingTools);
            this.c1Ribbon1.Tabs.Add(this.rtChartTools);
            this.c1Ribbon1.Tabs.Add(this.rtSparkline);
            this.c1Ribbon1.Tabs.Add(this.rtTable);
            this.c1Ribbon1.Tabs.Add(this.ribbonTab1);
            // 
            // ribbonApplicationMenu1
            // 
            this.ribbonApplicationMenu1.DropDownWidth = 100;
            this.ribbonApplicationMenu1.LargeImage = global::SpreadDesigner.Properties.Resources.SpreadWin;
            this.ribbonApplicationMenu1.Name = "ribbonApplicationMenu1";
            this.ribbonApplicationMenu1.Visible = false;
            // 
            // ribbonConfigToolBar1
            // 
            this.ribbonConfigToolBar1.Name = "ribbonConfigToolBar1";
            // 
            // ribbonQat1
            // 
            this.ribbonQat1.ItemLinks.Add(this.tbnUndo);
            this.ribbonQat1.ItemLinks.Add(this.tbnRedo);
            this.ribbonQat1.ItemLinks.Add(this.ribbonSeparator22);
            this.ribbonQat1.ItemLinks.Add(this.tbnNew);
            this.ribbonQat1.ItemLinks.Add(this.tbnOpen);
            this.ribbonQat1.ItemLinks.Add(this.tbnSave);
            this.ribbonQat1.ItemLinks.Add(this.ribbonSeparator21);
            this.ribbonQat1.ItemLinks.Add(this.tbnPrint);
            this.ribbonQat1.MenuVisible = false;
            this.ribbonQat1.Name = "ribbonQat1";
            // 
            // tbnUndo
            // 
            this.tbnUndo.Name = "tbnUndo";
            this.tbnUndo.SmallImage = ((System.Drawing.Image)(resources.GetObject("tbnUndo.SmallImage")));
            this.tbnUndo.Text = "Button";
            // 
            // tbnRedo
            // 
            this.tbnRedo.Name = "tbnRedo";
            this.tbnRedo.SmallImage = ((System.Drawing.Image)(resources.GetObject("tbnRedo.SmallImage")));
            this.tbnRedo.Text = "Button";
            // 
            // ribbonSeparator22
            // 
            this.ribbonSeparator22.Name = "ribbonSeparator22";
            // 
            // tbnNew
            // 
            this.tbnNew.Name = "tbnNew";
            this.tbnNew.SmallImage = ((System.Drawing.Image)(resources.GetObject("tbnNew.SmallImage")));
            this.tbnNew.Text = "New";
            // 
            // tbnOpen
            // 
            this.tbnOpen.Name = "tbnOpen";
            this.tbnOpen.SmallImage = ((System.Drawing.Image)(resources.GetObject("tbnOpen.SmallImage")));
            this.tbnOpen.Text = "Button";
            // 
            // tbnSave
            // 
            this.tbnSave.Name = "tbnSave";
            this.tbnSave.SmallImage = ((System.Drawing.Image)(resources.GetObject("tbnSave.SmallImage")));
            this.tbnSave.Text = "Button";
            // 
            // ribbonSeparator21
            // 
            this.ribbonSeparator21.Name = "ribbonSeparator21";
            // 
            // tbnPrint
            // 
            this.tbnPrint.Name = "tbnPrint";
            this.tbnPrint.SmallImage = ((System.Drawing.Image)(resources.GetObject("tbnPrint.SmallImage")));
            this.tbnPrint.Text = "Button";
            // 
            // rtHome
            // 
            this.rtHome.Groups.Add(this.rpClipboard);
            this.rtHome.Groups.Add(this.rpFont);
            this.rtHome.Groups.Add(this.rpAlignment);
            this.rtHome.Groups.Add(this.rpCellType);
            this.rtHome.Groups.Add(this.rpStyle);
            this.rtHome.Groups.Add(this.rpEditing);
            this.rtHome.Name = "rtHome";
            this.rtHome.Text = "HOME";
            // 
            // rpClipboard
            // 
            this.rpClipboard.Items.Add(this.rbPaste);
            this.rpClipboard.Items.Add(this.rbCut);
            this.rpClipboard.Items.Add(this.rbCopy);
            this.rpClipboard.Name = "rpClipboard";
            this.rpClipboard.Text = "Clipboard";
            // 
            // rbPaste
            // 
            this.rbPaste.Items.Add(this.rbPasteAll);
            this.rbPaste.Items.Add(this.rbPasteValues);
            this.rbPaste.Items.Add(this.rbPasteFormatting);
            this.rbPaste.Items.Add(this.rbPasteFormulas);
            this.rbPaste.Items.Add(this.rbPasteAsLink);
            this.rbPaste.Items.Add(this.rbPasteAsString);
            this.rbPaste.Items.Add(this.rbPasteAsShape);
            this.rbPaste.Items.Add(this.rbPasteAsChart);
            this.rbPaste.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbPaste.LargeImage")));
            this.rbPaste.Name = "rbPaste";
            this.rbPaste.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbPaste.SmallImage")));
            this.rbPaste.Text = "Paste";
            this.rbPaste.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbPaste.Click += new System.EventHandler(this.btnPaste_Click);
            this.rbPaste.DropDown += new System.EventHandler(this.rbPaste_DropDown);
            // 
            // rbPasteAll
            // 
            this.rbPasteAll.Name = "rbPasteAll";
            this.rbPasteAll.Text = "&All CellInfo";
            // 
            // rbPasteValues
            // 
            this.rbPasteValues.Name = "rbPasteValues";
            this.rbPasteValues.Text = "&Data";
            // 
            // rbPasteFormatting
            // 
            this.rbPasteFormatting.Name = "rbPasteFormatting";
            this.rbPasteFormatting.Text = "&Formatting";
            // 
            // rbPasteFormulas
            // 
            this.rbPasteFormulas.Name = "rbPasteFormulas";
            this.rbPasteFormulas.Text = "Form&ulas";
            // 
            // rbPasteAsLink
            // 
            this.rbPasteAsLink.Name = "rbPasteAsLink";
            this.rbPasteAsLink.Text = "As &Link";
            // 
            // rbPasteAsString
            // 
            this.rbPasteAsString.Name = "rbPasteAsString";
            this.rbPasteAsString.Text = "As &String";
            // 
            // rbPasteAsShape
            // 
            this.rbPasteAsShape.Name = "rbPasteAsShape";
            this.rbPasteAsShape.Text = "Sha&pe";
            // 
            // rbPasteAsChart
            // 
            this.rbPasteAsChart.Name = "rbPasteAsChart";
            this.rbPasteAsChart.Text = "&Chart";
            // 
            // rbCut
            // 
            this.rbCut.Name = "rbCut";
            this.rbCut.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbCut.SmallImage")));
            this.rbCut.Text = "Cut";
            this.rbCut.Click += new System.EventHandler(this.btnCut_Click);
            // 
            // rbCopy
            // 
            this.rbCopy.Name = "rbCopy";
            this.rbCopy.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbCopy.SmallImage")));
            this.rbCopy.Text = "Copy";
            this.rbCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // rpFont
            // 
            this.rpFont.Items.Add(this.ribbonToolBar3);
            this.rpFont.Items.Add(this.ribbonToolBar2);
            this.rpFont.Items.Add(this.ribbonToolBar1);
            this.rpFont.Items.Add(this.ribbonToolBar5);
            this.rpFont.Items.Add(this.ribbonToolBar6);
            this.rpFont.Name = "rpFont";
            this.rpFont.Text = "Font";
            // 
            // ribbonToolBar3
            // 
            this.ribbonToolBar3.Items.Add(this.rcbFont);
            this.ribbonToolBar3.Items.Add(this.rcbFontSize);
            this.ribbonToolBar3.Items.Add(this.rbIncreaseFontSize);
            this.ribbonToolBar3.Items.Add(this.rbDecreaseFontSize);
            this.ribbonToolBar3.Name = "ribbonToolBar3";
            // 
            // rcbFont
            // 
            this.rcbFont.Name = "rcbFont";
            this.rcbFont.PreviewFonts = false;
            this.rcbFont.TextAreaWidth = 110;
            this.rcbFont.SelectedIndexChanged += new System.EventHandler(this.ComboBoxFont_SelectedIndexChanged);
            // 
            // rcbFontSize
            // 
            this.rcbFontSize.MaxDropDownItems = 18;
            this.rcbFontSize.Name = "rcbFontSize";
            this.rcbFontSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.rcbFontSize.TextAreaWidth = 30;
            this.rcbFontSize.SelectedIndexChanged += new System.EventHandler(this.ComboBoxFontSize_SelectedIndexChanged);
            // 
            // rbIncreaseFontSize
            // 
            this.rbIncreaseFontSize.Name = "rbIncreaseFontSize";
            this.rbIncreaseFontSize.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbIncreaseFontSize.SmallImage")));
            this.rbIncreaseFontSize.Click += new System.EventHandler(this.btnIncreaseFontSize_Click);
            // 
            // rbDecreaseFontSize
            // 
            this.rbDecreaseFontSize.Name = "rbDecreaseFontSize";
            this.rbDecreaseFontSize.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbDecreaseFontSize.SmallImage")));
            this.rbDecreaseFontSize.Click += new System.EventHandler(this.btnDecreaseFontSize_Click);
            // 
            // ribbonToolBar2
            // 
            this.ribbonToolBar2.Items.Add(this.rbBold);
            this.ribbonToolBar2.Items.Add(this.rbItalic);
            this.ribbonToolBar2.Items.Add(this.rbUnderLine);
            this.ribbonToolBar2.Items.Add(this.ribbonSeparator1);
            this.ribbonToolBar2.Items.Add(this.ribbonSeparator2);
            this.ribbonToolBar2.Items.Add(this.rccFillColor);
            this.ribbonToolBar2.Items.Add(this.rccFontColor);
            this.ribbonToolBar2.Items.Add(this.rbCellBorder);
            this.ribbonToolBar2.Name = "ribbonToolBar2";
            // 
            // rbBold
            // 
            this.rbBold.Name = "rbBold";
            this.rbBold.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbBold.SmallImage")));
            this.rbBold.Click += new System.EventHandler(this.btnBold_Click);
            // 
            // rbItalic
            // 
            this.rbItalic.Name = "rbItalic";
            this.rbItalic.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbItalic.SmallImage")));
            this.rbItalic.Click += new System.EventHandler(this.btnItalic_Click);
            // 
            // rbUnderLine
            // 
            this.rbUnderLine.Name = "rbUnderLine";
            this.rbUnderLine.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbUnderLine.SmallImage")));
            this.rbUnderLine.Click += new System.EventHandler(this.btnUnderLine_Click);
            // 
            // ribbonSeparator1
            // 
            this.ribbonSeparator1.Name = "ribbonSeparator1";
            // 
            // ribbonSeparator2
            // 
            this.ribbonSeparator2.Name = "ribbonSeparator2";
            // 
            // rccFillColor
            // 
            this.rccFillColor.DefaultColor = System.Drawing.Color.Yellow;
            this.rccFillColor.Name = "rccFillColor";
            this.rccFillColor.SmallImage = ((System.Drawing.Image)(resources.GetObject("rccFillColor.SmallImage")));
            this.rccFillColor.SelectedColorChanged += new System.EventHandler(this.ColorPickerFillColor_SelectedColorChanged);
            this.rccFillColor.Click += new System.EventHandler(this.ColorPickerFillColor_Click);
            // 
            // rccFontColor
            // 
            this.rccFontColor.Name = "rccFontColor";
            this.rccFontColor.SmallImage = ((System.Drawing.Image)(resources.GetObject("rccFontColor.SmallImage")));
            this.rccFontColor.SelectedColorChanged += new System.EventHandler(this.ColorPickerFontColor_SelectedColorChanged);
            this.rccFontColor.Click += new System.EventHandler(this.ColorPickerFontColor_Click);
            // 
            // rbCellBorder
            // 
            this.rbCellBorder.Items.Add(this.rbBottomBorder);
            this.rbCellBorder.Items.Add(this.rbTopBorder);
            this.rbCellBorder.Items.Add(this.rbLeftBorder);
            this.rbCellBorder.Items.Add(this.rbRightBorder);
            this.rbCellBorder.Items.Add(this.ribbonSeparator11);
            this.rbCellBorder.Items.Add(this.rbNoBorder);
            this.rbCellBorder.Items.Add(this.rbAllBorder);
            this.rbCellBorder.Items.Add(this.rbOutsideBorder);
            this.rbCellBorder.Items.Add(this.rbThickBoxBorder);
            this.rbCellBorder.Items.Add(this.ribbonSeparator17);
            this.rbCellBorder.Items.Add(this.rbBottomDoubleBorder);
            this.rbCellBorder.Items.Add(this.rbThickBottomBorder);
            this.rbCellBorder.Items.Add(this.rbTopAndBottomBorder);
            this.rbCellBorder.Items.Add(this.rbTopAndThickBottomBorder);
            this.rbCellBorder.Items.Add(this.rbTopAndDoubleBottomBorder);
            this.rbCellBorder.Items.Add(this.ribbonSeparator18);
            this.rbCellBorder.Items.Add(this.rbMoreBorder);
            this.rbCellBorder.Name = "rbCellBorder";
            this.rbCellBorder.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbCellBorder.SmallImage")));
            // 
            // rbBottomBorder
            // 
            this.rbBottomBorder.Name = "rbBottomBorder";
            this.rbBottomBorder.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbBottomBorder.SmallImage")));
            this.rbBottomBorder.Text = "Bottom Border";
            // 
            // rbTopBorder
            // 
            this.rbTopBorder.Name = "rbTopBorder";
            this.rbTopBorder.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbTopBorder.SmallImage")));
            this.rbTopBorder.Text = "Top Border";
            // 
            // rbLeftBorder
            // 
            this.rbLeftBorder.Name = "rbLeftBorder";
            this.rbLeftBorder.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbLeftBorder.SmallImage")));
            this.rbLeftBorder.Text = "Left Border";
            // 
            // rbRightBorder
            // 
            this.rbRightBorder.Name = "rbRightBorder";
            this.rbRightBorder.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbRightBorder.SmallImage")));
            this.rbRightBorder.Text = "Right Border";
            // 
            // ribbonSeparator11
            // 
            this.ribbonSeparator11.Name = "ribbonSeparator11";
            // 
            // rbNoBorder
            // 
            this.rbNoBorder.Name = "rbNoBorder";
            this.rbNoBorder.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbNoBorder.SmallImage")));
            this.rbNoBorder.Text = "No Border";
            // 
            // rbAllBorder
            // 
            this.rbAllBorder.Name = "rbAllBorder";
            this.rbAllBorder.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbAllBorder.SmallImage")));
            this.rbAllBorder.Text = "All Border";
            // 
            // rbOutsideBorder
            // 
            this.rbOutsideBorder.Name = "rbOutsideBorder";
            this.rbOutsideBorder.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbOutsideBorder.SmallImage")));
            this.rbOutsideBorder.Text = "Outside Border";
            // 
            // rbThickBoxBorder
            // 
            this.rbThickBoxBorder.Name = "rbThickBoxBorder";
            this.rbThickBoxBorder.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbThickBoxBorder.SmallImage")));
            this.rbThickBoxBorder.Text = "Thick Box Border";
            // 
            // ribbonSeparator17
            // 
            this.ribbonSeparator17.Name = "ribbonSeparator17";
            // 
            // rbBottomDoubleBorder
            // 
            this.rbBottomDoubleBorder.Name = "rbBottomDoubleBorder";
            this.rbBottomDoubleBorder.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbBottomDoubleBorder.SmallImage")));
            this.rbBottomDoubleBorder.Text = "Bottom Double Border";
            // 
            // rbThickBottomBorder
            // 
            this.rbThickBottomBorder.Name = "rbThickBottomBorder";
            this.rbThickBottomBorder.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbThickBottomBorder.SmallImage")));
            this.rbThickBottomBorder.Text = "Thick Bottom Border";
            // 
            // rbTopAndBottomBorder
            // 
            this.rbTopAndBottomBorder.Name = "rbTopAndBottomBorder";
            this.rbTopAndBottomBorder.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbTopAndBottomBorder.SmallImage")));
            this.rbTopAndBottomBorder.Text = "Top and Bottom Border";
            // 
            // rbTopAndThickBottomBorder
            // 
            this.rbTopAndThickBottomBorder.Name = "rbTopAndThickBottomBorder";
            this.rbTopAndThickBottomBorder.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbTopAndThickBottomBorder.SmallImage")));
            this.rbTopAndThickBottomBorder.Text = "Top and Thick Bottom Border";
            // 
            // rbTopAndDoubleBottomBorder
            // 
            this.rbTopAndDoubleBottomBorder.Name = "rbTopAndDoubleBottomBorder";
            this.rbTopAndDoubleBottomBorder.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbTopAndDoubleBottomBorder.SmallImage")));
            this.rbTopAndDoubleBottomBorder.Text = "Top and Double Bottom Border";
            // 
            // ribbonSeparator18
            // 
            this.ribbonSeparator18.Name = "ribbonSeparator18";
            // 
            // rbMoreBorder
            // 
            this.rbMoreBorder.Name = "rbMoreBorder";
            this.rbMoreBorder.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbMoreBorder.SmallImage")));
            this.rbMoreBorder.Text = "More Border";
            // 
            // ribbonToolBar1
            // 
            this.ribbonToolBar1.Name = "ribbonToolBar1";
            // 
            // ribbonToolBar5
            // 
            this.ribbonToolBar5.Name = "ribbonToolBar5";
            // 
            // ribbonToolBar6
            // 
            this.ribbonToolBar6.Name = "ribbonToolBar6";
            // 
            // rpAlignment
            // 
            this.rpAlignment.Items.Add(this.ribbonToolBar4);
            this.rpAlignment.Items.Add(this.ribbonToolBar7);
            this.rpAlignment.Items.Add(this.ribbonSeparator6);
            this.rpAlignment.Items.Add(this.rbWrapText);
            this.rpAlignment.Items.Add(this.rbMerge);
            this.rpAlignment.Name = "rpAlignment";
            this.rpAlignment.Text = "Alignment";
            // 
            // ribbonToolBar4
            // 
            this.ribbonToolBar4.Items.Add(this.rbTopAlign);
            this.ribbonToolBar4.Items.Add(this.rbMiddleAlign);
            this.ribbonToolBar4.Items.Add(this.rbBottomAlign);
            this.ribbonToolBar4.Items.Add(this.rbVerticalJustify);
            this.ribbonToolBar4.Items.Add(this.rbVerticalDistributed);
            this.ribbonToolBar4.Name = "ribbonToolBar4";
            // 
            // rbTopAlign
            // 
            this.rbTopAlign.Name = "rbTopAlign";
            this.rbTopAlign.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbTopAlign.SmallImage")));
            this.rbTopAlign.Click += new System.EventHandler(this.btnTopAlign_Click);
            // 
            // rbMiddleAlign
            // 
            this.rbMiddleAlign.Name = "rbMiddleAlign";
            this.rbMiddleAlign.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbMiddleAlign.SmallImage")));
            this.rbMiddleAlign.Click += new System.EventHandler(this.btnMiddleAlign_Click);
            // 
            // rbBottomAlign
            // 
            this.rbBottomAlign.Name = "rbBottomAlign";
            this.rbBottomAlign.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbBottomAlign.SmallImage")));
            this.rbBottomAlign.Click += new System.EventHandler(this.btnBottomAlign_Click);
            // 
            // rbVerticalJustify
            // 
            this.rbVerticalJustify.Name = "rbVerticalJustify";
            this.rbVerticalJustify.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbVerticalJustify.SmallImage")));
            this.rbVerticalJustify.Click += new System.EventHandler(this.btnVerticaljustify_Click);
            // 
            // rbVerticalDistributed
            // 
            this.rbVerticalDistributed.Name = "rbVerticalDistributed";
            this.rbVerticalDistributed.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbVerticalDistributed.SmallImage")));
            this.rbVerticalDistributed.Click += new System.EventHandler(this.btnVerticalDistributed_Click);
            // 
            // ribbonToolBar7
            // 
            this.ribbonToolBar7.Items.Add(this.rbAlignTextLeft);
            this.ribbonToolBar7.Items.Add(this.rbAlignTextMiddle);
            this.ribbonToolBar7.Items.Add(this.rbAlignTextRight);
            this.ribbonToolBar7.Items.Add(this.rbHorizontalJustify);
            this.ribbonToolBar7.Items.Add(this.rbHorizontalDistributed);
            this.ribbonToolBar7.Items.Add(this.rbDecreaseIndent);
            this.ribbonToolBar7.Items.Add(this.rbIncreaseIndent);
            this.ribbonToolBar7.Name = "ribbonToolBar7";
            // 
            // rbAlignTextLeft
            // 
            this.rbAlignTextLeft.Name = "rbAlignTextLeft";
            this.rbAlignTextLeft.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbAlignTextLeft.SmallImage")));
            this.rbAlignTextLeft.Click += new System.EventHandler(this.btnAlignTextLeft_Click);
            // 
            // rbAlignTextMiddle
            // 
            this.rbAlignTextMiddle.Name = "rbAlignTextMiddle";
            this.rbAlignTextMiddle.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbAlignTextMiddle.SmallImage")));
            this.rbAlignTextMiddle.Click += new System.EventHandler(this.btnCenter_Click);
            // 
            // rbAlignTextRight
            // 
            this.rbAlignTextRight.Name = "rbAlignTextRight";
            this.rbAlignTextRight.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbAlignTextRight.SmallImage")));
            this.rbAlignTextRight.Click += new System.EventHandler(this.btnAlignTextRight_Click);
            // 
            // rbHorizontalJustify
            // 
            this.rbHorizontalJustify.Name = "rbHorizontalJustify";
            this.rbHorizontalJustify.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbHorizontalJustify.SmallImage")));
            this.rbHorizontalJustify.Click += new System.EventHandler(this.btnAlignTextHorizontalJustify_Click);
            // 
            // rbHorizontalDistributed
            // 
            this.rbHorizontalDistributed.Name = "rbHorizontalDistributed";
            this.rbHorizontalDistributed.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbHorizontalDistributed.SmallImage")));
            this.rbHorizontalDistributed.Click += new System.EventHandler(this.btnAlignTextHorizontalDistributed_Click);
            // 
            // rbDecreaseIndent
            // 
            this.rbDecreaseIndent.Name = "rbDecreaseIndent";
            this.rbDecreaseIndent.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbDecreaseIndent.SmallImage")));
            this.rbDecreaseIndent.Click += new System.EventHandler(this.btnDecIndent_Click);
            // 
            // rbIncreaseIndent
            // 
            this.rbIncreaseIndent.Name = "rbIncreaseIndent";
            this.rbIncreaseIndent.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbIncreaseIndent.SmallImage")));
            this.rbIncreaseIndent.Click += new System.EventHandler(this.btnIncreaseIndent_Click);
            // 
            // ribbonSeparator6
            // 
            this.ribbonSeparator6.Name = "ribbonSeparator6";
            // 
            // rbWrapText
            // 
            this.rbWrapText.Name = "rbWrapText";
            this.rbWrapText.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbWrapText.SmallImage")));
            this.rbWrapText.Text = "Wrap Text";
            this.rbWrapText.Click += new System.EventHandler(this.btnWrapText_Click);
            // 
            // rbMerge
            // 
            this.rbMerge.Name = "rbMerge";
            this.rbMerge.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbMerge.SmallImage")));
            this.rbMerge.Text = "Merge";
            this.rbMerge.Click += new System.EventHandler(this.btnMerge_Click);
            // 
            // rpCellType
            // 
            this.rpCellType.Items.Add(this.rcbCellType);
            this.rpCellType.Items.Add(this.rbClearCellType);
            this.rpCellType.Name = "rpCellType";
            this.rpCellType.Text = "CellType";
            // 
            // rcbCellType
            // 
            this.rcbCellType.Expanded = false;
            this.rcbCellType.Items.Add(this.rbdwGeneral);
            this.rcbCellType.Items.Add(this.rbdwText);
            this.rcbCellType.Items.Add(this.rbdwNumber);
            this.rcbCellType.Items.Add(this.rbdwPercent);
            this.rcbCellType.Items.Add(this.rbdwCurrency);
            this.rcbCellType.Items.Add(this.rbdwRegularExpression);
            this.rcbCellType.Items.Add(this.rbdwBarCode);
            this.rcbCellType.Items.Add(this.rbdwButton);
            this.rcbCellType.Items.Add(this.rbdwCheckBox);
            this.rcbCellType.Items.Add(this.rbdwMultiOption);
            this.rcbCellType.Items.Add(this.rbdwComboBox);
            this.rcbCellType.Items.Add(this.rbdwMultiColumnComboBox);
            this.rcbCellType.Items.Add(this.rbdwListBox);
            this.rcbCellType.Items.Add(this.rbdwRichText);
            this.rcbCellType.Items.Add(this.rbdwDateTime);
            this.rcbCellType.Items.Add(this.rbdwPicture);
            this.rcbCellType.Items.Add(this.rbdwColorPicker);
            this.rcbCellType.Items.Add(this.rbdwProgress);
            this.rcbCellType.Items.Add(this.rbdwSlider);
            this.rcbCellType.Items.Add(this.rbdwMask);
            this.rcbCellType.Items.Add(this.rbdwHyperLink);
            this.rcbCellType.Items.Add(this.rbdwGcDate);
            this.rcbCellType.Items.Add(this.rbdwGcTextBox);
            this.rcbCellType.ItemSize = new System.Drawing.Size(68, 68);
            this.rcbCellType.LargeImage = ((System.Drawing.Image)(resources.GetObject("rcbCellType.LargeImage")));
            this.rcbCellType.Name = "rcbCellType";
            this.rcbCellType.Text = "None";
            this.rcbCellType.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rcbCellType.VisibleItems = 6;
            // 
            // rbdwGeneral
            // 
            this.rbdwGeneral.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbdwGeneral.LargeImage")));
            this.rbdwGeneral.Name = "rbdwGeneral";
            this.rbdwGeneral.Text = "General";
            // 
            // rbdwText
            // 
            this.rbdwText.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbdwText.LargeImage")));
            this.rbdwText.Name = "rbdwText";
            this.rbdwText.Text = "Text";
            // 
            // rbdwNumber
            // 
            this.rbdwNumber.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbdwNumber.LargeImage")));
            this.rbdwNumber.Name = "rbdwNumber";
            this.rbdwNumber.Text = "Number";
            // 
            // rbdwPercent
            // 
            this.rbdwPercent.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbdwPercent.LargeImage")));
            this.rbdwPercent.Name = "rbdwPercent";
            this.rbdwPercent.Text = "Percent";
            // 
            // rbdwCurrency
            // 
            this.rbdwCurrency.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbdwCurrency.LargeImage")));
            this.rbdwCurrency.Name = "rbdwCurrency";
            this.rbdwCurrency.Text = "Currency";
            // 
            // rbdwRegularExpression
            // 
            this.rbdwRegularExpression.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbdwRegularExpression.LargeImage")));
            this.rbdwRegularExpression.Name = "rbdwRegularExpression";
            this.rbdwRegularExpression.Text = "RegularExpression";
            // 
            // rbdwBarCode
            // 
            this.rbdwBarCode.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbdwBarCode.LargeImage")));
            this.rbdwBarCode.Name = "rbdwBarCode";
            this.rbdwBarCode.Text = "BarCode";
            // 
            // rbdwButton
            // 
            this.rbdwButton.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbdwButton.LargeImage")));
            this.rbdwButton.Name = "rbdwButton";
            this.rbdwButton.Text = "Button";
            // 
            // rbdwCheckBox
            // 
            this.rbdwCheckBox.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbdwCheckBox.LargeImage")));
            this.rbdwCheckBox.Name = "rbdwCheckBox";
            this.rbdwCheckBox.Text = "CheckBox";
            // 
            // rbdwMultiOption
            // 
            this.rbdwMultiOption.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbdwMultiOption.LargeImage")));
            this.rbdwMultiOption.Name = "rbdwMultiOption";
            this.rbdwMultiOption.Text = "MultiOption";
            // 
            // rbdwComboBox
            // 
            this.rbdwComboBox.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbdwComboBox.LargeImage")));
            this.rbdwComboBox.Name = "rbdwComboBox";
            this.rbdwComboBox.Text = "ComboBox";
            // 
            // rbdwMultiColumnComboBox
            // 
            this.rbdwMultiColumnComboBox.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbdwMultiColumnComboBox.LargeImage")));
            this.rbdwMultiColumnComboBox.Name = "rbdwMultiColumnComboBox";
            this.rbdwMultiColumnComboBox.Text = "MultiColumnComboBox";
            // 
            // rbdwListBox
            // 
            this.rbdwListBox.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbdwListBox.LargeImage")));
            this.rbdwListBox.Name = "rbdwListBox";
            this.rbdwListBox.Text = "ListBox";
            // 
            // rbdwRichText
            // 
            this.rbdwRichText.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbdwRichText.LargeImage")));
            this.rbdwRichText.Name = "rbdwRichText";
            this.rbdwRichText.Text = "RichText";
            // 
            // rbdwDateTime
            // 
            this.rbdwDateTime.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbdwDateTime.LargeImage")));
            this.rbdwDateTime.Name = "rbdwDateTime";
            this.rbdwDateTime.Text = "DateTime";
            // 
            // rbdwPicture
            // 
            this.rbdwPicture.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbdwPicture.LargeImage")));
            this.rbdwPicture.Name = "rbdwPicture";
            this.rbdwPicture.Text = "Picture";
            // 
            // rbdwColorPicker
            // 
            this.rbdwColorPicker.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbdwColorPicker.LargeImage")));
            this.rbdwColorPicker.Name = "rbdwColorPicker";
            this.rbdwColorPicker.Text = "ColorPicker";
            // 
            // rbdwProgress
            // 
            this.rbdwProgress.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbdwProgress.LargeImage")));
            this.rbdwProgress.Name = "rbdwProgress";
            this.rbdwProgress.Text = "Progress";
            // 
            // rbdwSlider
            // 
            this.rbdwSlider.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbdwSlider.LargeImage")));
            this.rbdwSlider.Name = "rbdwSlider";
            this.rbdwSlider.Text = "Slider";
            // 
            // rbdwMask
            // 
            this.rbdwMask.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbdwMask.LargeImage")));
            this.rbdwMask.Name = "rbdwMask";
            this.rbdwMask.Text = "Mask";
            // 
            // rbdwHyperLink
            // 
            this.rbdwHyperLink.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbdwHyperLink.LargeImage")));
            this.rbdwHyperLink.Name = "rbdwHyperLink";
            this.rbdwHyperLink.Text = "HyperLink";
            // 
            // rbdwGcDate
            // 
            this.rbdwGcDate.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbdwGcDate.LargeImage")));
            this.rbdwGcDate.Name = "rbdwGcDate";
            this.rbdwGcDate.Text = "GcDateTime";
            // 
            // rbdwGcTextBox
            // 
            this.rbdwGcTextBox.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbdwGcTextBox.LargeImage")));
            this.rbdwGcTextBox.Name = "rbdwGcTextBox";
            this.rbdwGcTextBox.Text = "GcTextBox";
            // 
            // rbClearCellType
            // 
            this.rbClearCellType.Enabled = false;
            this.rbClearCellType.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbClearCellType.LargeImage")));
            this.rbClearCellType.Name = "rbClearCellType";
            this.rbClearCellType.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbClearCellType.SmallImage")));
            this.rbClearCellType.SupportedGroupSizing = C1.Win.C1Ribbon.SupportedGroupSizing.LargeImageOnly;
            this.rbClearCellType.Text = "Clear CellType";
            this.rbClearCellType.Click += new System.EventHandler(this.btnClearCellType_Click);
            // 
            // rpStyle
            // 
            this.rpStyle.Items.Add(this.rbConditionalFormat);
            this.rpStyle.Name = "rpStyle";
            this.rpStyle.Text = "Style";
            // 
            // rbConditionalFormat
            // 
            this.rbConditionalFormat.Items.Add(this.rbCFHighLightCellsRules);
            this.rbConditionalFormat.Items.Add(this.rbCFTopBottomRules);
            this.rbConditionalFormat.Items.Add(this.ribbonSeparator8);
            this.rbConditionalFormat.Items.Add(this.rbCFDataBars);
            this.rbConditionalFormat.Items.Add(this.rbCFColorScales);
            this.rbConditionalFormat.Items.Add(this.rbCFIconSet);
            this.rbConditionalFormat.Items.Add(this.ribbonSeparator10);
            this.rbConditionalFormat.Items.Add(this.rbCFNewRule);
            this.rbConditionalFormat.Items.Add(this.rbCFClearRules);
            this.rbConditionalFormat.Items.Add(this.rbCFManagerRules);
            this.rbConditionalFormat.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbConditionalFormat.LargeImage")));
            this.rbConditionalFormat.Name = "rbConditionalFormat";
            this.rbConditionalFormat.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbConditionalFormat.SmallImage")));
            this.rbConditionalFormat.Text = "Conditional Formatting";
            // 
            // rbCFHighLightCellsRules
            // 
            this.rbCFHighLightCellsRules.Items.Add(this.rbGreaterThan);
            this.rbCFHighLightCellsRules.Items.Add(this.rbLessThan);
            this.rbCFHighLightCellsRules.Items.Add(this.rbBetween);
            this.rbCFHighLightCellsRules.Items.Add(this.rbCFEqualTo);
            this.rbCFHighLightCellsRules.Items.Add(this.rbCFTextThatContains);
            this.rbCFHighLightCellsRules.Items.Add(this.rbCFADateOccurring);
            this.rbCFHighLightCellsRules.Items.Add(this.rbCFDuplicateValues);
            this.rbCFHighLightCellsRules.Items.Add(this.ribbonSeparator20);
            this.rbCFHighLightCellsRules.Items.Add(this.rbCFHighLightMoreRules);
            this.rbCFHighLightCellsRules.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFHighLightCellsRules.LargeImage")));
            this.rbCFHighLightCellsRules.Name = "rbCFHighLightCellsRules";
            this.rbCFHighLightCellsRules.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbCFHighLightCellsRules.SmallImage")));
            this.rbCFHighLightCellsRules.Text = "&Highlight Cell Rules";
            // 
            // rbGreaterThan
            // 
            this.rbGreaterThan.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbGreaterThan.LargeImage")));
            this.rbGreaterThan.Name = "rbGreaterThan";
            this.rbGreaterThan.Text = "&Greater Than...";
            // 
            // rbLessThan
            // 
            this.rbLessThan.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbLessThan.LargeImage")));
            this.rbLessThan.Name = "rbLessThan";
            this.rbLessThan.Text = "&Less Than...";
            // 
            // rbBetween
            // 
            this.rbBetween.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbBetween.LargeImage")));
            this.rbBetween.Name = "rbBetween";
            this.rbBetween.Text = "&Between...";
            // 
            // rbCFEqualTo
            // 
            this.rbCFEqualTo.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFEqualTo.LargeImage")));
            this.rbCFEqualTo.Name = "rbCFEqualTo";
            this.rbCFEqualTo.Text = "&Equal To...";
            // 
            // rbCFTextThatContains
            // 
            this.rbCFTextThatContains.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFTextThatContains.LargeImage")));
            this.rbCFTextThatContains.Name = "rbCFTextThatContains";
            this.rbCFTextThatContains.Text = "&Text That Contains...";
            // 
            // rbCFADateOccurring
            // 
            this.rbCFADateOccurring.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFADateOccurring.LargeImage")));
            this.rbCFADateOccurring.Name = "rbCFADateOccurring";
            this.rbCFADateOccurring.Text = "&A Date Occurring...";
            // 
            // rbCFDuplicateValues
            // 
            this.rbCFDuplicateValues.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFDuplicateValues.LargeImage")));
            this.rbCFDuplicateValues.Name = "rbCFDuplicateValues";
            this.rbCFDuplicateValues.Text = "&Duplicate Values...";
            // 
            // ribbonSeparator20
            // 
            this.ribbonSeparator20.Name = "ribbonSeparator20";
            // 
            // rbCFHighLightMoreRules
            // 
            this.rbCFHighLightMoreRules.Name = "rbCFHighLightMoreRules";
            this.rbCFHighLightMoreRules.Text = "&More Rules...";
            // 
            // rbCFTopBottomRules
            // 
            this.rbCFTopBottomRules.Items.Add(this.rbCFTop10Items);
            this.rbCFTopBottomRules.Items.Add(this.rbCFTop10Per);
            this.rbCFTopBottomRules.Items.Add(this.rbCFBottom10Items);
            this.rbCFTopBottomRules.Items.Add(this.rbCFBottom10Per);
            this.rbCFTopBottomRules.Items.Add(this.rbCFAboveAverage);
            this.rbCFTopBottomRules.Items.Add(this.rbCFBelowAverage);
            this.rbCFTopBottomRules.Items.Add(this.ribbonSeparator9);
            this.rbCFTopBottomRules.Items.Add(this.rbCFTopBottomMoreRules);
            this.rbCFTopBottomRules.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFTopBottomRules.LargeImage")));
            this.rbCFTopBottomRules.Name = "rbCFTopBottomRules";
            this.rbCFTopBottomRules.Text = "&Top/Bottom Rules";
            // 
            // rbCFTop10Items
            // 
            this.rbCFTop10Items.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFTop10Items.LargeImage")));
            this.rbCFTop10Items.Name = "rbCFTop10Items";
            this.rbCFTop10Items.Text = "&Top 10 Items...";
            // 
            // rbCFTop10Per
            // 
            this.rbCFTop10Per.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFTop10Per.LargeImage")));
            this.rbCFTop10Per.Name = "rbCFTop10Per";
            this.rbCFTop10Per.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbCFTop10Per.SmallImage")));
            this.rbCFTop10Per.Text = "To&p 10 %...";
            // 
            // rbCFBottom10Items
            // 
            this.rbCFBottom10Items.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFBottom10Items.LargeImage")));
            this.rbCFBottom10Items.Name = "rbCFBottom10Items";
            this.rbCFBottom10Items.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbCFBottom10Items.SmallImage")));
            this.rbCFBottom10Items.Text = "&Bottom 10 Items...";
            // 
            // rbCFBottom10Per
            // 
            this.rbCFBottom10Per.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFBottom10Per.LargeImage")));
            this.rbCFBottom10Per.Name = "rbCFBottom10Per";
            this.rbCFBottom10Per.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbCFBottom10Per.SmallImage")));
            this.rbCFBottom10Per.Text = "B&ottom 10 %...";
            // 
            // rbCFAboveAverage
            // 
            this.rbCFAboveAverage.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFAboveAverage.LargeImage")));
            this.rbCFAboveAverage.Name = "rbCFAboveAverage";
            this.rbCFAboveAverage.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbCFAboveAverage.SmallImage")));
            this.rbCFAboveAverage.Text = "&Above Average...";
            // 
            // rbCFBelowAverage
            // 
            this.rbCFBelowAverage.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFBelowAverage.LargeImage")));
            this.rbCFBelowAverage.Name = "rbCFBelowAverage";
            this.rbCFBelowAverage.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbCFBelowAverage.SmallImage")));
            this.rbCFBelowAverage.Text = "Below A&verage...";
            // 
            // ribbonSeparator9
            // 
            this.ribbonSeparator9.Name = "ribbonSeparator9";
            // 
            // rbCFTopBottomMoreRules
            // 
            this.rbCFTopBottomMoreRules.Name = "rbCFTopBottomMoreRules";
            this.rbCFTopBottomMoreRules.Text = "&More Rules...";
            // 
            // ribbonSeparator8
            // 
            this.ribbonSeparator8.Name = "ribbonSeparator8";
            // 
            // rbCFDataBars
            // 
            this.rbCFDataBars.Items.Add(this.rbCFGradientBlue);
            this.rbCFDataBars.Items.Add(this.rbCFGradientGreen);
            this.rbCFDataBars.Items.Add(this.rbCFGradientRed);
            this.rbCFDataBars.Items.Add(this.rbCFGradientOrange);
            this.rbCFDataBars.Items.Add(this.rbCFGradientLightBlue);
            this.rbCFDataBars.Items.Add(this.rbCFGradientPurple);
            this.rbCFDataBars.Items.Add(this.rbCFSolidBlue);
            this.rbCFDataBars.Items.Add(this.rbCFSolidGreen);
            this.rbCFDataBars.Items.Add(this.rbCFSolidRed);
            this.rbCFDataBars.Items.Add(this.rbCFSolidOrange);
            this.rbCFDataBars.Items.Add(this.rbCFSolidLightBlue);
            this.rbCFDataBars.Items.Add(this.rbCFSolidPurple);
            this.rbCFDataBars.ItemSize = new System.Drawing.Size(50, 50);
            this.rbCFDataBars.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFDataBars.LargeImage")));
            this.rbCFDataBars.MenuItems.Add(this.rbCFDataBarMoreRules);
            this.rbCFDataBars.Name = "rbCFDataBars";
            this.rbCFDataBars.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbCFDataBars.SmallImage")));
            this.rbCFDataBars.Text = "&Data Bars";
            // 
            // rbCFGradientBlue
            // 
            this.rbCFGradientBlue.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFGradientBlue.LargeImage")));
            this.rbCFGradientBlue.Name = "rbCFGradientBlue";
            // 
            // rbCFGradientGreen
            // 
            this.rbCFGradientGreen.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFGradientGreen.LargeImage")));
            this.rbCFGradientGreen.Name = "rbCFGradientGreen";
            // 
            // rbCFGradientRed
            // 
            this.rbCFGradientRed.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFGradientRed.LargeImage")));
            this.rbCFGradientRed.Name = "rbCFGradientRed";
            // 
            // rbCFGradientOrange
            // 
            this.rbCFGradientOrange.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFGradientOrange.LargeImage")));
            this.rbCFGradientOrange.Name = "rbCFGradientOrange";
            // 
            // rbCFGradientLightBlue
            // 
            this.rbCFGradientLightBlue.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFGradientLightBlue.LargeImage")));
            this.rbCFGradientLightBlue.Name = "rbCFGradientLightBlue";
            // 
            // rbCFGradientPurple
            // 
            this.rbCFGradientPurple.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFGradientPurple.LargeImage")));
            this.rbCFGradientPurple.Name = "rbCFGradientPurple";
            // 
            // rbCFSolidBlue
            // 
            this.rbCFSolidBlue.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFSolidBlue.LargeImage")));
            this.rbCFSolidBlue.Name = "rbCFSolidBlue";
            // 
            // rbCFSolidGreen
            // 
            this.rbCFSolidGreen.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFSolidGreen.LargeImage")));
            this.rbCFSolidGreen.Name = "rbCFSolidGreen";
            // 
            // rbCFSolidRed
            // 
            this.rbCFSolidRed.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFSolidRed.LargeImage")));
            this.rbCFSolidRed.Name = "rbCFSolidRed";
            // 
            // rbCFSolidOrange
            // 
            this.rbCFSolidOrange.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFSolidOrange.LargeImage")));
            this.rbCFSolidOrange.Name = "rbCFSolidOrange";
            // 
            // rbCFSolidLightBlue
            // 
            this.rbCFSolidLightBlue.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFSolidLightBlue.LargeImage")));
            this.rbCFSolidLightBlue.Name = "rbCFSolidLightBlue";
            // 
            // rbCFSolidPurple
            // 
            this.rbCFSolidPurple.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFSolidPurple.LargeImage")));
            this.rbCFSolidPurple.Name = "rbCFSolidPurple";
            // 
            // rbCFDataBarMoreRules
            // 
            this.rbCFDataBarMoreRules.Name = "rbCFDataBarMoreRules";
            this.rbCFDataBarMoreRules.Text = "&More Rules...";
            // 
            // rbCFColorScales
            // 
            this.rbCFColorScales.Items.Add(this.rbCFGreenYellowRed);
            this.rbCFColorScales.Items.Add(this.ItemRedYellowGreenScale);
            this.rbCFColorScales.Items.Add(this.ItemGreenWhiteRedScale);
            this.rbCFColorScales.Items.Add(this.ItemRedWhiteGreenScale);
            this.rbCFColorScales.Items.Add(this.ItemBlueWhiteRedScale);
            this.rbCFColorScales.Items.Add(this.ItemRedWhiteBlueScale);
            this.rbCFColorScales.Items.Add(this.ItemWhiteRedScale);
            this.rbCFColorScales.Items.Add(this.ItemRedWhiteScale);
            this.rbCFColorScales.Items.Add(this.ItemGreenWihte);
            this.rbCFColorScales.Items.Add(this.ItemWhiteGreen);
            this.rbCFColorScales.Items.Add(this.ItemGreenYellowScale);
            this.rbCFColorScales.Items.Add(this.ItemYellowGreenScale);
            this.rbCFColorScales.ItemSize = new System.Drawing.Size(48, 48);
            this.rbCFColorScales.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFColorScales.LargeImage")));
            this.rbCFColorScales.MenuItems.Add(this.rbCFColorScaleMoreRule);
            this.rbCFColorScales.Name = "rbCFColorScales";
            this.rbCFColorScales.Text = "Color &Scales";
            this.rbCFColorScales.VisibleItems = 4;
            // 
            // rbCFGreenYellowRed
            // 
            this.rbCFGreenYellowRed.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFGreenYellowRed.LargeImage")));
            this.rbCFGreenYellowRed.Name = "rbCFGreenYellowRed";
            // 
            // ItemRedYellowGreenScale
            // 
            this.ItemRedYellowGreenScale.LargeImage = ((System.Drawing.Image)(resources.GetObject("ItemRedYellowGreenScale.LargeImage")));
            this.ItemRedYellowGreenScale.Name = "ItemRedYellowGreenScale";
            // 
            // ItemGreenWhiteRedScale
            // 
            this.ItemGreenWhiteRedScale.LargeImage = ((System.Drawing.Image)(resources.GetObject("ItemGreenWhiteRedScale.LargeImage")));
            this.ItemGreenWhiteRedScale.Name = "ItemGreenWhiteRedScale";
            // 
            // ItemRedWhiteGreenScale
            // 
            this.ItemRedWhiteGreenScale.LargeImage = ((System.Drawing.Image)(resources.GetObject("ItemRedWhiteGreenScale.LargeImage")));
            this.ItemRedWhiteGreenScale.Name = "ItemRedWhiteGreenScale";
            // 
            // ItemBlueWhiteRedScale
            // 
            this.ItemBlueWhiteRedScale.LargeImage = ((System.Drawing.Image)(resources.GetObject("ItemBlueWhiteRedScale.LargeImage")));
            this.ItemBlueWhiteRedScale.Name = "ItemBlueWhiteRedScale";
            // 
            // ItemRedWhiteBlueScale
            // 
            this.ItemRedWhiteBlueScale.LargeImage = ((System.Drawing.Image)(resources.GetObject("ItemRedWhiteBlueScale.LargeImage")));
            this.ItemRedWhiteBlueScale.Name = "ItemRedWhiteBlueScale";
            // 
            // ItemWhiteRedScale
            // 
            this.ItemWhiteRedScale.LargeImage = ((System.Drawing.Image)(resources.GetObject("ItemWhiteRedScale.LargeImage")));
            this.ItemWhiteRedScale.Name = "ItemWhiteRedScale";
            // 
            // ItemRedWhiteScale
            // 
            this.ItemRedWhiteScale.LargeImage = ((System.Drawing.Image)(resources.GetObject("ItemRedWhiteScale.LargeImage")));
            this.ItemRedWhiteScale.Name = "ItemRedWhiteScale";
            // 
            // ItemGreenWihte
            // 
            this.ItemGreenWihte.LargeImage = ((System.Drawing.Image)(resources.GetObject("ItemGreenWihte.LargeImage")));
            this.ItemGreenWihte.Name = "ItemGreenWihte";
            // 
            // ItemWhiteGreen
            // 
            this.ItemWhiteGreen.LargeImage = ((System.Drawing.Image)(resources.GetObject("ItemWhiteGreen.LargeImage")));
            this.ItemWhiteGreen.Name = "ItemWhiteGreen";
            // 
            // ItemGreenYellowScale
            // 
            this.ItemGreenYellowScale.LargeImage = ((System.Drawing.Image)(resources.GetObject("ItemGreenYellowScale.LargeImage")));
            this.ItemGreenYellowScale.Name = "ItemGreenYellowScale";
            // 
            // ItemYellowGreenScale
            // 
            this.ItemYellowGreenScale.LargeImage = ((System.Drawing.Image)(resources.GetObject("ItemYellowGreenScale.LargeImage")));
            this.ItemYellowGreenScale.Name = "ItemYellowGreenScale";
            // 
            // rbCFColorScaleMoreRule
            // 
            this.rbCFColorScaleMoreRule.Name = "rbCFColorScaleMoreRule";
            this.rbCFColorScaleMoreRule.Text = "&More Rules...";
            // 
            // rbCFIconSet
            // 
            this.rbCFIconSet.Items.Add(this.rbCF3ArrowsColored);
            this.rbCFIconSet.Items.Add(this.rbCF3ArrowsGray);
            this.rbCFIconSet.Items.Add(this.rbCF3Triangles);
            this.rbCFIconSet.Items.Add(this.rbCF4ArrowsGray);
            this.rbCFIconSet.Items.Add(this.rbCF4ArrowsColored);
            this.rbCFIconSet.Items.Add(this.rbCF5ArrowsGray);
            this.rbCFIconSet.Items.Add(this.rbCF5Colored);
            this.rbCFIconSet.Items.Add(this.ribbonGalleryItem113);
            this.rbCFIconSet.Items.Add(this.rbCF3TrafficLightsUnRimmmed);
            this.rbCFIconSet.Items.Add(this.rbCF3TrafficLightsRimmmed);
            this.rbCFIconSet.Items.Add(this.rbCF3Sign);
            this.rbCFIconSet.Items.Add(this.rbCF4TracfficLightsUnRimmed);
            this.rbCFIconSet.Items.Add(this.rbCFRedToBlack);
            this.rbCFIconSet.Items.Add(this.ribbonGalleryItem122);
            this.rbCFIconSet.Items.Add(this.rbCF3SymbolsCircled);
            this.rbCFIconSet.Items.Add(this.rbCF3SymbolsUnCircled);
            this.rbCFIconSet.Items.Add(this.rbCF3Flags);
            this.rbCFIconSet.Items.Add(this.ribbonGalleryItem126);
            this.rbCFIconSet.Items.Add(this.rbCFStars);
            this.rbCFIconSet.Items.Add(this.rbCF4Ratings);
            this.rbCFIconSet.Items.Add(this.rbCFQuarsters);
            this.rbCFIconSet.Items.Add(this.rbCF5Ratings);
            this.rbCFIconSet.Items.Add(this.rbCF5Boxs);
            this.rbCFIconSet.ItemSize = new System.Drawing.Size(126, 30);
            this.rbCFIconSet.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFIconSet.LargeImage")));
            this.rbCFIconSet.MenuItems.Add(this.rbCFIconSetsMoreRule);
            this.rbCFIconSet.Name = "rbCFIconSet";
            this.rbCFIconSet.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbCFIconSet.SmallImage")));
            this.rbCFIconSet.Text = "&Icon Set";
            this.rbCFIconSet.VisibleItems = 2;
            // 
            // rbCF3ArrowsColored
            // 
            this.rbCF3ArrowsColored.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCF3ArrowsColored.LargeImage")));
            this.rbCF3ArrowsColored.Name = "rbCF3ArrowsColored";
            // 
            // rbCF3ArrowsGray
            // 
            this.rbCF3ArrowsGray.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCF3ArrowsGray.LargeImage")));
            this.rbCF3ArrowsGray.Name = "rbCF3ArrowsGray";
            // 
            // rbCF3Triangles
            // 
            this.rbCF3Triangles.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCF3Triangles.LargeImage")));
            this.rbCF3Triangles.Name = "rbCF3Triangles";
            // 
            // rbCF4ArrowsGray
            // 
            this.rbCF4ArrowsGray.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCF4ArrowsGray.LargeImage")));
            this.rbCF4ArrowsGray.Name = "rbCF4ArrowsGray";
            // 
            // rbCF4ArrowsColored
            // 
            this.rbCF4ArrowsColored.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCF4ArrowsColored.LargeImage")));
            this.rbCF4ArrowsColored.Name = "rbCF4ArrowsColored";
            // 
            // rbCF5ArrowsGray
            // 
            this.rbCF5ArrowsGray.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCF5ArrowsGray.LargeImage")));
            this.rbCF5ArrowsGray.Name = "rbCF5ArrowsGray";
            // 
            // rbCF5Colored
            // 
            this.rbCF5Colored.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCF5Colored.LargeImage")));
            this.rbCF5Colored.Name = "rbCF5Colored";
            // 
            // ribbonGalleryItem113
            // 
            this.ribbonGalleryItem113.Name = "ribbonGalleryItem113";
            // 
            // rbCF3TrafficLightsUnRimmmed
            // 
            this.rbCF3TrafficLightsUnRimmmed.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCF3TrafficLightsUnRimmmed.LargeImage")));
            this.rbCF3TrafficLightsUnRimmmed.Name = "rbCF3TrafficLightsUnRimmmed";
            // 
            // rbCF3TrafficLightsRimmmed
            // 
            this.rbCF3TrafficLightsRimmmed.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCF3TrafficLightsRimmmed.LargeImage")));
            this.rbCF3TrafficLightsRimmmed.Name = "rbCF3TrafficLightsRimmmed";
            // 
            // rbCF3Sign
            // 
            this.rbCF3Sign.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCF3Sign.LargeImage")));
            this.rbCF3Sign.Name = "rbCF3Sign";
            // 
            // rbCF4TracfficLightsUnRimmed
            // 
            this.rbCF4TracfficLightsUnRimmed.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCF4TracfficLightsUnRimmed.LargeImage")));
            this.rbCF4TracfficLightsUnRimmed.Name = "rbCF4TracfficLightsUnRimmed";
            // 
            // rbCFRedToBlack
            // 
            this.rbCFRedToBlack.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFRedToBlack.LargeImage")));
            this.rbCFRedToBlack.Name = "rbCFRedToBlack";
            // 
            // ribbonGalleryItem122
            // 
            this.ribbonGalleryItem122.Name = "ribbonGalleryItem122";
            // 
            // rbCF3SymbolsCircled
            // 
            this.rbCF3SymbolsCircled.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCF3SymbolsCircled.LargeImage")));
            this.rbCF3SymbolsCircled.Name = "rbCF3SymbolsCircled";
            // 
            // rbCF3SymbolsUnCircled
            // 
            this.rbCF3SymbolsUnCircled.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCF3SymbolsUnCircled.LargeImage")));
            this.rbCF3SymbolsUnCircled.Name = "rbCF3SymbolsUnCircled";
            // 
            // rbCF3Flags
            // 
            this.rbCF3Flags.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCF3Flags.LargeImage")));
            this.rbCF3Flags.Name = "rbCF3Flags";
            // 
            // ribbonGalleryItem126
            // 
            this.ribbonGalleryItem126.Name = "ribbonGalleryItem126";
            // 
            // rbCFStars
            // 
            this.rbCFStars.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFStars.LargeImage")));
            this.rbCFStars.Name = "rbCFStars";
            // 
            // rbCF4Ratings
            // 
            this.rbCF4Ratings.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCF4Ratings.LargeImage")));
            this.rbCF4Ratings.Name = "rbCF4Ratings";
            // 
            // rbCFQuarsters
            // 
            this.rbCFQuarsters.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFQuarsters.LargeImage")));
            this.rbCFQuarsters.Name = "rbCFQuarsters";
            // 
            // rbCF5Ratings
            // 
            this.rbCF5Ratings.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCF5Ratings.LargeImage")));
            this.rbCF5Ratings.Name = "rbCF5Ratings";
            // 
            // rbCF5Boxs
            // 
            this.rbCF5Boxs.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCF5Boxs.LargeImage")));
            this.rbCF5Boxs.Name = "rbCF5Boxs";
            // 
            // rbCFIconSetsMoreRule
            // 
            this.rbCFIconSetsMoreRule.Name = "rbCFIconSetsMoreRule";
            this.rbCFIconSetsMoreRule.Text = "&More Rules...";
            // 
            // ribbonSeparator10
            // 
            this.ribbonSeparator10.Name = "ribbonSeparator10";
            // 
            // rbCFNewRule
            // 
            this.rbCFNewRule.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFNewRule.LargeImage")));
            this.rbCFNewRule.Name = "rbCFNewRule";
            this.rbCFNewRule.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbCFNewRule.SmallImage")));
            this.rbCFNewRule.Text = "&New Rules...";
            // 
            // rbCFClearRules
            // 
            this.rbCFClearRules.Items.Add(this.rbCFClearFromSelectedCell);
            this.rbCFClearRules.Items.Add(this.rbCFClearEntireSheet);
            this.rbCFClearRules.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFClearRules.LargeImage")));
            this.rbCFClearRules.Name = "rbCFClearRules";
            this.rbCFClearRules.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbCFClearRules.SmallImage")));
            this.rbCFClearRules.Text = "&Clear Rules";
            // 
            // rbCFClearFromSelectedCell
            // 
            this.rbCFClearFromSelectedCell.Name = "rbCFClearFromSelectedCell";
            this.rbCFClearFromSelectedCell.Text = "Clear Rules From &Selected Cell";
            // 
            // rbCFClearEntireSheet
            // 
            this.rbCFClearEntireSheet.Name = "rbCFClearEntireSheet";
            this.rbCFClearEntireSheet.Text = "Clear Rules &Entire Sheet";
            // 
            // rbCFManagerRules
            // 
            this.rbCFManagerRules.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCFManagerRules.LargeImage")));
            this.rbCFManagerRules.Name = "rbCFManagerRules";
            this.rbCFManagerRules.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbCFManagerRules.SmallImage")));
            this.rbCFManagerRules.Text = "&Manage Rules...";
            // 
            // rpEditing
            // 
            this.rpEditing.Items.Add(this.rbLock);
            this.rpEditing.Items.Add(this.rbClearAll);
            this.rpEditing.Items.Add(this.rbSelectAll);
            this.rpEditing.Items.Add(this.ribbonSeparator7);
            this.rpEditing.Items.Add(this.rbSortFilter);
            this.rpEditing.Items.Add(this.rbFind2);
            this.rpEditing.Name = "rpEditing";
            this.rpEditing.Text = "Editing";
            // 
            // rbLock
            // 
            this.rbLock.Name = "rbLock";
            this.rbLock.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbLock.SmallImage")));
            this.rbLock.Text = "Lock";
            this.rbLock.Click += new System.EventHandler(this.btnLock_Click);
            // 
            // rbClearAll
            // 
            this.rbClearAll.Name = "rbClearAll";
            this.rbClearAll.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbClearAll.SmallImage")));
            this.rbClearAll.Text = "Clear All";
            this.rbClearAll.Click += new System.EventHandler(this.btnClearAll_Click);
            // 
            // rbSelectAll
            // 
            this.rbSelectAll.Items.Add(this.rbSelectAllSheet);
            this.rbSelectAll.Items.Add(this.rbSelectAllCells);
            this.rbSelectAll.Items.Add(this.rbSelectAllData);
            this.rbSelectAll.Name = "rbSelectAll";
            this.rbSelectAll.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSelectAll.SmallImage")));
            this.rbSelectAll.Text = "Select All";
            // 
            // rbSelectAllSheet
            // 
            this.rbSelectAllSheet.Name = "rbSelectAllSheet";
            this.rbSelectAllSheet.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSelectAllSheet.SmallImage")));
            this.rbSelectAllSheet.Text = "&Sheet";
            // 
            // rbSelectAllCells
            // 
            this.rbSelectAllCells.Name = "rbSelectAllCells";
            this.rbSelectAllCells.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSelectAllCells.SmallImage")));
            this.rbSelectAllCells.Text = "&Cells";
            // 
            // rbSelectAllData
            // 
            this.rbSelectAllData.Name = "rbSelectAllData";
            this.rbSelectAllData.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSelectAllData.SmallImage")));
            this.rbSelectAllData.Text = "&Data";
            // 
            // ribbonSeparator7
            // 
            this.ribbonSeparator7.Name = "ribbonSeparator7";
            // 
            // rbSortFilter
            // 
            this.rbSortFilter.Items.Add(this.rbSortAToZ);
            this.rbSortFilter.Items.Add(this.rbSortZToA);
            this.rbSortFilter.Items.Add(this.rbCustomSort);
            this.rbSortFilter.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbSortFilter.LargeImage")));
            this.rbSortFilter.Name = "rbSortFilter";
            this.rbSortFilter.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSortFilter.SmallImage")));
            this.rbSortFilter.Text = "Sort";
            this.rbSortFilter.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbSortFilter.Click += new System.EventHandler(this.btnMenuSortAToZ_Click);
            // 
            // rbSortAToZ
            // 
            this.rbSortAToZ.Name = "rbSortAToZ";
            this.rbSortAToZ.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSortAToZ.SmallImage")));
            this.rbSortAToZ.Text = "&Sort A to Z";
            // 
            // rbSortZToA
            // 
            this.rbSortZToA.Name = "rbSortZToA";
            this.rbSortZToA.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSortZToA.SmallImage")));
            this.rbSortZToA.Text = "S&ort Z to A";
            // 
            // rbCustomSort
            // 
            this.rbCustomSort.Name = "rbCustomSort";
            this.rbCustomSort.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbCustomSort.SmallImage")));
            this.rbCustomSort.Text = "C&ustom Sort...";
            // 
            // rbFind2
            // 
            this.rbFind2.Items.Add(this.rbFind);
            this.rbFind2.Items.Add(this.rbGoto);
            this.rbFind2.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbFind2.LargeImage")));
            this.rbFind2.Name = "rbFind2";
            this.rbFind2.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbFind2.SmallImage")));
            this.rbFind2.Text = "Find";
            this.rbFind2.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            // 
            // rbFind
            // 
            this.rbFind.Name = "rbFind";
            this.rbFind.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbFind.SmallImage")));
            this.rbFind.Text = "&Find...";
            // 
            // rbGoto
            // 
            this.rbGoto.Name = "rbGoto";
            this.rbGoto.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbGoto.SmallImage")));
            this.rbGoto.Text = "&Goto...";
            // 
            // rtInsert
            // 
            this.rtInsert.Groups.Add(this.rpTables);
            this.rtInsert.Groups.Add(this.rpText);
            this.rtInsert.Groups.Add(this.rplllustrations);
            this.rtInsert.Groups.Add(this.rpChart);
            this.rtInsert.Groups.Add(this.rpDelete);
            this.rtInsert.Groups.Add(this.rpSparkline);
            this.rtInsert.Groups.Add(this.rpCameraShape);
            this.rtInsert.Name = "rtInsert";
            this.rtInsert.Text = "INSERT";
            // 
            // rpTables
            // 
            this.rpTables.Items.Add(this.rbTable);
            this.rpTables.Name = "rpTables";
            this.rpTables.Text = "Tables";
            // 
            // rbTable
            // 
            this.rbTable.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbTable.LargeImage")));
            this.rbTable.Name = "rbTable";
            this.rbTable.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbTable.SmallImage")));
            this.rbTable.Text = "Table";
            this.rbTable.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            // 
            // rpText
            // 
            this.rpText.Items.Add(this.rbSymbol);
            this.rpText.Items.Add(this.rbWordArt);
            this.rpText.Name = "rpText";
            this.rpText.Text = "Text";
            // 
            // rbSymbol
            // 
            this.rbSymbol.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbSymbol.LargeImage")));
            this.rbSymbol.Name = "rbSymbol";
            this.rbSymbol.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSymbol.SmallImage")));
            this.rbSymbol.Text = "Symbol";
            this.rbSymbol.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            // 
            // rbWordArt
            // 
            this.rbWordArt.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbWordArt.LargeImage")));
            this.rbWordArt.Name = "rbWordArt";
            this.rbWordArt.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbWordArt.SmallImage")));
            this.rbWordArt.Text = "WordArt";
            this.rbWordArt.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            // 
            // rplllustrations
            // 
            this.rplllustrations.Items.Add(this.rbPicture);
            this.rplllustrations.Items.Add(this.rbShapes);
            this.rplllustrations.Items.Add(this.rbAnnotactionMode);
            this.rplllustrations.Name = "rplllustrations";
            this.rplllustrations.Text = "Illustration";
            // 
            // rbPicture
            // 
            this.rbPicture.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbPicture.LargeImage")));
            this.rbPicture.Name = "rbPicture";
            this.rbPicture.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbPicture.SmallImage")));
            this.rbPicture.Text = "Picture";
            this.rbPicture.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbPicture.Click += new System.EventHandler(this.btnPicture_Click);
            // 
            // rbShapes
            // 
            this.rbShapes.Expanded = false;
            this.rbShapes.Items.Add(this.Shape11);
            this.rbShapes.Items.Add(this.Shape12);
            this.rbShapes.Items.Add(this.ribbonGalleryItem143);
            this.rbShapes.Items.Add(this.ribbonGalleryItem144);
            this.rbShapes.Items.Add(this.ribbonGalleryItem145);
            this.rbShapes.Items.Add(this.ribbonGalleryItem146);
            this.rbShapes.Items.Add(this.ribbonGalleryItem147);
            this.rbShapes.Items.Add(this.Shape21);
            this.rbShapes.Items.Add(this.Shape22);
            this.rbShapes.Items.Add(this.Shape23);
            this.rbShapes.Items.Add(this.Shape24);
            this.rbShapes.Items.Add(this.Shape25);
            this.rbShapes.Items.Add(this.Shape26);
            this.rbShapes.Items.Add(this.Shape27);
            this.rbShapes.Items.Add(this.Shape31);
            this.rbShapes.Items.Add(this.Shape32);
            this.rbShapes.Items.Add(this.Shape33);
            this.rbShapes.Items.Add(this.Shape34);
            this.rbShapes.Items.Add(this.Shape35);
            this.rbShapes.Items.Add(this.Shape36);
            this.rbShapes.Items.Add(this.Shape37);
            this.rbShapes.Items.Add(this.Shape41);
            this.rbShapes.Items.Add(this.Shape42);
            this.rbShapes.Items.Add(this.Shape43);
            this.rbShapes.Items.Add(this.Shape44);
            this.rbShapes.Items.Add(this.ribbonGalleryItem165);
            this.rbShapes.Items.Add(this.ribbonGalleryItem166);
            this.rbShapes.Items.Add(this.ribbonGalleryItem167);
            this.rbShapes.Items.Add(this.Shape51);
            this.rbShapes.Items.Add(this.Shape52);
            this.rbShapes.Items.Add(this.Shape53);
            this.rbShapes.Items.Add(this.Shape54);
            this.rbShapes.Items.Add(this.Shape55);
            this.rbShapes.Items.Add(this.Shape56);
            this.rbShapes.Items.Add(this.Shape57);
            this.rbShapes.Items.Add(this.Shape61);
            this.rbShapes.Items.Add(this.Shape62);
            this.rbShapes.Items.Add(this.Shape63);
            this.rbShapes.Items.Add(this.Shape64);
            this.rbShapes.Items.Add(this.Shape65);
            this.rbShapes.ItemSize = new System.Drawing.Size(25, 23);
            this.rbShapes.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbShapes.LargeImage")));
            this.rbShapes.Name = "rbShapes";
            this.rbShapes.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbShapes.SmallImage")));
            this.rbShapes.Text = "Shapes";
            this.rbShapes.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbShapes.VisibleItems = 7;
            // 
            // Shape11
            // 
            this.Shape11.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape11.LargeImage")));
            this.Shape11.Name = "Shape11";
            // 
            // Shape12
            // 
            this.Shape12.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape12.LargeImage")));
            this.Shape12.Name = "Shape12";
            // 
            // ribbonGalleryItem143
            // 
            this.ribbonGalleryItem143.Name = "ribbonGalleryItem143";
            // 
            // ribbonGalleryItem144
            // 
            this.ribbonGalleryItem144.Name = "ribbonGalleryItem144";
            // 
            // ribbonGalleryItem145
            // 
            this.ribbonGalleryItem145.Name = "ribbonGalleryItem145";
            // 
            // ribbonGalleryItem146
            // 
            this.ribbonGalleryItem146.Name = "ribbonGalleryItem146";
            // 
            // ribbonGalleryItem147
            // 
            this.ribbonGalleryItem147.Name = "ribbonGalleryItem147";
            // 
            // Shape21
            // 
            this.Shape21.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape21.LargeImage")));
            this.Shape21.Name = "Shape21";
            // 
            // Shape22
            // 
            this.Shape22.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape22.LargeImage")));
            this.Shape22.Name = "Shape22";
            // 
            // Shape23
            // 
            this.Shape23.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape23.LargeImage")));
            this.Shape23.Name = "Shape23";
            // 
            // Shape24
            // 
            this.Shape24.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape24.LargeImage")));
            this.Shape24.Name = "Shape24";
            // 
            // Shape25
            // 
            this.Shape25.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape25.LargeImage")));
            this.Shape25.Name = "Shape25";
            // 
            // Shape26
            // 
            this.Shape26.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape26.LargeImage")));
            this.Shape26.Name = "Shape26";
            // 
            // Shape27
            // 
            this.Shape27.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape27.LargeImage")));
            this.Shape27.Name = "Shape27";
            // 
            // Shape31
            // 
            this.Shape31.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape31.LargeImage")));
            this.Shape31.Name = "Shape31";
            // 
            // Shape32
            // 
            this.Shape32.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape32.LargeImage")));
            this.Shape32.Name = "Shape32";
            // 
            // Shape33
            // 
            this.Shape33.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape33.LargeImage")));
            this.Shape33.Name = "Shape33";
            // 
            // Shape34
            // 
            this.Shape34.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape34.LargeImage")));
            this.Shape34.Name = "Shape34";
            // 
            // Shape35
            // 
            this.Shape35.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape35.LargeImage")));
            this.Shape35.Name = "Shape35";
            // 
            // Shape36
            // 
            this.Shape36.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape36.LargeImage")));
            this.Shape36.Name = "Shape36";
            // 
            // Shape37
            // 
            this.Shape37.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape37.LargeImage")));
            this.Shape37.Name = "Shape37";
            // 
            // Shape41
            // 
            this.Shape41.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape41.LargeImage")));
            this.Shape41.Name = "Shape41";
            // 
            // Shape42
            // 
            this.Shape42.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape42.LargeImage")));
            this.Shape42.Name = "Shape42";
            // 
            // Shape43
            // 
            this.Shape43.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape43.LargeImage")));
            this.Shape43.Name = "Shape43";
            // 
            // Shape44
            // 
            this.Shape44.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape44.LargeImage")));
            this.Shape44.Name = "Shape44";
            // 
            // ribbonGalleryItem165
            // 
            this.ribbonGalleryItem165.Name = "ribbonGalleryItem165";
            // 
            // ribbonGalleryItem166
            // 
            this.ribbonGalleryItem166.Name = "ribbonGalleryItem166";
            // 
            // ribbonGalleryItem167
            // 
            this.ribbonGalleryItem167.Name = "ribbonGalleryItem167";
            // 
            // Shape51
            // 
            this.Shape51.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape51.LargeImage")));
            this.Shape51.Name = "Shape51";
            // 
            // Shape52
            // 
            this.Shape52.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape52.LargeImage")));
            this.Shape52.Name = "Shape52";
            // 
            // Shape53
            // 
            this.Shape53.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape53.LargeImage")));
            this.Shape53.Name = "Shape53";
            // 
            // Shape54
            // 
            this.Shape54.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape54.LargeImage")));
            this.Shape54.Name = "Shape54";
            // 
            // Shape55
            // 
            this.Shape55.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape55.LargeImage")));
            this.Shape55.Name = "Shape55";
            // 
            // Shape56
            // 
            this.Shape56.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape56.LargeImage")));
            this.Shape56.Name = "Shape56";
            // 
            // Shape57
            // 
            this.Shape57.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape57.LargeImage")));
            this.Shape57.Name = "Shape57";
            // 
            // Shape61
            // 
            this.Shape61.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape61.LargeImage")));
            this.Shape61.Name = "Shape61";
            // 
            // Shape62
            // 
            this.Shape62.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape62.LargeImage")));
            this.Shape62.Name = "Shape62";
            // 
            // Shape63
            // 
            this.Shape63.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape63.LargeImage")));
            this.Shape63.Name = "Shape63";
            // 
            // Shape64
            // 
            this.Shape64.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape64.LargeImage")));
            this.Shape64.Name = "Shape64";
            // 
            // Shape65
            // 
            this.Shape65.LargeImage = ((System.Drawing.Image)(resources.GetObject("Shape65.LargeImage")));
            this.Shape65.Name = "Shape65";
            // 
            // rbAnnotactionMode
            // 
            this.rbAnnotactionMode.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbAnnotactionMode.LargeImage")));
            this.rbAnnotactionMode.Name = "rbAnnotactionMode";
            this.rbAnnotactionMode.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbAnnotactionMode.SmallImage")));
            this.rbAnnotactionMode.Text = "Annotation Mode";
            this.rbAnnotactionMode.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbAnnotactionMode.Click += new System.EventHandler(this.btnAnnotionMode_Click);
            // 
            // rpChart
            // 
            this.rpChart.Items.Add(this.RibbonButtonChartColumn);
            this.rpChart.Items.Add(this.RibbonButtonChartLine);
            this.rpChart.Items.Add(this.RibbonButtonChartPie);
            this.rpChart.Items.Add(this.RibbonButtonChartBar);
            this.rpChart.Items.Add(this.RibbonButtonChartArea);
            this.rpChart.Items.Add(this.RibbonButtonChartScatter);
            this.rpChart.Items.Add(this.RibbonButtonChartOthers);
            this.rpChart.Name = "rpChart";
            this.rpChart.Text = "Charts";
            // 
            // RibbonButtonChartColumn
            // 
            this.RibbonButtonChartColumn.Expanded = false;
            this.RibbonButtonChartColumn.Items.Add(this.ClusteredColumnChart);
            this.RibbonButtonChartColumn.Items.Add(this.StackedColumnChart);
            this.RibbonButtonChartColumn.Items.Add(this.Stacked100ColumnChart);
            this.RibbonButtonChartColumn.Items.Add(this.HighLowColumnChart);
            this.RibbonButtonChartColumn.Items.Add(this.ClusteredColumnIn3DChart);
            this.RibbonButtonChartColumn.Items.Add(this.StackedColumnIn3DChart);
            this.RibbonButtonChartColumn.Items.Add(this.Stacked100ColumnIn3DChart);
            this.RibbonButtonChartColumn.Items.Add(this.ColumnIn3DChart);
            this.RibbonButtonChartColumn.Items.Add(this.ClusteredCylinderChart);
            this.RibbonButtonChartColumn.Items.Add(this.StackedCylinderChart);
            this.RibbonButtonChartColumn.Items.Add(this.Stacked100CylinderChart);
            this.RibbonButtonChartColumn.Items.Add(this.CylinderIn3DChart);
            this.RibbonButtonChartColumn.Items.Add(this.ClusteredFullConeChart);
            this.RibbonButtonChartColumn.Items.Add(this.StackedFullConeChart);
            this.RibbonButtonChartColumn.Items.Add(this.Stacked100FullConeChart);
            this.RibbonButtonChartColumn.Items.Add(this.FullConeIn3DChart);
            this.RibbonButtonChartColumn.Items.Add(this.ClusteredFullPyramidChart);
            this.RibbonButtonChartColumn.Items.Add(this.StackedFullPyramidChart);
            this.RibbonButtonChartColumn.Items.Add(this.Stacked100FullPyramidChart);
            this.RibbonButtonChartColumn.Items.Add(this.FullPyramidIn3DChart);
            this.RibbonButtonChartColumn.ItemSize = new System.Drawing.Size(60, 60);
            this.RibbonButtonChartColumn.LargeImage = ((System.Drawing.Image)(resources.GetObject("RibbonButtonChartColumn.LargeImage")));
            this.RibbonButtonChartColumn.MenuItems.Add(this.RibbonButton10);
            this.RibbonButtonChartColumn.Name = "RibbonButtonChartColumn";
            this.RibbonButtonChartColumn.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButtonChartColumn.SmallImage")));
            this.RibbonButtonChartColumn.Text = "Column";
            this.RibbonButtonChartColumn.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.RibbonButtonChartColumn.VisibleItems = 4;
            // 
            // ClusteredColumnChart
            // 
            this.ClusteredColumnChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("ClusteredColumnChart.LargeImage")));
            this.ClusteredColumnChart.Name = "ClusteredColumnChart";
            // 
            // StackedColumnChart
            // 
            this.StackedColumnChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("StackedColumnChart.LargeImage")));
            this.StackedColumnChart.Name = "StackedColumnChart";
            // 
            // Stacked100ColumnChart
            // 
            this.Stacked100ColumnChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("Stacked100ColumnChart.LargeImage")));
            this.Stacked100ColumnChart.Name = "Stacked100ColumnChart";
            // 
            // HighLowColumnChart
            // 
            this.HighLowColumnChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("HighLowColumnChart.LargeImage")));
            this.HighLowColumnChart.Name = "HighLowColumnChart";
            // 
            // ClusteredColumnIn3DChart
            // 
            this.ClusteredColumnIn3DChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("ClusteredColumnIn3DChart.LargeImage")));
            this.ClusteredColumnIn3DChart.Name = "ClusteredColumnIn3DChart";
            // 
            // StackedColumnIn3DChart
            // 
            this.StackedColumnIn3DChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("StackedColumnIn3DChart.LargeImage")));
            this.StackedColumnIn3DChart.Name = "StackedColumnIn3DChart";
            // 
            // Stacked100ColumnIn3DChart
            // 
            this.Stacked100ColumnIn3DChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("Stacked100ColumnIn3DChart.LargeImage")));
            this.Stacked100ColumnIn3DChart.Name = "Stacked100ColumnIn3DChart";
            // 
            // ColumnIn3DChart
            // 
            this.ColumnIn3DChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("ColumnIn3DChart.LargeImage")));
            this.ColumnIn3DChart.Name = "ColumnIn3DChart";
            // 
            // ClusteredCylinderChart
            // 
            this.ClusteredCylinderChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("ClusteredCylinderChart.LargeImage")));
            this.ClusteredCylinderChart.Name = "ClusteredCylinderChart";
            // 
            // StackedCylinderChart
            // 
            this.StackedCylinderChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("StackedCylinderChart.LargeImage")));
            this.StackedCylinderChart.Name = "StackedCylinderChart";
            // 
            // Stacked100CylinderChart
            // 
            this.Stacked100CylinderChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("Stacked100CylinderChart.LargeImage")));
            this.Stacked100CylinderChart.Name = "Stacked100CylinderChart";
            // 
            // CylinderIn3DChart
            // 
            this.CylinderIn3DChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("CylinderIn3DChart.LargeImage")));
            this.CylinderIn3DChart.Name = "CylinderIn3DChart";
            // 
            // ClusteredFullConeChart
            // 
            this.ClusteredFullConeChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("ClusteredFullConeChart.LargeImage")));
            this.ClusteredFullConeChart.Name = "ClusteredFullConeChart";
            // 
            // StackedFullConeChart
            // 
            this.StackedFullConeChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("StackedFullConeChart.LargeImage")));
            this.StackedFullConeChart.Name = "StackedFullConeChart";
            // 
            // Stacked100FullConeChart
            // 
            this.Stacked100FullConeChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("Stacked100FullConeChart.LargeImage")));
            this.Stacked100FullConeChart.Name = "Stacked100FullConeChart";
            // 
            // FullConeIn3DChart
            // 
            this.FullConeIn3DChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("FullConeIn3DChart.LargeImage")));
            this.FullConeIn3DChart.Name = "FullConeIn3DChart";
            // 
            // ClusteredFullPyramidChart
            // 
            this.ClusteredFullPyramidChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("ClusteredFullPyramidChart.LargeImage")));
            this.ClusteredFullPyramidChart.Name = "ClusteredFullPyramidChart";
            // 
            // StackedFullPyramidChart
            // 
            this.StackedFullPyramidChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("StackedFullPyramidChart.LargeImage")));
            this.StackedFullPyramidChart.Name = "StackedFullPyramidChart";
            // 
            // Stacked100FullPyramidChart
            // 
            this.Stacked100FullPyramidChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("Stacked100FullPyramidChart.LargeImage")));
            this.Stacked100FullPyramidChart.Name = "Stacked100FullPyramidChart";
            // 
            // FullPyramidIn3DChart
            // 
            this.FullPyramidIn3DChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("FullPyramidIn3DChart.LargeImage")));
            this.FullPyramidIn3DChart.Name = "FullPyramidIn3DChart";
            // 
            // RibbonButton10
            // 
            this.RibbonButton10.LargeImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton10.LargeImage")));
            this.RibbonButton10.Name = "RibbonButton10";
            this.RibbonButton10.Text = "&All Chart Types...";
            // 
            // RibbonButtonChartLine
            // 
            this.RibbonButtonChartLine.Expanded = false;
            this.RibbonButtonChartLine.Items.Add(this.LineIn2DChart);
            this.RibbonButtonChartLine.Items.Add(this.StackedLineChart);
            this.RibbonButtonChartLine.Items.Add(this.Stacked100LineChart);
            this.RibbonButtonChartLine.Items.Add(this.LineWithMarkersChart);
            this.RibbonButtonChartLine.Items.Add(this.StackedLineWithMarkersChart);
            this.RibbonButtonChartLine.Items.Add(this.Stacked100LineWithMarkersChart);
            this.RibbonButtonChartLine.Items.Add(this.LineIn3DChart);
            this.RibbonButtonChartLine.ItemSize = new System.Drawing.Size(60, 60);
            this.RibbonButtonChartLine.LargeImage = ((System.Drawing.Image)(resources.GetObject("RibbonButtonChartLine.LargeImage")));
            this.RibbonButtonChartLine.MenuItems.Add(this.RibbonButton11);
            this.RibbonButtonChartLine.Name = "RibbonButtonChartLine";
            this.RibbonButtonChartLine.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButtonChartLine.SmallImage")));
            this.RibbonButtonChartLine.Text = "Line";
            this.RibbonButtonChartLine.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            // 
            // LineIn2DChart
            // 
            this.LineIn2DChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("LineIn2DChart.LargeImage")));
            this.LineIn2DChart.Name = "LineIn2DChart";
            // 
            // StackedLineChart
            // 
            this.StackedLineChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("StackedLineChart.LargeImage")));
            this.StackedLineChart.Name = "StackedLineChart";
            // 
            // Stacked100LineChart
            // 
            this.Stacked100LineChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("Stacked100LineChart.LargeImage")));
            this.Stacked100LineChart.Name = "Stacked100LineChart";
            // 
            // LineWithMarkersChart
            // 
            this.LineWithMarkersChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("LineWithMarkersChart.LargeImage")));
            this.LineWithMarkersChart.Name = "LineWithMarkersChart";
            // 
            // StackedLineWithMarkersChart
            // 
            this.StackedLineWithMarkersChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("StackedLineWithMarkersChart.LargeImage")));
            this.StackedLineWithMarkersChart.Name = "StackedLineWithMarkersChart";
            // 
            // Stacked100LineWithMarkersChart
            // 
            this.Stacked100LineWithMarkersChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("Stacked100LineWithMarkersChart.LargeImage")));
            this.Stacked100LineWithMarkersChart.Name = "Stacked100LineWithMarkersChart";
            // 
            // LineIn3DChart
            // 
            this.LineIn3DChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("LineIn3DChart.LargeImage")));
            this.LineIn3DChart.Name = "LineIn3DChart";
            // 
            // RibbonButton11
            // 
            this.RibbonButton11.LargeImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton11.LargeImage")));
            this.RibbonButton11.Name = "RibbonButton11";
            this.RibbonButton11.Text = "&All Chart Types...";
            // 
            // RibbonButtonChartPie
            // 
            this.RibbonButtonChartPie.Expanded = false;
            this.RibbonButtonChartPie.Items.Add(this.PieIn2DChart);
            this.RibbonButtonChartPie.Items.Add(this.ExplodedPieIn2DChart);
            this.RibbonButtonChartPie.Items.Add(this.PieIn3DChart);
            this.RibbonButtonChartPie.Items.Add(this.ExplodedPieIn3DChart);
            this.RibbonButtonChartPie.ItemSize = new System.Drawing.Size(60, 60);
            this.RibbonButtonChartPie.LargeImage = ((System.Drawing.Image)(resources.GetObject("RibbonButtonChartPie.LargeImage")));
            this.RibbonButtonChartPie.MenuItems.Add(this.RibbonButton12);
            this.RibbonButtonChartPie.Name = "RibbonButtonChartPie";
            this.RibbonButtonChartPie.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButtonChartPie.SmallImage")));
            this.RibbonButtonChartPie.Text = "Pie";
            this.RibbonButtonChartPie.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.RibbonButtonChartPie.VisibleItems = 2;
            // 
            // PieIn2DChart
            // 
            this.PieIn2DChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("PieIn2DChart.LargeImage")));
            this.PieIn2DChart.Name = "PieIn2DChart";
            // 
            // ExplodedPieIn2DChart
            // 
            this.ExplodedPieIn2DChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("ExplodedPieIn2DChart.LargeImage")));
            this.ExplodedPieIn2DChart.Name = "ExplodedPieIn2DChart";
            // 
            // PieIn3DChart
            // 
            this.PieIn3DChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("PieIn3DChart.LargeImage")));
            this.PieIn3DChart.Name = "PieIn3DChart";
            // 
            // ExplodedPieIn3DChart
            // 
            this.ExplodedPieIn3DChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("ExplodedPieIn3DChart.LargeImage")));
            this.ExplodedPieIn3DChart.Name = "ExplodedPieIn3DChart";
            // 
            // RibbonButton12
            // 
            this.RibbonButton12.LargeImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton12.LargeImage")));
            this.RibbonButton12.Name = "RibbonButton12";
            this.RibbonButton12.Text = "&All Chart Types...";
            // 
            // RibbonButtonChartBar
            // 
            this.RibbonButtonChartBar.Expanded = false;
            this.RibbonButtonChartBar.Items.Add(this.ClusteredBarChart);
            this.RibbonButtonChartBar.Items.Add(this.StackedBarChart);
            this.RibbonButtonChartBar.Items.Add(this.Stacked100BarChart);
            this.RibbonButtonChartBar.Items.Add(this.ClusteredBarIn3DChart);
            this.RibbonButtonChartBar.Items.Add(this.StackedBarIn3DChart);
            this.RibbonButtonChartBar.Items.Add(this.Stacked100BarIn3DChart);
            this.RibbonButtonChartBar.Items.Add(this.ClusteredHorizontalCylinderChart);
            this.RibbonButtonChartBar.Items.Add(this.StackedHorizontalCylinderChart);
            this.RibbonButtonChartBar.Items.Add(this.Stacked100HorizontalCylinderChart);
            this.RibbonButtonChartBar.Items.Add(this.ClusteredHorizontalFullConeChart);
            this.RibbonButtonChartBar.Items.Add(this.StackedHorizontalFullConeChart);
            this.RibbonButtonChartBar.Items.Add(this.Stacked100HorizontalFullConeChart);
            this.RibbonButtonChartBar.Items.Add(this.ClusteredHorizontalFullPyramidChart);
            this.RibbonButtonChartBar.Items.Add(this.Stacked100HorizontalFullPyramidChart);
            this.RibbonButtonChartBar.Items.Add(this.HighLowBarFullPyramidChart);
            this.RibbonButtonChartBar.ItemSize = new System.Drawing.Size(60, 60);
            this.RibbonButtonChartBar.LargeImage = ((System.Drawing.Image)(resources.GetObject("RibbonButtonChartBar.LargeImage")));
            this.RibbonButtonChartBar.MenuItems.Add(this.RibbonButton13);
            this.RibbonButtonChartBar.Name = "RibbonButtonChartBar";
            this.RibbonButtonChartBar.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButtonChartBar.SmallImage")));
            this.RibbonButtonChartBar.Text = "Bar";
            this.RibbonButtonChartBar.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            // 
            // ClusteredBarChart
            // 
            this.ClusteredBarChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("ClusteredBarChart.LargeImage")));
            this.ClusteredBarChart.Name = "ClusteredBarChart";
            // 
            // StackedBarChart
            // 
            this.StackedBarChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("StackedBarChart.LargeImage")));
            this.StackedBarChart.Name = "StackedBarChart";
            // 
            // Stacked100BarChart
            // 
            this.Stacked100BarChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("Stacked100BarChart.LargeImage")));
            this.Stacked100BarChart.Name = "Stacked100BarChart";
            // 
            // ClusteredBarIn3DChart
            // 
            this.ClusteredBarIn3DChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("ClusteredBarIn3DChart.LargeImage")));
            this.ClusteredBarIn3DChart.Name = "ClusteredBarIn3DChart";
            // 
            // StackedBarIn3DChart
            // 
            this.StackedBarIn3DChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("StackedBarIn3DChart.LargeImage")));
            this.StackedBarIn3DChart.Name = "StackedBarIn3DChart";
            // 
            // Stacked100BarIn3DChart
            // 
            this.Stacked100BarIn3DChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("Stacked100BarIn3DChart.LargeImage")));
            this.Stacked100BarIn3DChart.Name = "Stacked100BarIn3DChart";
            // 
            // ClusteredHorizontalCylinderChart
            // 
            this.ClusteredHorizontalCylinderChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("ClusteredHorizontalCylinderChart.LargeImage")));
            this.ClusteredHorizontalCylinderChart.Name = "ClusteredHorizontalCylinderChart";
            // 
            // StackedHorizontalCylinderChart
            // 
            this.StackedHorizontalCylinderChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("StackedHorizontalCylinderChart.LargeImage")));
            this.StackedHorizontalCylinderChart.Name = "StackedHorizontalCylinderChart";
            // 
            // Stacked100HorizontalCylinderChart
            // 
            this.Stacked100HorizontalCylinderChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("Stacked100HorizontalCylinderChart.LargeImage")));
            this.Stacked100HorizontalCylinderChart.Name = "Stacked100HorizontalCylinderChart";
            // 
            // ClusteredHorizontalFullConeChart
            // 
            this.ClusteredHorizontalFullConeChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("ClusteredHorizontalFullConeChart.LargeImage")));
            this.ClusteredHorizontalFullConeChart.Name = "ClusteredHorizontalFullConeChart";
            // 
            // StackedHorizontalFullConeChart
            // 
            this.StackedHorizontalFullConeChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("StackedHorizontalFullConeChart.LargeImage")));
            this.StackedHorizontalFullConeChart.Name = "StackedHorizontalFullConeChart";
            // 
            // Stacked100HorizontalFullConeChart
            // 
            this.Stacked100HorizontalFullConeChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("Stacked100HorizontalFullConeChart.LargeImage")));
            this.Stacked100HorizontalFullConeChart.Name = "Stacked100HorizontalFullConeChart";
            // 
            // ClusteredHorizontalFullPyramidChart
            // 
            this.ClusteredHorizontalFullPyramidChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("ClusteredHorizontalFullPyramidChart.LargeImage")));
            this.ClusteredHorizontalFullPyramidChart.Name = "ClusteredHorizontalFullPyramidChart";
            // 
            // Stacked100HorizontalFullPyramidChart
            // 
            this.Stacked100HorizontalFullPyramidChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("Stacked100HorizontalFullPyramidChart.LargeImage")));
            this.Stacked100HorizontalFullPyramidChart.Name = "Stacked100HorizontalFullPyramidChart";
            // 
            // HighLowBarFullPyramidChart
            // 
            this.HighLowBarFullPyramidChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("HighLowBarFullPyramidChart.LargeImage")));
            this.HighLowBarFullPyramidChart.Name = "HighLowBarFullPyramidChart";
            // 
            // RibbonButton13
            // 
            this.RibbonButton13.LargeImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton13.LargeImage")));
            this.RibbonButton13.Name = "RibbonButton13";
            this.RibbonButton13.Text = "&All Chart Types...";
            // 
            // RibbonButtonChartArea
            // 
            this.RibbonButtonChartArea.Expanded = false;
            this.RibbonButtonChartArea.Items.Add(this.AreaIn2DChart);
            this.RibbonButtonChartArea.Items.Add(this.StackedAreaChart);
            this.RibbonButtonChartArea.Items.Add(this.Stacked100AreaChart);
            this.RibbonButtonChartArea.Items.Add(this.AreaIn3DChart);
            this.RibbonButtonChartArea.Items.Add(this.StackedAreaIn3DChart);
            this.RibbonButtonChartArea.Items.Add(this.Stacked100AreaIn3DChart);
            this.RibbonButtonChartArea.ItemSize = new System.Drawing.Size(60, 60);
            this.RibbonButtonChartArea.LargeImage = ((System.Drawing.Image)(resources.GetObject("RibbonButtonChartArea.LargeImage")));
            this.RibbonButtonChartArea.MenuItems.Add(this.RibbonButton14);
            this.RibbonButtonChartArea.Name = "RibbonButtonChartArea";
            this.RibbonButtonChartArea.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButtonChartArea.SmallImage")));
            this.RibbonButtonChartArea.Text = "Area";
            this.RibbonButtonChartArea.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            // 
            // AreaIn2DChart
            // 
            this.AreaIn2DChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("AreaIn2DChart.LargeImage")));
            this.AreaIn2DChart.Name = "AreaIn2DChart";
            // 
            // StackedAreaChart
            // 
            this.StackedAreaChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("StackedAreaChart.LargeImage")));
            this.StackedAreaChart.Name = "StackedAreaChart";
            // 
            // Stacked100AreaChart
            // 
            this.Stacked100AreaChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("Stacked100AreaChart.LargeImage")));
            this.Stacked100AreaChart.Name = "Stacked100AreaChart";
            // 
            // AreaIn3DChart
            // 
            this.AreaIn3DChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("AreaIn3DChart.LargeImage")));
            this.AreaIn3DChart.Name = "AreaIn3DChart";
            // 
            // StackedAreaIn3DChart
            // 
            this.StackedAreaIn3DChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("StackedAreaIn3DChart.LargeImage")));
            this.StackedAreaIn3DChart.Name = "StackedAreaIn3DChart";
            // 
            // Stacked100AreaIn3DChart
            // 
            this.Stacked100AreaIn3DChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("Stacked100AreaIn3DChart.LargeImage")));
            this.Stacked100AreaIn3DChart.Name = "Stacked100AreaIn3DChart";
            // 
            // RibbonButton14
            // 
            this.RibbonButton14.LargeImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton14.LargeImage")));
            this.RibbonButton14.Name = "RibbonButton14";
            this.RibbonButton14.Text = "&All Chart Types...";
            // 
            // RibbonButtonChartScatter
            // 
            this.RibbonButtonChartScatter.Expanded = false;
            this.RibbonButtonChartScatter.Items.Add(this.XYPointChart);
            this.RibbonButtonChartScatter.Items.Add(this.XYLineChart);
            this.RibbonButtonChartScatter.Items.Add(this.XYLineWithMarkerChart);
            this.RibbonButtonChartScatter.ItemSize = new System.Drawing.Size(60, 60);
            this.RibbonButtonChartScatter.LargeImage = ((System.Drawing.Image)(resources.GetObject("RibbonButtonChartScatter.LargeImage")));
            this.RibbonButtonChartScatter.MenuItems.Add(this.RibbonButton15);
            this.RibbonButtonChartScatter.Name = "RibbonButtonChartScatter";
            this.RibbonButtonChartScatter.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButtonChartScatter.SmallImage")));
            this.RibbonButtonChartScatter.Text = "Scatter";
            this.RibbonButtonChartScatter.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.RibbonButtonChartScatter.VisibleItems = 2;
            // 
            // XYPointChart
            // 
            this.XYPointChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("XYPointChart.LargeImage")));
            this.XYPointChart.Name = "XYPointChart";
            // 
            // XYLineChart
            // 
            this.XYLineChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("XYLineChart.LargeImage")));
            this.XYLineChart.Name = "XYLineChart";
            // 
            // XYLineWithMarkerChart
            // 
            this.XYLineWithMarkerChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("XYLineWithMarkerChart.LargeImage")));
            this.XYLineWithMarkerChart.Name = "XYLineWithMarkerChart";
            // 
            // RibbonButton15
            // 
            this.RibbonButton15.LargeImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton15.LargeImage")));
            this.RibbonButton15.Name = "RibbonButton15";
            this.RibbonButton15.SupportedGroupSizing = C1.Win.C1Ribbon.SupportedGroupSizing.LargeImageOnly;
            this.RibbonButton15.Text = "&All Chart Types...";
            // 
            // RibbonButtonChartOthers
            // 
            this.RibbonButtonChartOthers.Expanded = false;
            this.RibbonButtonChartOthers.Items.Add(this.HighLowCloseChart);
            this.RibbonButtonChartOthers.Items.Add(this.OpenHighLowCloseChart);
            this.RibbonButtonChartOthers.Items.Add(this.CandlestickChart);
            this.RibbonButtonChartOthers.Items.Add(this.XYZSurfaceChart);
            this.RibbonButtonChartOthers.Items.Add(this.ribbonGalleryItem19);
            this.RibbonButtonChartOthers.Items.Add(this.ribbonGalleryItem20);
            this.RibbonButtonChartOthers.Items.Add(this.DoughnutChart);
            this.RibbonButtonChartOthers.Items.Add(this.ExplodedDoughnutChart);
            this.RibbonButtonChartOthers.Items.Add(this.ribbonGalleryItem23);
            this.RibbonButtonChartOthers.Items.Add(this.XYBubbleIn2DChart);
            this.RibbonButtonChartOthers.Items.Add(this.XYBubbleIn3DChart);
            this.RibbonButtonChartOthers.Items.Add(this.ribbonGalleryItem26);
            this.RibbonButtonChartOthers.Items.Add(this.RadarLineChart);
            this.RibbonButtonChartOthers.Items.Add(this.RadarLineWithMarkerChart);
            this.RibbonButtonChartOthers.Items.Add(this.RadarAreaChart);
            this.RibbonButtonChartOthers.ItemSize = new System.Drawing.Size(60, 60);
            this.RibbonButtonChartOthers.LargeImage = ((System.Drawing.Image)(resources.GetObject("RibbonButtonChartOthers.LargeImage")));
            this.RibbonButtonChartOthers.MenuItems.Add(this.RibbonButton50);
            this.RibbonButtonChartOthers.Name = "RibbonButtonChartOthers";
            this.RibbonButtonChartOthers.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButtonChartOthers.SmallImage")));
            this.RibbonButtonChartOthers.Text = "Other Charts";
            this.RibbonButtonChartOthers.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            // 
            // HighLowCloseChart
            // 
            this.HighLowCloseChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("HighLowCloseChart.LargeImage")));
            this.HighLowCloseChart.Name = "HighLowCloseChart";
            // 
            // OpenHighLowCloseChart
            // 
            this.OpenHighLowCloseChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("OpenHighLowCloseChart.LargeImage")));
            this.OpenHighLowCloseChart.Name = "OpenHighLowCloseChart";
            // 
            // CandlestickChart
            // 
            this.CandlestickChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("CandlestickChart.LargeImage")));
            this.CandlestickChart.Name = "CandlestickChart";
            // 
            // XYZSurfaceChart
            // 
            this.XYZSurfaceChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("XYZSurfaceChart.LargeImage")));
            this.XYZSurfaceChart.Name = "XYZSurfaceChart";
            // 
            // ribbonGalleryItem19
            // 
            this.ribbonGalleryItem19.Name = "ribbonGalleryItem19";
            // 
            // ribbonGalleryItem20
            // 
            this.ribbonGalleryItem20.Name = "ribbonGalleryItem20";
            // 
            // DoughnutChart
            // 
            this.DoughnutChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("DoughnutChart.LargeImage")));
            this.DoughnutChart.Name = "DoughnutChart";
            // 
            // ExplodedDoughnutChart
            // 
            this.ExplodedDoughnutChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("ExplodedDoughnutChart.LargeImage")));
            this.ExplodedDoughnutChart.Name = "ExplodedDoughnutChart";
            // 
            // ribbonGalleryItem23
            // 
            this.ribbonGalleryItem23.Name = "ribbonGalleryItem23";
            // 
            // XYBubbleIn2DChart
            // 
            this.XYBubbleIn2DChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("XYBubbleIn2DChart.LargeImage")));
            this.XYBubbleIn2DChart.Name = "XYBubbleIn2DChart";
            // 
            // XYBubbleIn3DChart
            // 
            this.XYBubbleIn3DChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("XYBubbleIn3DChart.LargeImage")));
            this.XYBubbleIn3DChart.Name = "XYBubbleIn3DChart";
            // 
            // ribbonGalleryItem26
            // 
            this.ribbonGalleryItem26.Name = "ribbonGalleryItem26";
            // 
            // RadarLineChart
            // 
            this.RadarLineChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("RadarLineChart.LargeImage")));
            this.RadarLineChart.Name = "RadarLineChart";
            // 
            // RadarLineWithMarkerChart
            // 
            this.RadarLineWithMarkerChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("RadarLineWithMarkerChart.LargeImage")));
            this.RadarLineWithMarkerChart.Name = "RadarLineWithMarkerChart";
            // 
            // RadarAreaChart
            // 
            this.RadarAreaChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("RadarAreaChart.LargeImage")));
            this.RadarAreaChart.Name = "RadarAreaChart";
            // 
            // RibbonButton50
            // 
            this.RibbonButton50.LargeImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton50.LargeImage")));
            this.RibbonButton50.Name = "RibbonButton50";
            this.RibbonButton50.Text = "All Chart Types...";
            // 
            // rpDelete
            // 
            this.rpDelete.Items.Add(this.rbDeleteShape);
            this.rpDelete.Name = "rpDelete";
            this.rpDelete.Text = "Delete";
            // 
            // rbDeleteShape
            // 
            this.rbDeleteShape.Enabled = false;
            this.rbDeleteShape.Items.Add(this.rbDeleteActiveShape);
            this.rbDeleteShape.Items.Add(this.rbDeleteAllShape);
            this.rbDeleteShape.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbDeleteShape.LargeImage")));
            this.rbDeleteShape.Name = "rbDeleteShape";
            this.rbDeleteShape.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbDeleteShape.SmallImage")));
            this.rbDeleteShape.Text = "Delete";
            this.rbDeleteShape.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            // 
            // rbDeleteActiveShape
            // 
            this.rbDeleteActiveShape.Name = "rbDeleteActiveShape";
            this.rbDeleteActiveShape.Text = "&Active Shape";
            // 
            // rbDeleteAllShape
            // 
            this.rbDeleteAllShape.Name = "rbDeleteAllShape";
            this.rbDeleteAllShape.Text = "All &Shape";
            // 
            // rpSparkline
            // 
            this.rpSparkline.Items.Add(this.rbSparklineLine);
            this.rpSparkline.Items.Add(this.rbSparklineColumn);
            this.rpSparkline.Items.Add(this.rbSparklineWinLoss);
            this.rpSparkline.Name = "rpSparkline";
            this.rpSparkline.Text = "Sparklines";
            // 
            // rbSparklineLine
            // 
            this.rbSparklineLine.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbSparklineLine.LargeImage")));
            this.rbSparklineLine.Name = "rbSparklineLine";
            this.rbSparklineLine.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSparklineLine.SmallImage")));
            this.rbSparklineLine.Text = "Line";
            this.rbSparklineLine.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbSparklineLine.Click += new System.EventHandler(this.btnLineChart_Click);
            // 
            // rbSparklineColumn
            // 
            this.rbSparklineColumn.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbSparklineColumn.LargeImage")));
            this.rbSparklineColumn.Name = "rbSparklineColumn";
            this.rbSparklineColumn.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSparklineColumn.SmallImage")));
            this.rbSparklineColumn.Text = "Column";
            this.rbSparklineColumn.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbSparklineColumn.Click += new System.EventHandler(this.btnColumnChart_Click);
            // 
            // rbSparklineWinLoss
            // 
            this.rbSparklineWinLoss.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbSparklineWinLoss.LargeImage")));
            this.rbSparklineWinLoss.Name = "rbSparklineWinLoss";
            this.rbSparklineWinLoss.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSparklineWinLoss.SmallImage")));
            this.rbSparklineWinLoss.Text = "Win/Loss";
            this.rbSparklineWinLoss.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbSparklineWinLoss.Click += new System.EventHandler(this.btnWinLoss_Click);
            // 
            // rpCameraShape
            // 
            this.rpCameraShape.Items.Add(this.rbCameraShape);
            this.rpCameraShape.Name = "rpCameraShape";
            this.rpCameraShape.Text = "Camera Shape";
            // 
            // rbCameraShape
            // 
            this.rbCameraShape.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbCameraShape.LargeImage")));
            this.rbCameraShape.Name = "rbCameraShape";
            this.rbCameraShape.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbCameraShape.SmallImage")));
            this.rbCameraShape.Text = "Camera Shape";
            this.rbCameraShape.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbCameraShape.Click += new System.EventHandler(this.btnCameraShape_Click);
            // 
            // rtpageLayout
            // 
            this.rtpageLayout.Groups.Add(this.rpPageSetup);
            this.rtpageLayout.Name = "rtpageLayout";
            this.rtpageLayout.Text = "PAGE LAYOUT";
            // 
            // rpPageSetup
            // 
            this.rpPageSetup.Items.Add(this.rbMarginsTemp);
            this.rpPageSetup.Items.Add(this.rbMargins);
            this.rpPageSetup.Items.Add(this.rbOrientation);
            this.rpPageSetup.Items.Add(this.RibbonButton263);
            this.rpPageSetup.Items.Add(this.rbBackGround);
            this.rpPageSetup.Items.Add(this.rbPrintTitles);
            this.rpPageSetup.Items.Add(this.rbSmartPrint);
            this.rpPageSetup.Name = "rpPageSetup";
            this.rpPageSetup.Text = "Page Setup";
            // 
            // rbMarginsTemp
            // 
            this.rbMarginsTemp.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbMarginsTemp.LargeImage")));
            this.rbMarginsTemp.Name = "rbMarginsTemp";
            this.rbMarginsTemp.Text = "Margins";
            this.rbMarginsTemp.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbMarginsTemp.Click += new System.EventHandler(this.btnCustomMargins_Click);
            // 
            // rbMargins
            // 
            this.rbMargins.Expanded = false;
            this.rbMargins.GripHandleVisible = false;
            this.rbMargins.Items.Add(this.rbMarginNormal);
            this.rbMargins.Items.Add(this.rbMarginNarrow);
            this.rbMargins.Items.Add(this.rbMarginWide);
            this.rbMargins.ItemSize = new System.Drawing.Size(240, 80);
            this.rbMargins.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbMargins.LargeImage")));
            this.rbMargins.MenuItems.Add(this.btnCustomMargins);
            this.rbMargins.Name = "rbMargins";
            this.rbMargins.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbMargins.SmallImage")));
            this.rbMargins.Text = "Margins";
            this.rbMargins.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbMargins.Visible = false;
            this.rbMargins.VisibleItems = 1;
            // 
            // rbMarginNormal
            // 
            this.rbMarginNormal.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbMarginNormal.LargeImage")));
            this.rbMarginNormal.Name = "rbMarginNormal";
            this.rbMarginNormal.Visible = false;
            // 
            // rbMarginNarrow
            // 
            this.rbMarginNarrow.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbMarginNarrow.LargeImage")));
            this.rbMarginNarrow.Name = "rbMarginNarrow";
            this.rbMarginNarrow.Visible = false;
            // 
            // rbMarginWide
            // 
            this.rbMarginWide.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbMarginWide.LargeImage")));
            this.rbMarginWide.Name = "rbMarginWide";
            this.rbMarginWide.Visible = false;
            // 
            // btnCustomMargins
            // 
            this.btnCustomMargins.Name = "btnCustomMargins";
            this.btnCustomMargins.Text = "Custom Margins...";
            // 
            // rbOrientation
            // 
            this.rbOrientation.Items.Add(this.rbOrientationNormal);
            this.rbOrientation.Items.Add(this.rbOrientationPortraint);
            this.rbOrientation.Items.Add(this.rbOrientationLandscape);
            this.rbOrientation.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbOrientation.LargeImage")));
            this.rbOrientation.Name = "rbOrientation";
            this.rbOrientation.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbOrientation.SmallImage")));
            this.rbOrientation.Text = "Orientation";
            this.rbOrientation.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            // 
            // rbOrientationNormal
            // 
            this.rbOrientationNormal.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbOrientationNormal.LargeImage")));
            this.rbOrientationNormal.Name = "rbOrientationNormal";
            this.rbOrientationNormal.Text = "&Auto";
            // 
            // rbOrientationPortraint
            // 
            this.rbOrientationPortraint.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbOrientationPortraint.LargeImage")));
            this.rbOrientationPortraint.Name = "rbOrientationPortraint";
            this.rbOrientationPortraint.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbOrientationPortraint.SmallImage")));
            this.rbOrientationPortraint.Text = "&Portrait";
            // 
            // rbOrientationLandscape
            // 
            this.rbOrientationLandscape.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbOrientationLandscape.LargeImage")));
            this.rbOrientationLandscape.Name = "rbOrientationLandscape";
            this.rbOrientationLandscape.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbOrientationLandscape.SmallImage")));
            this.rbOrientationLandscape.Text = "&Landscape";
            // 
            // RibbonButton263
            // 
            this.RibbonButton263.Items.Add(this.rbPrintAreaSet);
            this.RibbonButton263.Items.Add(this.rbPrintAreaClear);
            this.RibbonButton263.LargeImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton263.LargeImage")));
            this.RibbonButton263.Name = "RibbonButton263";
            this.RibbonButton263.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton263.SmallImage")));
            this.RibbonButton263.Text = "Print Area";
            this.RibbonButton263.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            // 
            // rbPrintAreaSet
            // 
            this.rbPrintAreaSet.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbPrintAreaSet.LargeImage")));
            this.rbPrintAreaSet.Name = "rbPrintAreaSet";
            this.rbPrintAreaSet.Text = "&Set";
            // 
            // rbPrintAreaClear
            // 
            this.rbPrintAreaClear.Enabled = false;
            this.rbPrintAreaClear.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbPrintAreaClear.LargeImage")));
            this.rbPrintAreaClear.Name = "rbPrintAreaClear";
            this.rbPrintAreaClear.Text = "&Clear";
            // 
            // rbBackGround
            // 
            this.rbBackGround.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbBackGround.LargeImage")));
            this.rbBackGround.Name = "rbBackGround";
            this.rbBackGround.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbBackGround.SmallImage")));
            this.rbBackGround.Text = "Background";
            this.rbBackGround.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbBackGround.Click += new System.EventHandler(this.btnBackgroud_Click);
            // 
            // rbPrintTitles
            // 
            this.rbPrintTitles.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbPrintTitles.LargeImage")));
            this.rbPrintTitles.Name = "rbPrintTitles";
            this.rbPrintTitles.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbPrintTitles.SmallImage")));
            this.rbPrintTitles.Text = "Print Titles";
            this.rbPrintTitles.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbPrintTitles.Click += new System.EventHandler(this.btnPrintTitles_Click);
            // 
            // rbSmartPrint
            // 
            this.rbSmartPrint.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbSmartPrint.LargeImage")));
            this.rbSmartPrint.Name = "rbSmartPrint";
            this.rbSmartPrint.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSmartPrint.SmallImage")));
            this.rbSmartPrint.Text = "Smart Print";
            this.rbSmartPrint.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbSmartPrint.Click += new System.EventHandler(this.btnSmartPrint_Click);
            // 
            // rtData
            // 
            this.rtData.Groups.Add(this.rpOutline);
            this.rtData.Groups.Add(this.rpSort);
            this.rtData.Groups.Add(this.rpCustomName);
            this.rtData.Name = "rtData";
            this.rtData.Text = "DATA";
            // 
            // rpOutline
            // 
            this.rpOutline.Items.Add(this.rbGroup);
            this.rpOutline.Items.Add(this.rbUnGroup);
            this.rpOutline.Items.Add(this.ribbonSeparator3);
            this.rpOutline.Items.Add(this.rbExpandGroup);
            this.rpOutline.Items.Add(this.rbCollapseGroup);
            this.rpOutline.Name = "rpOutline";
            this.rpOutline.Text = "Outline";
            // 
            // rbGroup
            // 
            this.rbGroup.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbGroup.LargeImage")));
            this.rbGroup.Name = "rbGroup";
            this.rbGroup.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbGroup.SmallImage")));
            this.rbGroup.Text = "Group";
            this.rbGroup.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbGroup.Click += new System.EventHandler(this.rbGroup_Click);
            // 
            // rbUnGroup
            // 
            this.rbUnGroup.Enabled = false;
            this.rbUnGroup.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbUnGroup.LargeImage")));
            this.rbUnGroup.Name = "rbUnGroup";
            this.rbUnGroup.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbUnGroup.SmallImage")));
            this.rbUnGroup.Text = "Ungroup";
            this.rbUnGroup.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbUnGroup.Click += new System.EventHandler(this.rbUnGroup_Click);
            // 
            // ribbonSeparator3
            // 
            this.ribbonSeparator3.Name = "ribbonSeparator3";
            // 
            // rbExpandGroup
            // 
            this.rbExpandGroup.Enabled = false;
            this.rbExpandGroup.Name = "rbExpandGroup";
            this.rbExpandGroup.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbExpandGroup.SmallImage")));
            this.rbExpandGroup.Text = "Expand Group";
            this.rbExpandGroup.Click += new System.EventHandler(this.rbExpandGroup_Click);
            // 
            // rbCollapseGroup
            // 
            this.rbCollapseGroup.Enabled = false;
            this.rbCollapseGroup.Name = "rbCollapseGroup";
            this.rbCollapseGroup.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbCollapseGroup.SmallImage")));
            this.rbCollapseGroup.Text = "Collapse Group";
            this.rbCollapseGroup.Click += new System.EventHandler(this.rbCollapseGroup_Click);
            // 
            // rpSort
            // 
            this.rpSort.Items.Add(this.rbSortAZ);
            this.rpSort.Items.Add(this.rbSortZA);
            this.rpSort.Items.Add(this.rbSort);
            this.rpSort.Name = "rpSort";
            this.rpSort.Text = "Sort";
            // 
            // rbSortAZ
            // 
            this.rbSortAZ.Name = "rbSortAZ";
            this.rbSortAZ.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSortAZ.SmallImage")));
            this.rbSortAZ.Click += new System.EventHandler(this.btnSortAtoZ_Click);
            // 
            // rbSortZA
            // 
            this.rbSortZA.Name = "rbSortZA";
            this.rbSortZA.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSortZA.SmallImage")));
            this.rbSortZA.Click += new System.EventHandler(this.btnSortZtoA_Click);
            // 
            // rbSort
            // 
            this.rbSort.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbSort.LargeImage")));
            this.rbSort.Name = "rbSort";
            this.rbSort.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSort.SmallImage")));
            this.rbSort.Text = "Sort";
            this.rbSort.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbSort.Click += new System.EventHandler(this.btnSortData_Click);
            // 
            // rpCustomName
            // 
            this.rpCustomName.Items.Add(this.rbAddCustomname);
            this.rpCustomName.Name = "rpCustomName";
            this.rpCustomName.Text = "Defined Name";
            // 
            // rbAddCustomname
            // 
            this.rbAddCustomname.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbAddCustomname.LargeImage")));
            this.rbAddCustomname.Name = "rbAddCustomname";
            this.rbAddCustomname.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbAddCustomname.SmallImage")));
            this.rbAddCustomname.Text = "Name Manager";
            this.rbAddCustomname.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbAddCustomname.Click += new System.EventHandler(this.btnNameManager_Click);
            // 
            // rtView
            // 
            this.rtView.Groups.Add(this.rpShowHide);
            this.rtView.Groups.Add(this.rpZoom);
            this.rtView.Groups.Add(this.rpWindow);
            this.rtView.Name = "rtView";
            this.rtView.Text = "VIEW";
            // 
            // rpShowHide
            // 
            this.rpShowHide.Items.Add(this.rbRowHeader);
            this.rpShowHide.Items.Add(this.rbColumnHeader);
            this.rpShowHide.Items.Add(this.ribbonSeparator4);
            this.rpShowHide.Items.Add(this.rbVerticalGridLine);
            this.rpShowHide.Items.Add(this.rbHorizontalGridLine);
            this.rpShowHide.Items.Add(this.ribbonSeparator5);
            this.rpShowHide.Items.Add(this.rbFormulaBar);
            this.rpShowHide.Name = "rpShowHide";
            this.rpShowHide.Text = "Show/Hide";
            // 
            // rbRowHeader
            // 
            this.rbRowHeader.Checked = true;
            this.rbRowHeader.Name = "rbRowHeader";
            this.rbRowHeader.Text = "Row Header";
            this.rbRowHeader.CheckedChanged += new System.EventHandler(this.CheckBoxRowHeader_CheckedChanged);
            // 
            // rbColumnHeader
            // 
            this.rbColumnHeader.Checked = true;
            this.rbColumnHeader.Name = "rbColumnHeader";
            this.rbColumnHeader.Text = "Column Header";
            this.rbColumnHeader.CheckedChanged += new System.EventHandler(this.CheckBoxColumnHeader_CheckedChanged);
            // 
            // ribbonSeparator4
            // 
            this.ribbonSeparator4.Name = "ribbonSeparator4";
            // 
            // rbVerticalGridLine
            // 
            this.rbVerticalGridLine.Checked = true;
            this.rbVerticalGridLine.Name = "rbVerticalGridLine";
            this.rbVerticalGridLine.Text = "Vertical Gridline";
            this.rbVerticalGridLine.CheckedChanged += new System.EventHandler(this.CheckBoxVerticalGridLine_CheckedChanged);
            // 
            // rbHorizontalGridLine
            // 
            this.rbHorizontalGridLine.Checked = true;
            this.rbHorizontalGridLine.Name = "rbHorizontalGridLine";
            this.rbHorizontalGridLine.Text = "Horizontal Gridline";
            this.rbHorizontalGridLine.CheckedChanged += new System.EventHandler(this.CheckBoxHorizontalGridLine_CheckedChanged);
            // 
            // ribbonSeparator5
            // 
            this.ribbonSeparator5.Name = "ribbonSeparator5";
            // 
            // rbFormulaBar
            // 
            this.rbFormulaBar.Checked = true;
            this.rbFormulaBar.Name = "rbFormulaBar";
            this.rbFormulaBar.Text = "Formula Bar";
            this.rbFormulaBar.CheckedChanged += new System.EventHandler(this.CheckBoxFormulaBar_CheckedChanged);
            // 
            // rpZoom
            // 
            this.rpZoom.Items.Add(this.rbZoom);
            this.rpZoom.Items.Add(this.rb100Percent);
            this.rpZoom.Items.Add(this.rbZoomToSelection);
            this.rpZoom.Name = "rpZoom";
            this.rpZoom.Text = "Zoom";
            // 
            // rbZoom
            // 
            this.rbZoom.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbZoom.LargeImage")));
            this.rbZoom.Name = "rbZoom";
            this.rbZoom.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbZoom.SmallImage")));
            this.rbZoom.Text = "Zoom";
            this.rbZoom.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbZoom.Click += new System.EventHandler(this.btnZoom_Click);
            // 
            // rb100Percent
            // 
            this.rb100Percent.LargeImage = ((System.Drawing.Image)(resources.GetObject("rb100Percent.LargeImage")));
            this.rb100Percent.Name = "rb100Percent";
            this.rb100Percent.SmallImage = ((System.Drawing.Image)(resources.GetObject("rb100Percent.SmallImage")));
            this.rb100Percent.Text = "100%";
            this.rb100Percent.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rb100Percent.Click += new System.EventHandler(this.btn100_Click);
            // 
            // rbZoomToSelection
            // 
            this.rbZoomToSelection.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbZoomToSelection.LargeImage")));
            this.rbZoomToSelection.Name = "rbZoomToSelection";
            this.rbZoomToSelection.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbZoomToSelection.SmallImage")));
            this.rbZoomToSelection.Text = "Zoom to Selection";
            this.rbZoomToSelection.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbZoomToSelection.Click += new System.EventHandler(this.btnZoomToSelection_Click);
            // 
            // rpWindow
            // 
            this.rpWindow.Items.Add(this.rbFreezePanes);
            this.rpWindow.Name = "rpWindow";
            this.rpWindow.Text = "Window";
            // 
            // rbFreezePanes
            // 
            this.rbFreezePanes.Items.Add(this.rbFreezeRowsAndColumns);
            this.rbFreezePanes.Items.Add(this.rbFreezeTopRow);
            this.rbFreezePanes.Items.Add(this.rbFreezeFirstColumn);
            this.rbFreezePanes.Items.Add(this.rbFreezeTrailingRowsAndColumns);
            this.rbFreezePanes.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbFreezePanes.LargeImage")));
            this.rbFreezePanes.Name = "rbFreezePanes";
            this.rbFreezePanes.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbFreezePanes.SmallImage")));
            this.rbFreezePanes.Text = "Freeze Panes";
            this.rbFreezePanes.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            // 
            // rbFreezeRowsAndColumns
            // 
            this.rbFreezeRowsAndColumns.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbFreezeRowsAndColumns.LargeImage")));
            this.rbFreezeRowsAndColumns.Name = "rbFreezeRowsAndColumns";
            this.rbFreezeRowsAndColumns.Text = "&Freeze Row And Column";
            // 
            // rbFreezeTopRow
            // 
            this.rbFreezeTopRow.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbFreezeTopRow.LargeImage")));
            this.rbFreezeTopRow.Name = "rbFreezeTopRow";
            this.rbFreezeTopRow.SupportedGroupSizing = C1.Win.C1Ribbon.SupportedGroupSizing.LargeImageOnly;
            this.rbFreezeTopRow.Text = "F&ree Top Row";
            // 
            // rbFreezeFirstColumn
            // 
            this.rbFreezeFirstColumn.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbFreezeFirstColumn.LargeImage")));
            this.rbFreezeFirstColumn.Name = "rbFreezeFirstColumn";
            this.rbFreezeFirstColumn.SupportedGroupSizing = C1.Win.C1Ribbon.SupportedGroupSizing.LargeImageOnly;
            this.rbFreezeFirstColumn.Text = "Freeze First &Column";
            // 
            // rbFreezeTrailingRowsAndColumns
            // 
            this.rbFreezeTrailingRowsAndColumns.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbFreezeTrailingRowsAndColumns.LargeImage")));
            this.rbFreezeTrailingRowsAndColumns.Name = "rbFreezeTrailingRowsAndColumns";
            this.rbFreezeTrailingRowsAndColumns.Text = "Freeze &Trailing Rows And Columns";
            // 
            // rtSetting
            // 
            this.rtSetting.Groups.Add(this.rpSpreadSettings);
            this.rtSetting.Groups.Add(this.rpSheetSettings);
            this.rtSetting.Groups.Add(this.rpAppearanceSettings);
            this.rtSetting.Groups.Add(this.rpOtherSettings);
            this.rtSetting.Groups.Add(this.rpDesignerSettings);
            this.rtSetting.Name = "rtSetting";
            this.rtSetting.Text = "SETTINGS";
            // 
            // rpSpreadSettings
            // 
            this.rpSpreadSettings.Items.Add(this.rbSpreadSettingGeneral);
            this.rpSpreadSettings.Items.Add(this.rbSpreadSettingEdit);
            this.rpSpreadSettings.Items.Add(this.rbSpreadSettingScrollBar);
            this.rpSpreadSettings.Items.Add(this.rbSpreadSettingSplitBox);
            this.rpSpreadSettings.Items.Add(this.rbSpreadSettingView);
            this.rpSpreadSettings.Items.Add(this.rbSpreadSettingTitles);
            this.rpSpreadSettings.Name = "rpSpreadSettings";
            this.rpSpreadSettings.Text = "Spread Settings";
            // 
            // rbSpreadSettingGeneral
            // 
            this.rbSpreadSettingGeneral.Name = "rbSpreadSettingGeneral";
            this.rbSpreadSettingGeneral.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSpreadSettingGeneral.SmallImage")));
            this.rbSpreadSettingGeneral.Text = "General";
            this.rbSpreadSettingGeneral.Click += new System.EventHandler(this.btnGeneral1_Click);
            // 
            // rbSpreadSettingEdit
            // 
            this.rbSpreadSettingEdit.Name = "rbSpreadSettingEdit";
            this.rbSpreadSettingEdit.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSpreadSettingEdit.SmallImage")));
            this.rbSpreadSettingEdit.Text = "Edit";
            this.rbSpreadSettingEdit.Click += new System.EventHandler(this.btnEdit1_Click);
            // 
            // rbSpreadSettingScrollBar
            // 
            this.rbSpreadSettingScrollBar.Name = "rbSpreadSettingScrollBar";
            this.rbSpreadSettingScrollBar.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSpreadSettingScrollBar.SmallImage")));
            this.rbSpreadSettingScrollBar.Text = "ScrollBars";
            this.rbSpreadSettingScrollBar.Click += new System.EventHandler(this.btnScrollBar1_Click);
            // 
            // rbSpreadSettingSplitBox
            // 
            this.rbSpreadSettingSplitBox.Name = "rbSpreadSettingSplitBox";
            this.rbSpreadSettingSplitBox.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSpreadSettingSplitBox.SmallImage")));
            this.rbSpreadSettingSplitBox.Text = "SplitBox";
            this.rbSpreadSettingSplitBox.Click += new System.EventHandler(this.btnSplitBox1_Click);
            // 
            // rbSpreadSettingView
            // 
            this.rbSpreadSettingView.Name = "rbSpreadSettingView";
            this.rbSpreadSettingView.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSpreadSettingView.SmallImage")));
            this.rbSpreadSettingView.Text = "View";
            this.rbSpreadSettingView.Click += new System.EventHandler(this.btnView1_Click);
            // 
            // rbSpreadSettingTitles
            // 
            this.rbSpreadSettingTitles.Name = "rbSpreadSettingTitles";
            this.rbSpreadSettingTitles.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSpreadSettingTitles.SmallImage")));
            this.rbSpreadSettingTitles.Text = "Titles";
            this.rbSpreadSettingTitles.Click += new System.EventHandler(this.btnTitles1_Click);
            // 
            // rpSheetSettings
            // 
            this.rpSheetSettings.Items.Add(this.rbSheetSettingGeneral);
            this.rpSheetSettings.Items.Add(this.rbSheetSettingColor);
            this.rpSheetSettings.Items.Add(this.rbSheetSettingHeaders);
            this.rpSheetSettings.Items.Add(this.rbSheetSettingGridLines);
            this.rpSheetSettings.Items.Add(this.rbSheetSettingCalculation);
            this.rpSheetSettings.Items.Add(this.rbSheetSettingFonts);
            this.rpSheetSettings.Name = "rpSheetSettings";
            this.rpSheetSettings.Text = "Sheet Settings";
            // 
            // rbSheetSettingGeneral
            // 
            this.rbSheetSettingGeneral.Name = "rbSheetSettingGeneral";
            this.rbSheetSettingGeneral.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSheetSettingGeneral.SmallImage")));
            this.rbSheetSettingGeneral.Text = "General";
            this.rbSheetSettingGeneral.Click += new System.EventHandler(this.btnGeneralChart1_Click);
            // 
            // rbSheetSettingColor
            // 
            this.rbSheetSettingColor.Name = "rbSheetSettingColor";
            this.rbSheetSettingColor.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSheetSettingColor.SmallImage")));
            this.rbSheetSettingColor.Text = "Colors";
            this.rbSheetSettingColor.Click += new System.EventHandler(this.btnColors1_Click);
            // 
            // rbSheetSettingHeaders
            // 
            this.rbSheetSettingHeaders.Name = "rbSheetSettingHeaders";
            this.rbSheetSettingHeaders.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSheetSettingHeaders.SmallImage")));
            this.rbSheetSettingHeaders.Text = "Headers";
            this.rbSheetSettingHeaders.Click += new System.EventHandler(this.btnHeaders1_Click);
            // 
            // rbSheetSettingGridLines
            // 
            this.rbSheetSettingGridLines.Name = "rbSheetSettingGridLines";
            this.rbSheetSettingGridLines.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSheetSettingGridLines.SmallImage")));
            this.rbSheetSettingGridLines.Text = "Gridlines";
            this.rbSheetSettingGridLines.Click += new System.EventHandler(this.btnGridLines1_Click);
            // 
            // rbSheetSettingCalculation
            // 
            this.rbSheetSettingCalculation.Name = "rbSheetSettingCalculation";
            this.rbSheetSettingCalculation.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSheetSettingCalculation.SmallImage")));
            this.rbSheetSettingCalculation.Text = "Calculation";
            this.rbSheetSettingCalculation.Click += new System.EventHandler(this.btnCalculation1_Click);
            // 
            // rbSheetSettingFonts
            // 
            this.rbSheetSettingFonts.Name = "rbSheetSettingFonts";
            this.rbSheetSettingFonts.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSheetSettingFonts.SmallImage")));
            this.rbSheetSettingFonts.Text = "Fonts";
            this.rbSheetSettingFonts.Click += new System.EventHandler(this.btnFonts1_Click);
            // 
            // rpAppearanceSettings
            // 
            this.rpAppearanceSettings.Items.Add(this.rbSpreadSkin);
            this.rpAppearanceSettings.Items.Add(this.rbSheetSkin);
            this.rpAppearanceSettings.Items.Add(this.rbFocusIndicator);
            this.rpAppearanceSettings.Items.Add(this.rbStyle);
            this.rpAppearanceSettings.Items.Add(this.rbTabStrip);
            this.rpAppearanceSettings.Items.Add(this.rbAlternatingRow);
            this.rpAppearanceSettings.Name = "rpAppearanceSettings";
            this.rpAppearanceSettings.Text = "Appearance Settings";
            // 
            // rbSpreadSkin
            // 
            this.rbSpreadSkin.Name = "rbSpreadSkin";
            this.rbSpreadSkin.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSpreadSkin.SmallImage")));
            this.rbSpreadSkin.Text = "SpreadSkin";
            this.rbSpreadSkin.Click += new System.EventHandler(this.btnSpreadSkins1_Click);
            // 
            // rbSheetSkin
            // 
            this.rbSheetSkin.Name = "rbSheetSkin";
            this.rbSheetSkin.Text = "SheetSkin";
            this.rbSheetSkin.Click += new System.EventHandler(this.btnSheetSkins1_Click);
            // 
            // rbFocusIndicator
            // 
            this.rbFocusIndicator.Name = "rbFocusIndicator";
            this.rbFocusIndicator.Text = "Focus Indicator";
            this.rbFocusIndicator.Click += new System.EventHandler(this.btnFocusIndicator1_Click);
            // 
            // rbStyle
            // 
            this.rbStyle.Name = "rbStyle";
            this.rbStyle.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbStyle.SmallImage")));
            this.rbStyle.Text = "Style";
            this.rbStyle.Click += new System.EventHandler(this.btnStyle1_Click);
            // 
            // rbTabStrip
            // 
            this.rbTabStrip.Name = "rbTabStrip";
            this.rbTabStrip.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbTabStrip.SmallImage")));
            this.rbTabStrip.Text = "Tabstrip";
            this.rbTabStrip.Click += new System.EventHandler(this.btnTabScript1_Click);
            // 
            // rbAlternatingRow
            // 
            this.rbAlternatingRow.Name = "rbAlternatingRow";
            this.rbAlternatingRow.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbAlternatingRow.SmallImage")));
            this.rbAlternatingRow.Text = "Alternating Row";
            this.rbAlternatingRow.Click += new System.EventHandler(this.btnAlternatingRow1_Click);
            // 
            // rpOtherSettings
            // 
            this.rpOtherSettings.Items.Add(this.rbGroupInfo);
            this.rpOtherSettings.Items.Add(this.rbCellColumnsandRows);
            this.rpOtherSettings.Items.Add(this.rbInputMap);
            this.rpOtherSettings.Name = "rpOtherSettings";
            this.rpOtherSettings.Text = "Other Settings";
            // 
            // rbGroupInfo
            // 
            this.rbGroupInfo.Name = "rbGroupInfo";
            this.rbGroupInfo.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbGroupInfo.SmallImage")));
            this.rbGroupInfo.Text = "Group Info";
            this.rbGroupInfo.Click += new System.EventHandler(this.btnGroupInfo1_Click);
            // 
            // rbCellColumnsandRows
            // 
            this.rbCellColumnsandRows.Name = "rbCellColumnsandRows";
            this.rbCellColumnsandRows.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbCellColumnsandRows.SmallImage")));
            this.rbCellColumnsandRows.Text = "Cells, Columns and Rows";
            this.rbCellColumnsandRows.Click += new System.EventHandler(this.btnCellsColumns1_Click);
            // 
            // rbInputMap
            // 
            this.rbInputMap.Name = "rbInputMap";
            this.rbInputMap.SmallImage = global::SpreadDesigner.Properties.Resources.rbInputMapEditor;
            this.rbInputMap.Text = "Input Map";
            this.rbInputMap.Click += new System.EventHandler(this.btnInputMap1_Click);
            // 
            // rpDesignerSettings
            // 
            this.rpDesignerSettings.Items.Add(this.rbPreferences);
            this.rpDesignerSettings.Name = "rpDesignerSettings";
            this.rpDesignerSettings.Text = "Designer Settings";
            this.rpDesignerSettings.Visible = false;
            // 
            // rbPreferences
            // 
            this.rbPreferences.Items.Add(this.rbShowStart);
            this.rbPreferences.Items.Add(this.rbAllowDrag);
            this.rbPreferences.Items.Add(this.rbAutoLaunch);
            this.rbPreferences.Items.Add(this.rbAutomticSelectContextMenu);
            this.rbPreferences.Items.Add(this.rbShowPropertyGrid);
            this.rbPreferences.Items.Add(this.rbShowAllConditionalFormat);
            this.rbPreferences.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbPreferences.LargeImage")));
            this.rbPreferences.Name = "rbPreferences";
            this.rbPreferences.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbPreferences.SmallImage")));
            this.rbPreferences.Text = "Preferences";
            this.rbPreferences.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            // 
            // rbShowStart
            // 
            this.rbShowStart.Name = "rbShowStart";
            this.rbShowStart.Pressed = true;
            this.rbShowStart.Text = "&Show Startup Screen";
            // 
            // rbAllowDrag
            // 
            this.rbAllowDrag.Name = "rbAllowDrag";
            this.rbAllowDrag.Text = "&Allow Designer DragFill";
            // 
            // rbAutoLaunch
            // 
            this.rbAutoLaunch.Name = "rbAutoLaunch";
            this.rbAutoLaunch.Pressed = true;
            this.rbAutoLaunch.Text = "Auto-launch &Designer on new instance of Spread";
            // 
            // rbAutomticSelectContextMenu
            // 
            this.rbAutomticSelectContextMenu.Name = "rbAutomticSelectContextMenu";
            this.rbAutomticSelectContextMenu.Pressed = true;
            this.rbAutomticSelectContextMenu.Text = "S&elect Context Menu Automatic";
            // 
            // rbShowPropertyGrid
            // 
            this.rbShowPropertyGrid.Name = "rbShowPropertyGrid";
            this.rbShowPropertyGrid.Text = "Show &Property Grid";
            // 
            // rbShowAllConditionalFormat
            // 
            this.rbShowAllConditionalFormat.Name = "rbShowAllConditionalFormat";
            this.rbShowAllConditionalFormat.Pressed = true;
            this.rbShowAllConditionalFormat.Text = "Show all type &conditional format";
            // 
            // rtDrawingTools
            // 
            this.rtDrawingTools.Groups.Add(this.RibbonPanel13);
            this.rtDrawingTools.Groups.Add(this.RibbonPanel14);
            this.rtDrawingTools.Name = "rtDrawingTools";
            this.rtDrawingTools.Text = "DRAWING TOOLS";
            // 
            // RibbonPanel13
            // 
            this.RibbonPanel13.Items.Add(this.RibbonColorChooser1);
            this.RibbonPanel13.Items.Add(this.RibbonColorChooser2);
            this.RibbonPanel13.Items.Add(this.RibbonColorChooser3);
            this.RibbonPanel13.Items.Add(this.ribbonSeparator12);
            this.RibbonPanel13.Items.Add(this.RibbonButton110);
            this.RibbonPanel13.Items.Add(this.RibbonButton111);
            this.RibbonPanel13.Items.Add(this.RibbonButton137);
            this.RibbonPanel13.Name = "RibbonPanel13";
            this.RibbonPanel13.Text = "Shape Style";
            // 
            // RibbonColorChooser1
            // 
            this.RibbonColorChooser1.Name = "RibbonColorChooser1";
            this.RibbonColorChooser1.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonColorChooser1.SmallImage")));
            this.RibbonColorChooser1.Text = "Back Color";
            this.RibbonColorChooser1.Click += new System.EventHandler(this.ColorPickerBackColor_Click);
            // 
            // RibbonColorChooser2
            // 
            this.RibbonColorChooser2.DefaultColor = System.Drawing.Color.White;
            this.RibbonColorChooser2.Name = "RibbonColorChooser2";
            this.RibbonColorChooser2.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonColorChooser2.SmallImage")));
            this.RibbonColorChooser2.Text = "Fore Color";
            this.RibbonColorChooser2.Click += new System.EventHandler(this.ColorPickerForeColor_Click);
            // 
            // RibbonColorChooser3
            // 
            this.RibbonColorChooser3.DefaultColor = System.Drawing.Color.Black;
            this.RibbonColorChooser3.Name = "RibbonColorChooser3";
            this.RibbonColorChooser3.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonColorChooser3.SmallImage")));
            this.RibbonColorChooser3.Text = "Outline Color";
            this.RibbonColorChooser3.Click += new System.EventHandler(this.ColorPickerOutLineColor_Click);
            // 
            // ribbonSeparator12
            // 
            this.ribbonSeparator12.Name = "ribbonSeparator12";
            // 
            // RibbonButton110
            // 
            this.RibbonButton110.Items.Add(this.rbShapeThickNone);
            this.RibbonButton110.Items.Add(this.RibbonButton159);
            this.RibbonButton110.Items.Add(this.RibbonButton160);
            this.RibbonButton110.Items.Add(this.RibbonButton161);
            this.RibbonButton110.Items.Add(this.RibbonButton162);
            this.RibbonButton110.Items.Add(this.RibbonButton163);
            this.RibbonButton110.Items.Add(this.RibbonButton164);
            this.RibbonButton110.Items.Add(this.rbShapeThickCustom);
            this.RibbonButton110.Name = "RibbonButton110";
            this.RibbonButton110.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton110.SmallImage")));
            this.RibbonButton110.Text = "Thickness";
            // 
            // rbShapeThickNone
            // 
            this.rbShapeThickNone.Name = "rbShapeThickNone";
            this.rbShapeThickNone.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbShapeThickNone.SmallImage")));
            this.rbShapeThickNone.Text = "&None";
            // 
            // RibbonButton159
            // 
            this.RibbonButton159.Name = "RibbonButton159";
            this.RibbonButton159.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton159.SmallImage")));
            this.RibbonButton159.Text = "&1 Pixel";
            // 
            // RibbonButton160
            // 
            this.RibbonButton160.Name = "RibbonButton160";
            this.RibbonButton160.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton160.SmallImage")));
            this.RibbonButton160.Text = "&2 Pixels";
            // 
            // RibbonButton161
            // 
            this.RibbonButton161.Name = "RibbonButton161";
            this.RibbonButton161.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton161.SmallImage")));
            this.RibbonButton161.Text = "&3 Pixels";
            // 
            // RibbonButton162
            // 
            this.RibbonButton162.Name = "RibbonButton162";
            this.RibbonButton162.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton162.SmallImage")));
            this.RibbonButton162.Text = "4 Pixels";
            // 
            // RibbonButton163
            // 
            this.RibbonButton163.Name = "RibbonButton163";
            this.RibbonButton163.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton163.SmallImage")));
            this.RibbonButton163.Text = "&5 Pixels";
            // 
            // RibbonButton164
            // 
            this.RibbonButton164.Name = "RibbonButton164";
            this.RibbonButton164.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton164.SmallImage")));
            this.RibbonButton164.Text = "1&0 Pixels";
            // 
            // rbShapeThickCustom
            // 
            this.rbShapeThickCustom.Name = "rbShapeThickCustom";
            this.rbShapeThickCustom.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbShapeThickCustom.SmallImage")));
            this.rbShapeThickCustom.Text = "&Custom";
            // 
            // RibbonButton111
            // 
            this.RibbonButton111.Items.Add(this.rbShapeOutlineSolid);
            this.RibbonButton111.Items.Add(this.rbShapeOutlineDash);
            this.RibbonButton111.Items.Add(this.rbShapeOutlineDot);
            this.RibbonButton111.Items.Add(this.rbShapeOutlineDashDot);
            this.RibbonButton111.Items.Add(this.rbShapeOutlineDashDotDot);
            this.RibbonButton111.Name = "RibbonButton111";
            this.RibbonButton111.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton111.SmallImage")));
            this.RibbonButton111.Text = "Outline Style";
            // 
            // rbShapeOutlineSolid
            // 
            this.rbShapeOutlineSolid.Name = "rbShapeOutlineSolid";
            this.rbShapeOutlineSolid.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbShapeOutlineSolid.SmallImage")));
            this.rbShapeOutlineSolid.Text = "&Solid";
            // 
            // rbShapeOutlineDash
            // 
            this.rbShapeOutlineDash.Name = "rbShapeOutlineDash";
            this.rbShapeOutlineDash.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbShapeOutlineDash.SmallImage")));
            this.rbShapeOutlineDash.Text = "&Dash";
            // 
            // rbShapeOutlineDot
            // 
            this.rbShapeOutlineDot.Name = "rbShapeOutlineDot";
            this.rbShapeOutlineDot.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbShapeOutlineDot.SmallImage")));
            this.rbShapeOutlineDot.Text = "D&ot";
            // 
            // rbShapeOutlineDashDot
            // 
            this.rbShapeOutlineDashDot.Name = "rbShapeOutlineDashDot";
            this.rbShapeOutlineDashDot.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbShapeOutlineDashDot.SmallImage")));
            this.rbShapeOutlineDashDot.Text = "D&ashDot";
            // 
            // rbShapeOutlineDashDotDot
            // 
            this.rbShapeOutlineDashDotDot.Name = "rbShapeOutlineDashDotDot";
            this.rbShapeOutlineDashDotDot.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbShapeOutlineDashDotDot.SmallImage")));
            this.rbShapeOutlineDashDotDot.Text = "Das&hDotDot";
            // 
            // RibbonButton137
            // 
            this.RibbonButton137.Items.Add(this.rbShapeShadowNone);
            this.RibbonButton137.Items.Add(this.rbShapeShadowRight);
            this.RibbonButton137.Items.Add(this.rbShapeShadowBottomRight);
            this.RibbonButton137.Items.Add(this.rbShapeShadowBottom);
            this.RibbonButton137.Items.Add(this.rbShapeShadowBottomLeft);
            this.RibbonButton137.Items.Add(this.rbShapeShadowLeft);
            this.RibbonButton137.Items.Add(this.rbShapeShadowTopLeft);
            this.RibbonButton137.Items.Add(this.rbShapeShadowTop);
            this.RibbonButton137.Items.Add(this.rbShapeShadowTopRight);
            this.RibbonButton137.Items.Add(this.rbShapeShadowCustom);
            this.RibbonButton137.Name = "RibbonButton137";
            this.RibbonButton137.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton137.SmallImage")));
            this.RibbonButton137.Text = "Drop Shadow";
            // 
            // rbShapeShadowNone
            // 
            this.rbShapeShadowNone.Name = "rbShapeShadowNone";
            this.rbShapeShadowNone.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbShapeShadowNone.SmallImage")));
            this.rbShapeShadowNone.Text = "&None";
            // 
            // rbShapeShadowRight
            // 
            this.rbShapeShadowRight.Name = "rbShapeShadowRight";
            this.rbShapeShadowRight.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbShapeShadowRight.SmallImage")));
            this.rbShapeShadowRight.Text = "&Right";
            // 
            // rbShapeShadowBottomRight
            // 
            this.rbShapeShadowBottomRight.Name = "rbShapeShadowBottomRight";
            this.rbShapeShadowBottomRight.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbShapeShadowBottomRight.SmallImage")));
            this.rbShapeShadowBottomRight.Text = "B&ottom Right";
            // 
            // rbShapeShadowBottom
            // 
            this.rbShapeShadowBottom.Name = "rbShapeShadowBottom";
            this.rbShapeShadowBottom.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbShapeShadowBottom.SmallImage")));
            this.rbShapeShadowBottom.Text = "&Bottom";
            // 
            // rbShapeShadowBottomLeft
            // 
            this.rbShapeShadowBottomLeft.Name = "rbShapeShadowBottomLeft";
            this.rbShapeShadowBottomLeft.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbShapeShadowBottomLeft.SmallImage")));
            this.rbShapeShadowBottomLeft.Text = "&Botto&m Left";
            // 
            // rbShapeShadowLeft
            // 
            this.rbShapeShadowLeft.Name = "rbShapeShadowLeft";
            this.rbShapeShadowLeft.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbShapeShadowLeft.SmallImage")));
            this.rbShapeShadowLeft.Text = "&Left";
            // 
            // rbShapeShadowTopLeft
            // 
            this.rbShapeShadowTopLeft.Name = "rbShapeShadowTopLeft";
            this.rbShapeShadowTopLeft.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbShapeShadowTopLeft.SmallImage")));
            this.rbShapeShadowTopLeft.Text = "To&p Left";
            // 
            // rbShapeShadowTop
            // 
            this.rbShapeShadowTop.Name = "rbShapeShadowTop";
            this.rbShapeShadowTop.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbShapeShadowTop.SmallImage")));
            this.rbShapeShadowTop.Text = "&Top";
            // 
            // rbShapeShadowTopRight
            // 
            this.rbShapeShadowTopRight.Name = "rbShapeShadowTopRight";
            this.rbShapeShadowTopRight.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbShapeShadowTopRight.SmallImage")));
            this.rbShapeShadowTopRight.Text = "Top Ri&ght";
            // 
            // rbShapeShadowCustom
            // 
            this.rbShapeShadowCustom.Name = "rbShapeShadowCustom";
            this.rbShapeShadowCustom.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbShapeShadowCustom.SmallImage")));
            this.rbShapeShadowCustom.Text = "&Custom...";
            // 
            // RibbonPanel14
            // 
            this.RibbonPanel14.Items.Add(this.RibbonButton138);
            this.RibbonPanel14.Items.Add(this.RibbonButton139);
            this.RibbonPanel14.Items.Add(this.ribbonSeparator13);
            this.RibbonPanel14.Items.Add(this.RibbonButton140);
            this.RibbonPanel14.Items.Add(this.RibbonButton141);
            this.RibbonPanel14.Items.Add(this.RibbonButton142);
            this.RibbonPanel14.Items.Add(this.ribbonSeparator14);
            this.RibbonPanel14.Items.Add(this.rbSetPicture);
            this.RibbonPanel14.Items.Add(this.rbClearPicture);
            this.RibbonPanel14.Items.Add(this.ribbonSeparator15);
            this.RibbonPanel14.Items.Add(this.RibbonButton144);
            this.RibbonPanel14.Items.Add(this.RibbonButton145);
            this.RibbonPanel14.Items.Add(this.RibbonButton146);
            this.RibbonPanel14.Items.Add(this.RibbonButton147);
            this.RibbonPanel14.Items.Add(this.RibbonButton148);
            this.RibbonPanel14.Items.Add(this.RibbonButton149);
            this.RibbonPanel14.Items.Add(this.RibbonButton150);
            this.RibbonPanel14.Items.Add(this.RibbonButton151);
            this.RibbonPanel14.Items.Add(this.RibbonButton152);
            this.RibbonPanel14.Name = "RibbonPanel14";
            this.RibbonPanel14.Text = "Shape Arrange";
            // 
            // RibbonButton138
            // 
            this.RibbonButton138.Name = "RibbonButton138";
            this.RibbonButton138.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton138.SmallImage")));
            this.RibbonButton138.Text = "Bring to Front";
            this.RibbonButton138.Click += new System.EventHandler(this.btnBringToFront1_Click);
            // 
            // RibbonButton139
            // 
            this.RibbonButton139.Name = "RibbonButton139";
            this.RibbonButton139.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton139.SmallImage")));
            this.RibbonButton139.Text = "Send to Back";
            this.RibbonButton139.Click += new System.EventHandler(this.btnSendToBack1_Click);
            // 
            // ribbonSeparator13
            // 
            this.ribbonSeparator13.Name = "ribbonSeparator13";
            // 
            // RibbonButton140
            // 
            this.RibbonButton140.Items.Add(this.RibbonButton180);
            this.RibbonButton140.Items.Add(this.btnRotate1);
            this.RibbonButton140.Items.Add(this.btnRotate5);
            this.RibbonButton140.Items.Add(this.btnRotate45);
            this.RibbonButton140.Items.Add(this.btnRotate90);
            this.RibbonButton140.Items.Add(this.btnRotate180);
            this.RibbonButton140.Items.Add(this.btnRotate270);
            this.RibbonButton140.Items.Add(this.RibbonButton187);
            this.RibbonButton140.Name = "RibbonButton140";
            this.RibbonButton140.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton140.SmallImage")));
            this.RibbonButton140.Text = "Rotate";
            // 
            // RibbonButton180
            // 
            this.RibbonButton180.Name = "RibbonButton180";
            this.RibbonButton180.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton180.SmallImage")));
            this.RibbonButton180.Text = "&Reset";
            // 
            // btnRotate1
            // 
            this.btnRotate1.Name = "btnRotate1";
            this.btnRotate1.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnRotate1.SmallImage")));
            this.btnRotate1.Text = "&1";
            // 
            // btnRotate5
            // 
            this.btnRotate5.Name = "btnRotate5";
            this.btnRotate5.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnRotate5.SmallImage")));
            this.btnRotate5.Text = "&5";
            // 
            // btnRotate45
            // 
            this.btnRotate45.Name = "btnRotate45";
            this.btnRotate45.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnRotate45.SmallImage")));
            this.btnRotate45.Text = "&45";
            // 
            // btnRotate90
            // 
            this.btnRotate90.Name = "btnRotate90";
            this.btnRotate90.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnRotate90.SmallImage")));
            this.btnRotate90.Text = "&90";
            // 
            // btnRotate180
            // 
            this.btnRotate180.Name = "btnRotate180";
            this.btnRotate180.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnRotate180.SmallImage")));
            this.btnRotate180.Text = "1&80";
            // 
            // btnRotate270
            // 
            this.btnRotate270.Name = "btnRotate270";
            this.btnRotate270.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnRotate270.SmallImage")));
            this.btnRotate270.Text = "&270";
            // 
            // RibbonButton187
            // 
            this.RibbonButton187.Name = "RibbonButton187";
            this.RibbonButton187.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton187.SmallImage")));
            this.RibbonButton187.Text = "&Custom...";
            // 
            // RibbonButton141
            // 
            this.RibbonButton141.Items.Add(this.RibbonButton188);
            this.RibbonButton141.Items.Add(this.RibbonButton189);
            this.RibbonButton141.Name = "RibbonButton141";
            this.RibbonButton141.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton141.SmallImage")));
            this.RibbonButton141.Text = "Flip";
            // 
            // RibbonButton188
            // 
            this.RibbonButton188.Name = "RibbonButton188";
            this.RibbonButton188.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton188.SmallImage")));
            this.RibbonButton188.Text = "&Horizontal";
            // 
            // RibbonButton189
            // 
            this.RibbonButton189.Name = "RibbonButton189";
            this.RibbonButton189.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton189.SmallImage")));
            this.RibbonButton189.Text = "&Vertical";
            // 
            // RibbonButton142
            // 
            this.RibbonButton142.Items.Add(this.btnScale10);
            this.RibbonButton142.Items.Add(this.btnScale25);
            this.RibbonButton142.Items.Add(this.btnScale50);
            this.RibbonButton142.Items.Add(this.btnScale75);
            this.RibbonButton142.Items.Add(this.btnScale150);
            this.RibbonButton142.Items.Add(this.btnScale200);
            this.RibbonButton142.Items.Add(this.btnScale300);
            this.RibbonButton142.Items.Add(this.RibbonButton197);
            this.RibbonButton142.Name = "RibbonButton142";
            this.RibbonButton142.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton142.SmallImage")));
            this.RibbonButton142.Text = "Scale";
            // 
            // btnScale10
            // 
            this.btnScale10.Name = "btnScale10";
            this.btnScale10.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnScale10.SmallImage")));
            this.btnScale10.Text = "&10%";
            // 
            // btnScale25
            // 
            this.btnScale25.Name = "btnScale25";
            this.btnScale25.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnScale25.SmallImage")));
            this.btnScale25.Text = "&25%";
            // 
            // btnScale50
            // 
            this.btnScale50.Name = "btnScale50";
            this.btnScale50.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnScale50.SmallImage")));
            this.btnScale50.Text = "&50%";
            // 
            // btnScale75
            // 
            this.btnScale75.Name = "btnScale75";
            this.btnScale75.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnScale75.SmallImage")));
            this.btnScale75.Text = "&75%";
            // 
            // btnScale150
            // 
            this.btnScale150.Name = "btnScale150";
            this.btnScale150.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnScale150.SmallImage")));
            this.btnScale150.Text = "15&0%";
            // 
            // btnScale200
            // 
            this.btnScale200.Name = "btnScale200";
            this.btnScale200.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnScale200.SmallImage")));
            this.btnScale200.Text = "200&%";
            // 
            // btnScale300
            // 
            this.btnScale300.Name = "btnScale300";
            this.btnScale300.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnScale300.SmallImage")));
            this.btnScale300.Text = "&300%";
            // 
            // RibbonButton197
            // 
            this.RibbonButton197.Name = "RibbonButton197";
            this.RibbonButton197.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton197.SmallImage")));
            this.RibbonButton197.Text = "&Custom...";
            // 
            // ribbonSeparator14
            // 
            this.ribbonSeparator14.Name = "ribbonSeparator14";
            // 
            // rbSetPicture
            // 
            this.rbSetPicture.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbSetPicture.LargeImage")));
            this.rbSetPicture.Name = "rbSetPicture";
            this.rbSetPicture.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSetPicture.SmallImage")));
            this.rbSetPicture.Text = "Set Picture";
            this.rbSetPicture.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbSetPicture.Click += new System.EventHandler(this.btnSetPicture_Click);
            // 
            // rbClearPicture
            // 
            this.rbClearPicture.Enabled = false;
            this.rbClearPicture.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbClearPicture.LargeImage")));
            this.rbClearPicture.Name = "rbClearPicture";
            this.rbClearPicture.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbClearPicture.SmallImage")));
            this.rbClearPicture.Text = "Clear Picture";
            this.rbClearPicture.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            // 
            // ribbonSeparator15
            // 
            this.ribbonSeparator15.Name = "ribbonSeparator15";
            // 
            // RibbonButton144
            // 
            this.RibbonButton144.Name = "RibbonButton144";
            this.RibbonButton144.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton144.SmallImage")));
            // 
            // RibbonButton145
            // 
            this.RibbonButton145.Name = "RibbonButton145";
            this.RibbonButton145.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton145.SmallImage")));
            // 
            // RibbonButton146
            // 
            this.RibbonButton146.Name = "RibbonButton146";
            this.RibbonButton146.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton146.SmallImage")));
            // 
            // RibbonButton147
            // 
            this.RibbonButton147.Name = "RibbonButton147";
            this.RibbonButton147.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton147.SmallImage")));
            // 
            // RibbonButton148
            // 
            this.RibbonButton148.Name = "RibbonButton148";
            this.RibbonButton148.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton148.SmallImage")));
            // 
            // RibbonButton149
            // 
            this.RibbonButton149.Name = "RibbonButton149";
            this.RibbonButton149.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton149.SmallImage")));
            // 
            // RibbonButton150
            // 
            this.RibbonButton150.Name = "RibbonButton150";
            this.RibbonButton150.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton150.SmallImage")));
            // 
            // RibbonButton151
            // 
            this.RibbonButton151.Name = "RibbonButton151";
            this.RibbonButton151.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton151.SmallImage")));
            // 
            // RibbonButton152
            // 
            this.RibbonButton152.Name = "RibbonButton152";
            this.RibbonButton152.SmallImage = ((System.Drawing.Image)(resources.GetObject("RibbonButton152.SmallImage")));
            // 
            // rtChartTools
            // 
            this.rtChartTools.Groups.Add(this.rpChartType);
            this.rtChartTools.Groups.Add(this.rpChartData);
            this.rtChartTools.Groups.Add(this.rpChartArrange);
            this.rtChartTools.Groups.Add(this.rpChartLocation);
            this.rtChartTools.Groups.Add(this.rpChartMove);
            this.rtChartTools.Name = "rtChartTools";
            this.rtChartTools.Text = "CHART TOOLS";
            // 
            // rpChartType
            // 
            this.rpChartType.Items.Add(this.rbChartChangeChartType);
            this.rpChartType.Name = "rpChartType";
            this.rpChartType.Text = "Type";
            // 
            // rbChartChangeChartType
            // 
            this.rbChartChangeChartType.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbChartChangeChartType.LargeImage")));
            this.rbChartChangeChartType.Name = "rbChartChangeChartType";
            this.rbChartChangeChartType.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbChartChangeChartType.SmallImage")));
            this.rbChartChangeChartType.Text = "Change ChartType";
            this.rbChartChangeChartType.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbChartChangeChartType.Click += new System.EventHandler(this.btnChangeChartType_Click);
            // 
            // rpChartData
            // 
            this.rpChartData.Items.Add(this.rbChartSwitchRowColumn);
            this.rpChartData.Items.Add(this.rbChartSelectData);
            this.rpChartData.Name = "rpChartData";
            this.rpChartData.Text = "Data";
            // 
            // rbChartSwitchRowColumn
            // 
            this.rbChartSwitchRowColumn.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbChartSwitchRowColumn.LargeImage")));
            this.rbChartSwitchRowColumn.Name = "rbChartSwitchRowColumn";
            this.rbChartSwitchRowColumn.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbChartSwitchRowColumn.SmallImage")));
            this.rbChartSwitchRowColumn.Text = "Switch Row/Column";
            this.rbChartSwitchRowColumn.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbChartSwitchRowColumn.Click += new System.EventHandler(this.btnSwitchRowColumn_Click);
            // 
            // rbChartSelectData
            // 
            this.rbChartSelectData.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbChartSelectData.LargeImage")));
            this.rbChartSelectData.Name = "rbChartSelectData";
            this.rbChartSelectData.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbChartSelectData.SmallImage")));
            this.rbChartSelectData.Text = "Select Data";
            this.rbChartSelectData.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbChartSelectData.Click += new System.EventHandler(this.btnselectData_Click);
            // 
            // rpChartArrange
            // 
            this.rpChartArrange.Items.Add(this.rbChartSendToBack);
            this.rpChartArrange.Items.Add(this.rbChartBringToFront);
            this.rpChartArrange.Name = "rpChartArrange";
            this.rpChartArrange.Text = "Arrange";
            // 
            // rbChartSendToBack
            // 
            this.rbChartSendToBack.Name = "rbChartSendToBack";
            this.rbChartSendToBack.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbChartSendToBack.SmallImage")));
            this.rbChartSendToBack.Text = "Send to Back";
            this.rbChartSendToBack.Click += new System.EventHandler(this.btnSendToBack_Click);
            // 
            // rbChartBringToFront
            // 
            this.rbChartBringToFront.Name = "rbChartBringToFront";
            this.rbChartBringToFront.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbChartBringToFront.SmallImage")));
            this.rbChartBringToFront.Text = "Bring to Front";
            this.rbChartBringToFront.Click += new System.EventHandler(this.btnBringToFront_Click);
            // 
            // rpChartLocation
            // 
            this.rpChartLocation.Items.Add(this.rbMoveChart);
            this.rpChartLocation.Name = "rpChartLocation";
            this.rpChartLocation.Text = "Location";
            // 
            // rbMoveChart
            // 
            this.rbMoveChart.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbMoveChart.LargeImage")));
            this.rbMoveChart.Name = "rbMoveChart";
            this.rbMoveChart.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbMoveChart.SmallImage")));
            this.rbMoveChart.Text = "Move Chart";
            this.rbMoveChart.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbMoveChart.Click += new System.EventHandler(this.btnMoveChart_Click);
            // 
            // rpChartMove
            // 
            this.rpChartMove.Items.Add(this.rbChartMove);
            this.rpChartMove.Items.Add(this.rbChartSize);
            this.rpChartMove.Items.Add(this.ribbonSeparator16);
            this.rpChartMove.Items.Add(this.rbChartMoveWithCell);
            this.rpChartMove.Items.Add(this.rbChartSizeWithCell);
            this.rpChartMove.Name = "rpChartMove";
            this.rpChartMove.Text = "Move && Size";
            // 
            // rbChartMove
            // 
            this.rbChartMove.Items.Add(this.rbChartMoveHV);
            this.rbChartMove.Items.Add(this.rbChartMoveH);
            this.rbChartMove.Items.Add(this.rbChartMoveV);
            this.rbChartMove.Items.Add(this.rbChartMoveNone);
            this.rbChartMove.Label = "Allow Move:    ";
            this.rbChartMove.Name = "rbChartMove";
            // 
            // rbChartMoveHV
            // 
            this.rbChartMoveHV.Name = "rbChartMoveHV";
            this.rbChartMoveHV.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbChartMoveHV.SmallImage")));
            this.rbChartMoveHV.Text = "Both";
            // 
            // rbChartMoveH
            // 
            this.rbChartMoveH.Name = "rbChartMoveH";
            this.rbChartMoveH.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbChartMoveH.SmallImage")));
            this.rbChartMoveH.Text = "Horizontal";
            // 
            // rbChartMoveV
            // 
            this.rbChartMoveV.Name = "rbChartMoveV";
            this.rbChartMoveV.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbChartMoveV.SmallImage")));
            this.rbChartMoveV.Text = "Vertical";
            // 
            // rbChartMoveNone
            // 
            this.rbChartMoveNone.Name = "rbChartMoveNone";
            this.rbChartMoveNone.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbChartMoveNone.SmallImage")));
            this.rbChartMoveNone.Text = "None";
            // 
            // rbChartSize
            // 
            this.rbChartSize.Items.Add(this.rbChartSizeBoth);
            this.rbChartSize.Items.Add(this.rbChartSizeWidth);
            this.rbChartSize.Items.Add(this.rbChartSizeHeight);
            this.rbChartSize.Items.Add(this.rbChartSizeNone);
            this.rbChartSize.Label = "Allow Resize: ";
            this.rbChartSize.Name = "rbChartSize";
            // 
            // rbChartSizeBoth
            // 
            this.rbChartSizeBoth.Name = "rbChartSizeBoth";
            this.rbChartSizeBoth.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbChartSizeBoth.SmallImage")));
            this.rbChartSizeBoth.Text = "Both";
            // 
            // rbChartSizeWidth
            // 
            this.rbChartSizeWidth.Name = "rbChartSizeWidth";
            this.rbChartSizeWidth.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbChartSizeWidth.SmallImage")));
            this.rbChartSizeWidth.Text = "Horizontal";
            // 
            // rbChartSizeHeight
            // 
            this.rbChartSizeHeight.Name = "rbChartSizeHeight";
            this.rbChartSizeHeight.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbChartSizeHeight.SmallImage")));
            this.rbChartSizeHeight.Text = "Vertical";
            // 
            // rbChartSizeNone
            // 
            this.rbChartSizeNone.Name = "rbChartSizeNone";
            this.rbChartSizeNone.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbChartSizeNone.SmallImage")));
            this.rbChartSizeNone.Text = "None";
            // 
            // ribbonSeparator16
            // 
            this.ribbonSeparator16.Name = "ribbonSeparator16";
            // 
            // rbChartMoveWithCell
            // 
            this.rbChartMoveWithCell.Checked = true;
            this.rbChartMoveWithCell.Name = "rbChartMoveWithCell";
            this.rbChartMoveWithCell.Text = "Move with Cells";
            // 
            // rbChartSizeWithCell
            // 
            this.rbChartSizeWithCell.Checked = true;
            this.rbChartSizeWithCell.Name = "rbChartSizeWithCell";
            this.rbChartSizeWithCell.Text = "Resize with Cells";
            // 
            // rtSparkline
            // 
            this.rtSparkline.Groups.Add(this.rpSparklineEditData);
            this.rtSparkline.Groups.Add(this.rpSparklineType);
            this.rtSparkline.Groups.Add(this.rpSparklineShow);
            this.rtSparkline.Groups.Add(this.rpSparklineStyle);
            this.rtSparkline.Groups.Add(this.rpSparklineGroup);
            this.rtSparkline.Name = "rtSparkline";
            this.rtSparkline.Text = "SPARKLINE TOOLS";
            // 
            // rpSparklineEditData
            // 
            this.rpSparklineEditData.Items.Add(this.rbSparklineEditData);
            this.rpSparklineEditData.Name = "rpSparklineEditData";
            this.rpSparklineEditData.Text = "Sparkline";
            // 
            // rbSparklineEditData
            // 
            this.rbSparklineEditData.Items.Add(this.rbSparklineEditDataGroup);
            this.rbSparklineEditData.Items.Add(this.rbSparklineEditDataSingle);
            this.rbSparklineEditData.Items.Add(this.ribbonSeparator19);
            this.rbSparklineEditData.Items.Add(this.rbSparklineEditDataHidden);
            this.rbSparklineEditData.Items.Add(this.rbSparklineEditDataSwitch);
            this.rbSparklineEditData.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbSparklineEditData.LargeImage")));
            this.rbSparklineEditData.Name = "rbSparklineEditData";
            this.rbSparklineEditData.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSparklineEditData.SmallImage")));
            this.rbSparklineEditData.Text = "Edit Data";
            this.rbSparklineEditData.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            // 
            // rbSparklineEditDataGroup
            // 
            this.rbSparklineEditDataGroup.Name = "rbSparklineEditDataGroup";
            this.rbSparklineEditDataGroup.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSparklineEditDataGroup.SmallImage")));
            this.rbSparklineEditDataGroup.Text = "&Edit Group Location && Data...";
            // 
            // rbSparklineEditDataSingle
            // 
            this.rbSparklineEditDataSingle.Name = "rbSparklineEditDataSingle";
            this.rbSparklineEditDataSingle.Text = "Edit &Single Sparkline\'s Data...";
            // 
            // ribbonSeparator19
            // 
            this.ribbonSeparator19.Name = "ribbonSeparator19";
            // 
            // rbSparklineEditDataHidden
            // 
            this.rbSparklineEditDataHidden.Name = "rbSparklineEditDataHidden";
            this.rbSparklineEditDataHidden.Text = "&Hidden && Empty Cells...";
            // 
            // rbSparklineEditDataSwitch
            // 
            this.rbSparklineEditDataSwitch.Name = "rbSparklineEditDataSwitch";
            this.rbSparklineEditDataSwitch.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSparklineEditDataSwitch.SmallImage")));
            this.rbSparklineEditDataSwitch.Text = "S&witch Row/Column";
            // 
            // rpSparklineType
            // 
            this.rpSparklineType.Items.Add(this.rbSparklineTypeLine);
            this.rpSparklineType.Items.Add(this.rbSparklineTypeColumn);
            this.rpSparklineType.Items.Add(this.rbSparklineTypeWinLoss);
            this.rpSparklineType.Name = "rpSparklineType";
            this.rpSparklineType.Text = "Type";
            // 
            // rbSparklineTypeLine
            // 
            this.rbSparklineTypeLine.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbSparklineTypeLine.LargeImage")));
            this.rbSparklineTypeLine.Name = "rbSparklineTypeLine";
            this.rbSparklineTypeLine.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSparklineTypeLine.SmallImage")));
            this.rbSparklineTypeLine.Text = "Line";
            this.rbSparklineTypeLine.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbSparklineTypeLine.Click += new System.EventHandler(this.ConvertLine_Click);
            // 
            // rbSparklineTypeColumn
            // 
            this.rbSparklineTypeColumn.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbSparklineTypeColumn.LargeImage")));
            this.rbSparklineTypeColumn.Name = "rbSparklineTypeColumn";
            this.rbSparklineTypeColumn.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSparklineTypeColumn.SmallImage")));
            this.rbSparklineTypeColumn.Text = "Column";
            this.rbSparklineTypeColumn.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbSparklineTypeColumn.Click += new System.EventHandler(this.ConvertColumn_Click);
            // 
            // rbSparklineTypeWinLoss
            // 
            this.rbSparklineTypeWinLoss.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbSparklineTypeWinLoss.LargeImage")));
            this.rbSparklineTypeWinLoss.Name = "rbSparklineTypeWinLoss";
            this.rbSparklineTypeWinLoss.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSparklineTypeWinLoss.SmallImage")));
            this.rbSparklineTypeWinLoss.Text = "Win/Loss";
            this.rbSparklineTypeWinLoss.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.rbSparklineTypeWinLoss.Click += new System.EventHandler(this.ConvertWinLoss_Click);
            // 
            // rpSparklineShow
            // 
            this.rpSparklineShow.Items.Add(this.rbSparklineHighPoint);
            this.rpSparklineShow.Items.Add(this.rbSparklineLowPoint);
            this.rpSparklineShow.Items.Add(this.rbSparklineMarkerColorNegativePoint);
            this.rpSparklineShow.Items.Add(this.rbSparklineFirstPoint);
            this.rpSparklineShow.Items.Add(this.rbSparklineLastPoint);
            this.rpSparklineShow.Items.Add(this.rbSparklineMarkerColorMarker);
            this.rpSparklineShow.Name = "rpSparklineShow";
            this.rpSparklineShow.Text = "Show";
            // 
            // rbSparklineHighPoint
            // 
            this.rbSparklineHighPoint.Name = "rbSparklineHighPoint";
            this.rbSparklineHighPoint.Text = "High Point";
            // 
            // rbSparklineLowPoint
            // 
            this.rbSparklineLowPoint.Name = "rbSparklineLowPoint";
            this.rbSparklineLowPoint.Text = "Low Point";
            // 
            // rbSparklineMarkerColorNegativePoint
            // 
            this.rbSparklineMarkerColorNegativePoint.Name = "rbSparklineMarkerColorNegativePoint";
            this.rbSparklineMarkerColorNegativePoint.Text = "Negative Points";
            // 
            // rbSparklineFirstPoint
            // 
            this.rbSparklineFirstPoint.Name = "rbSparklineFirstPoint";
            this.rbSparklineFirstPoint.Text = "First Point";
            // 
            // rbSparklineLastPoint
            // 
            this.rbSparklineLastPoint.Name = "rbSparklineLastPoint";
            this.rbSparklineLastPoint.Text = "Last Points";
            // 
            // rbSparklineMarkerColorMarker
            // 
            this.rbSparklineMarkerColorMarker.Enabled = false;
            this.rbSparklineMarkerColorMarker.Name = "rbSparklineMarkerColorMarker";
            this.rbSparklineMarkerColorMarker.Text = "Markers";
            // 
            // rpSparklineStyle
            // 
            this.rpSparklineStyle.Items.Add(this.rbSparklineColor);
            this.rpSparklineStyle.Items.Add(this.rbSparklineWeight);
            this.rpSparklineStyle.Items.Add(this.rbSparklineMarkerColor);
            this.rpSparklineStyle.Name = "rpSparklineStyle";
            this.rpSparklineStyle.Text = "Style";
            // 
            // rbSparklineColor
            // 
            this.rbSparklineColor.Name = "rbSparklineColor";
            this.rbSparklineColor.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSparklineColor.SmallImage")));
            this.rbSparklineColor.Text = "Sparkline Color";
            this.rbSparklineColor.Click += new System.EventHandler(this.ColorPickerSparkline_Click);
            // 
            // rbSparklineWeight
            // 
            this.rbSparklineWeight.Enabled = false;
            this.rbSparklineWeight.Name = "rbSparklineWeight";
            this.rbSparklineWeight.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSparklineWeight.SmallImage")));
            this.rbSparklineWeight.Text = "Weight";
            // 
            // rbSparklineMarkerColor
            // 
            this.rbSparklineMarkerColor.Items.Add(this.rbSparklineNegativePoints);
            this.rbSparklineMarkerColor.Items.Add(this.rbSparklineMarkers);
            this.rbSparklineMarkerColor.Items.Add(this.rbSparklineMarkerColorHighPoint);
            this.rbSparklineMarkerColor.Items.Add(this.rbSparklineMarkerColorLowPoint);
            this.rbSparklineMarkerColor.Items.Add(this.rbSparklineMarkerColorFirstPoint);
            this.rbSparklineMarkerColor.Items.Add(this.rbSparklineMarkerColorLastPoint);
            this.rbSparklineMarkerColor.Name = "rbSparklineMarkerColor";
            this.rbSparklineMarkerColor.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSparklineMarkerColor.SmallImage")));
            this.rbSparklineMarkerColor.Text = "Marker Color";
            // 
            // rbSparklineNegativePoints
            // 
            this.rbSparklineNegativePoints.Name = "rbSparklineNegativePoints";
            this.rbSparklineNegativePoints.Text = "Negative Points";
            // 
            // rbSparklineMarkers
            // 
            this.rbSparklineMarkers.Name = "rbSparklineMarkers";
            this.rbSparklineMarkers.Text = "Markers";
            // 
            // rbSparklineMarkerColorHighPoint
            // 
            this.rbSparklineMarkerColorHighPoint.Name = "rbSparklineMarkerColorHighPoint";
            this.rbSparklineMarkerColorHighPoint.Text = "High Point";
            // 
            // rbSparklineMarkerColorLowPoint
            // 
            this.rbSparklineMarkerColorLowPoint.Name = "rbSparklineMarkerColorLowPoint";
            this.rbSparklineMarkerColorLowPoint.Text = "Low Point";
            // 
            // rbSparklineMarkerColorFirstPoint
            // 
            this.rbSparklineMarkerColorFirstPoint.Name = "rbSparklineMarkerColorFirstPoint";
            this.rbSparklineMarkerColorFirstPoint.Text = "First Point";
            // 
            // rbSparklineMarkerColorLastPoint
            // 
            this.rbSparklineMarkerColorLastPoint.Name = "rbSparklineMarkerColorLastPoint";
            this.rbSparklineMarkerColorLastPoint.Text = "Last Point";
            // 
            // rpSparklineGroup
            // 
            this.rpSparklineGroup.Items.Add(this.rbSparklineAxis);
            this.rpSparklineGroup.Items.Add(this.rbSparklineGroup);
            this.rpSparklineGroup.Items.Add(this.rbSparklineUngroup);
            this.rpSparklineGroup.Items.Add(this.rbSparklineClear);
            this.rpSparklineGroup.Name = "rpSparklineGroup";
            this.rpSparklineGroup.Text = "Group";
            // 
            // rbSparklineAxis
            // 
            this.rbSparklineAxis.Items.Add(this.rbSparklineAxisSubCaption1);
            this.rbSparklineAxis.Items.Add(this.rbSparklineAxisSub1);
            this.rbSparklineAxis.Items.Add(this.rbSparklineAxisSub2);
            this.rbSparklineAxis.Items.Add(this.rbSparklineAxisSub3);
            this.rbSparklineAxis.Items.Add(this.rbSparklineAxisSub4);
            this.rbSparklineAxis.Items.Add(this.rbSparklineAxisSubCaption2);
            this.rbSparklineAxis.Items.Add(this.rbSparklineAxisSub5);
            this.rbSparklineAxis.Items.Add(this.rbSparklineAxisSub6);
            this.rbSparklineAxis.Items.Add(this.rbSparklineAxisSub7);
            this.rbSparklineAxis.Items.Add(this.rbSparklineAxisSubCaption3);
            this.rbSparklineAxis.Items.Add(this.rbSparklineAxisSub8);
            this.rbSparklineAxis.Items.Add(this.rbSparklineAxisSub9);
            this.rbSparklineAxis.Items.Add(this.rbSparklineAxisSub10);
            this.rbSparklineAxis.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbSparklineAxis.LargeImage")));
            this.rbSparklineAxis.Name = "rbSparklineAxis";
            this.rbSparklineAxis.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSparklineAxis.SmallImage")));
            this.rbSparklineAxis.Text = "Axis";
            this.rbSparklineAxis.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            // 
            // rbSparklineAxisSubCaption1
            // 
            this.rbSparklineAxisSubCaption1.Name = "rbSparklineAxisSubCaption1";
            this.rbSparklineAxisSubCaption1.Text = "Horizontal Axis Options";
            // 
            // rbSparklineAxisSub1
            // 
            this.rbSparklineAxisSub1.Name = "rbSparklineAxisSub1";
            this.rbSparklineAxisSub1.Text = "&General Axis Type";
            // 
            // rbSparklineAxisSub2
            // 
            this.rbSparklineAxisSub2.Name = "rbSparklineAxisSub2";
            this.rbSparklineAxisSub2.Text = "&Date Axis Type...";
            // 
            // rbSparklineAxisSub3
            // 
            this.rbSparklineAxisSub3.Name = "rbSparklineAxisSub3";
            this.rbSparklineAxisSub3.Text = "&Show Axis";
            // 
            // rbSparklineAxisSub4
            // 
            this.rbSparklineAxisSub4.Name = "rbSparklineAxisSub4";
            this.rbSparklineAxisSub4.Text = "&Plot Data Right-to-Left";
            // 
            // rbSparklineAxisSubCaption2
            // 
            this.rbSparklineAxisSubCaption2.Name = "rbSparklineAxisSubCaption2";
            this.rbSparklineAxisSubCaption2.Text = "Vertical Axis Minimum Value Options";
            // 
            // rbSparklineAxisSub5
            // 
            this.rbSparklineAxisSub5.Name = "rbSparklineAxisSub5";
            this.rbSparklineAxisSub5.Text = "&Automatic For Each Sparkline";
            // 
            // rbSparklineAxisSub6
            // 
            this.rbSparklineAxisSub6.Name = "rbSparklineAxisSub6";
            this.rbSparklineAxisSub6.Text = "Same &for All Sparklines";
            // 
            // rbSparklineAxisSub7
            // 
            this.rbSparklineAxisSub7.Name = "rbSparklineAxisSub7";
            this.rbSparklineAxisSub7.Text = "&Custom Value...";
            // 
            // rbSparklineAxisSubCaption3
            // 
            this.rbSparklineAxisSubCaption3.Name = "rbSparklineAxisSubCaption3";
            this.rbSparklineAxisSubCaption3.Text = "Vertical Axis Maximum Value Options";
            // 
            // rbSparklineAxisSub8
            // 
            this.rbSparklineAxisSub8.Name = "rbSparklineAxisSub8";
            this.rbSparklineAxisSub8.Text = "Automatic for &Each Sparkline";
            // 
            // rbSparklineAxisSub9
            // 
            this.rbSparklineAxisSub9.Name = "rbSparklineAxisSub9";
            this.rbSparklineAxisSub9.Text = "Sa&me for All Sparklines";
            // 
            // rbSparklineAxisSub10
            // 
            this.rbSparklineAxisSub10.Name = "rbSparklineAxisSub10";
            this.rbSparklineAxisSub10.Text = "Custom &Value...";
            // 
            // rbSparklineGroup
            // 
            this.rbSparklineGroup.Enabled = false;
            this.rbSparklineGroup.Name = "rbSparklineGroup";
            this.rbSparklineGroup.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSparklineGroup.SmallImage")));
            this.rbSparklineGroup.Text = "Group";
            this.rbSparklineGroup.Click += new System.EventHandler(this.Group_Click);
            // 
            // rbSparklineUngroup
            // 
            this.rbSparklineUngroup.Enabled = false;
            this.rbSparklineUngroup.Name = "rbSparklineUngroup";
            this.rbSparklineUngroup.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSparklineUngroup.SmallImage")));
            this.rbSparklineUngroup.Text = "Ungroup";
            this.rbSparklineUngroup.Click += new System.EventHandler(this.unGroup_Click);
            // 
            // rbSparklineClear
            // 
            this.rbSparklineClear.Items.Add(this.rbSparklineClearSingle);
            this.rbSparklineClear.Items.Add(this.rbSparklineClearGroup);
            this.rbSparklineClear.Name = "rbSparklineClear";
            this.rbSparklineClear.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSparklineClear.SmallImage")));
            this.rbSparklineClear.Text = "Clear";
            // 
            // rbSparklineClearSingle
            // 
            this.rbSparklineClearSingle.Name = "rbSparklineClearSingle";
            this.rbSparklineClearSingle.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSparklineClearSingle.SmallImage")));
            this.rbSparklineClearSingle.Text = "Clear Selected Sparklines";
            // 
            // rbSparklineClearGroup
            // 
            this.rbSparklineClearGroup.Name = "rbSparklineClearGroup";
            this.rbSparklineClearGroup.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSparklineClearGroup.SmallImage")));
            this.rbSparklineClearGroup.Text = "Clear Selected Sparkline Groups";
            // 
            // rtTable
            // 
            this.rtTable.Groups.Add(this.rgProperties);
            this.rtTable.Groups.Add(this.rgTools);
            this.rtTable.Groups.Add(this.rgTableStyleOptions);
            this.rtTable.Groups.Add(this.rgTableStyles);
            this.rtTable.Name = "rtTable";
            this.rtTable.Text = "TABLE";
            // 
            // rgProperties
            // 
            this.rgProperties.Items.Add(this.rlTableName);
            this.rgProperties.Items.Add(this.rtbTableName);
            this.rgProperties.Items.Add(this.rbTableResize);
            this.rgProperties.Name = "rgProperties";
            this.rgProperties.Text = "Properties";
            // 
            // rlTableName
            // 
            this.rlTableName.Name = "rlTableName";
            this.rlTableName.Text = "Table Name:";
            // 
            // rtbTableName
            // 
            this.rtbTableName.Name = "rtbTableName";
            // 
            // rbTableResize
            // 
            this.rbTableResize.Name = "rbTableResize";
            this.rbTableResize.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbTableResize.SmallImage")));
            this.rbTableResize.Text = "Resize Table";
            // 
            // rgTools
            // 
            this.rgTools.Items.Add(this.rbTableConvertToRange);
            this.rgTools.Name = "rgTools";
            this.rgTools.Text = "Tools";
            // 
            // rbTableConvertToRange
            // 
            this.rbTableConvertToRange.Name = "rbTableConvertToRange";
            this.rbTableConvertToRange.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbTableConvertToRange.SmallImage")));
            this.rbTableConvertToRange.Text = "Convert to Range";
            // 
            // rgTableStyleOptions
            // 
            this.rgTableStyleOptions.Items.Add(this.rcTableHeaderRow);
            this.rgTableStyleOptions.Items.Add(this.rcTableTotalRow);
            this.rgTableStyleOptions.Items.Add(this.rcTableBandedRow);
            this.rgTableStyleOptions.Items.Add(this.rcTableFirstColumn);
            this.rgTableStyleOptions.Items.Add(this.rcTableLastColumn);
            this.rgTableStyleOptions.Items.Add(this.rcTableBandedColumn);
            this.rgTableStyleOptions.Items.Add(this.rcTableFilterButton);
            this.rgTableStyleOptions.Name = "rgTableStyleOptions";
            this.rgTableStyleOptions.Text = "Table Style Options";
            // 
            // rcTableHeaderRow
            // 
            this.rcTableHeaderRow.Checked = true;
            this.rcTableHeaderRow.Name = "rcTableHeaderRow";
            this.rcTableHeaderRow.Text = "Header Row";
            // 
            // rcTableTotalRow
            // 
            this.rcTableTotalRow.Name = "rcTableTotalRow";
            this.rcTableTotalRow.Text = "Total Row";
            // 
            // rcTableBandedRow
            // 
            this.rcTableBandedRow.Checked = true;
            this.rcTableBandedRow.Name = "rcTableBandedRow";
            this.rcTableBandedRow.Text = "Banded Rows";
            // 
            // rcTableFirstColumn
            // 
            this.rcTableFirstColumn.Name = "rcTableFirstColumn";
            this.rcTableFirstColumn.Text = "First Column";
            // 
            // rcTableLastColumn
            // 
            this.rcTableLastColumn.Name = "rcTableLastColumn";
            this.rcTableLastColumn.Text = "Last Column";
            // 
            // rcTableBandedColumn
            // 
            this.rcTableBandedColumn.Name = "rcTableBandedColumn";
            this.rcTableBandedColumn.Text = "Banded Column";
            // 
            // rcTableFilterButton
            // 
            this.rcTableFilterButton.Checked = true;
            this.rcTableFilterButton.Name = "rcTableFilterButton";
            this.rcTableFilterButton.Text = "Filter Button";
            // 
            // rgTableStyles
            // 
            this.rgTableStyles.Items.Add(this.rglTableStyles);
            this.rgTableStyles.Name = "rgTableStyles";
            this.rgTableStyles.Text = "Table Styles";
            // 
            // rglTableStyles
            // 
            this.rglTableStyles.MenuItems.Add(this.rbTableStyleClear);
            this.rglTableStyles.Name = "rglTableStyles";
            this.rglTableStyles.Text = "Quick Styles";
            this.rglTableStyles.VisibleItems = 6;
            // 
            // rbTableStyleClear
            // 
            this.rbTableStyleClear.Name = "rbTableStyleClear";
            this.rbTableStyleClear.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbTableStyleClear.SmallImage")));
            this.rbTableStyleClear.Text = "Clear";
            // 
            // fpSpread1
            // 
            this.fpSpread1.AccessibleDescription = "fpSpread1, Sheet1, Row 0, Column 0, ";
            this.fpSpread1.AllowUserFormulas = true;
            this.fpSpread1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fpSpread1.Location = new System.Drawing.Point(0, 175);
            this.fpSpread1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.fpSpread1.Name = "fpSpread1";
            this.fpSpread1.Sheets.AddRange(new FarPoint.Win.Spread.SheetView[] {
            this.fpSpread1_Sheet1});
            this.fpSpread1.Size = new System.Drawing.Size(1285, 386);
            this.fpSpread1.TabIndex = 1;
            this.fpSpread1.TabStripPolicy = FarPoint.Win.Spread.TabStripPolicy.Always;
            this.fpSpread1.EditModeOn += new System.EventHandler(this.fpSpread1_EditModeOn);
            this.fpSpread1.EditModeOff += new System.EventHandler(this.fpSpread1_EditModeOff);
            this.fpSpread1.SheetTabClick += new FarPoint.Win.Spread.SheetTabClickEventHandler(this.fpSpread1_SheetTabClick);
            this.fpSpread1.SheetTabDoubleClick += new FarPoint.Win.Spread.SheetTabDoubleClickEventHandler(this.fpSpread1_SheetTabDoubleClick);
            this.fpSpread1.ActiveSheetChanged += new System.EventHandler(this.fpSpread1_ActiveSheetChanged);
            this.fpSpread1.SelectionChanged += new FarPoint.Win.Spread.SelectionChangedEventHandler(this.fpSpread1_SelectionChanged);
            this.fpSpread1.EnterCell += new FarPoint.Win.Spread.EnterCellEventHandler(this.fpSpread1_EnterCell);
            this.fpSpread1.CellClick += new FarPoint.Win.Spread.CellClickEventHandler(this.fpSpread1_CellClick);
            this.fpSpread1.ShapeActivated += new System.EventHandler(this.fpSpread1_ShapeActivated);
            this.fpSpread1.ShapeDeactivated += new System.EventHandler(this.fpSpread1_ShapeDeactivated);
            this.fpSpread1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.fpSpread1_MouseDown);
            this.fpSpread1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.fpSpread1_MouseUp);
            // 
            // fpSpread1_Sheet1
            // 
            this.fpSpread1_Sheet1.Reset();
            this.fpSpread1_Sheet1.SheetName = "Sheet1";
            // 
            // formulaTextBox1
            // 
            this.formulaTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.formulaTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.formulaTextBox1.Location = new System.Drawing.Point(279, 0);
            this.formulaTextBox1.Margin = new System.Windows.Forms.Padding(0);
            this.formulaTextBox1.Multiline = false;
            this.formulaTextBox1.Name = "formulaTextBox1";
            this.formulaTextBox1.Size = new System.Drawing.Size(1005, 22);
            this.formulaTextBox1.TabIndex = 2;
            // Attach formulaTextBox1 to fpSpread1
            this.formulaTextBox1.Attach(this.fpSpread1);
            // 
            // ribbonButton19
            // 
            this.ribbonButton19.Name = "ribbonButton19";
            this.ribbonButton19.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton19.SmallImage")));
            this.ribbonButton19.Text = "&5";
            // 
            // ribbonButton20
            // 
            this.ribbonButton20.Name = "ribbonButton20";
            this.ribbonButton20.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton20.SmallImage")));
            this.ribbonButton20.Text = "&45";
            // 
            // ribbonButton21
            // 
            this.ribbonButton21.Name = "ribbonButton21";
            this.ribbonButton21.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton21.SmallImage")));
            this.ribbonButton21.Text = "&90";
            // 
            // ribbonButton22
            // 
            this.ribbonButton22.Name = "ribbonButton22";
            this.ribbonButton22.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton22.SmallImage")));
            this.ribbonButton22.Text = "1&80";
            // 
            // ribbonButton23
            // 
            this.ribbonButton23.Name = "ribbonButton23";
            this.ribbonButton23.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton23.SmallImage")));
            this.ribbonButton23.Text = "&270";
            // 
            // ribbonButton24
            // 
            this.ribbonButton24.Name = "ribbonButton24";
            this.ribbonButton24.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton24.SmallImage")));
            this.ribbonButton24.Text = "&Custom...";
            // 
            // FormulaPanel
            // 
            this.FormulaPanel.Controls.Add(this.nameBox1);
            this.FormulaPanel.Controls.Add(this.panel2);
            this.FormulaPanel.Controls.Add(this.formulaTextBox1);
            this.FormulaPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.FormulaPanel.Location = new System.Drawing.Point(0, 153);
            this.FormulaPanel.Name = "FormulaPanel";
            this.FormulaPanel.Size = new System.Drawing.Size(1285, 22);
            this.FormulaPanel.TabIndex = 4;
            // 
            // nameBox1
            // 
            this.nameBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.nameBox1.FormattingEnabled = true;
            this.nameBox1.Location = new System.Drawing.Point(0, 0);
            this.nameBox1.MaxLength = 255;
            this.nameBox1.Name = "nameBox1";
            this.nameBox1.Size = new System.Drawing.Size(194, 23);
            this.nameBox1.TabIndex = 9;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Controls.Add(this.btnFormulaEdit);
            this.panel2.Controls.Add(this.btnFormulaCancel);
            this.panel2.Controls.Add(this.btnFormulaSet);
            this.panel2.Location = new System.Drawing.Point(197, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(82, 22);
            this.panel2.TabIndex = 5;
            // 
            // btnFormulaEdit
            // 
            this.btnFormulaEdit.BackColor = System.Drawing.Color.White;
            this.btnFormulaEdit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFormulaEdit.BackgroundImage")));
            this.btnFormulaEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnFormulaEdit.FlatAppearance.BorderSize = 0;
            this.btnFormulaEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFormulaEdit.Location = new System.Drawing.Point(56, 2);
            this.btnFormulaEdit.Name = "btnFormulaEdit";
            this.btnFormulaEdit.Size = new System.Drawing.Size(21, 18);
            this.btnFormulaEdit.TabIndex = 7;
            this.btnFormulaEdit.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnFormulaEdit.UseVisualStyleBackColor = false;
            this.btnFormulaEdit.Click += new System.EventHandler(this.btnFormulaEdit_Click);
            // 
            // btnFormulaCancel
            // 
            this.btnFormulaCancel.BackColor = System.Drawing.Color.White;
            this.btnFormulaCancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFormulaCancel.BackgroundImage")));
            this.btnFormulaCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnFormulaCancel.FlatAppearance.BorderSize = 0;
            this.btnFormulaCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFormulaCancel.Location = new System.Drawing.Point(31, 2);
            this.btnFormulaCancel.Name = "btnFormulaCancel";
            this.btnFormulaCancel.Size = new System.Drawing.Size(21, 18);
            this.btnFormulaCancel.TabIndex = 6;
            this.btnFormulaCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnFormulaCancel.UseVisualStyleBackColor = false;
            // 
            // btnFormulaSet
            // 
            this.btnFormulaSet.BackColor = System.Drawing.Color.White;
            this.btnFormulaSet.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFormulaSet.BackgroundImage")));
            this.btnFormulaSet.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnFormulaSet.FlatAppearance.BorderSize = 0;
            this.btnFormulaSet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFormulaSet.Location = new System.Drawing.Point(5, 2);
            this.btnFormulaSet.Name = "btnFormulaSet";
            this.btnFormulaSet.Size = new System.Drawing.Size(21, 18);
            this.btnFormulaSet.TabIndex = 5;
            this.btnFormulaSet.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnFormulaSet.UseVisualStyleBackColor = false;
            // 
            // btnSheetSkins1
            // 
            this.btnSheetSkins1.Name = "btnSheetSkins1";
            this.btnSheetSkins1.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnSheetSkins1.SmallImage")));
            this.btnSheetSkins1.Text = "Sheet Skin";
            this.btnSheetSkins1.Click += new System.EventHandler(this.btnSheetSkins1_Click);
            // 
            // menuContext_Celltypes
            // 
            this.menuContext_Celltypes.Index = -1;
            this.menuContext_Celltypes.Text = "";
            // 
            // menuContext_Button
            // 
            this.menuContext_Button.Index = -1;
            this.menuContext_Button.Text = "";
            // 
            // menuContext_Checkbox
            // 
            this.menuContext_Checkbox.Index = -1;
            this.menuContext_Checkbox.Text = "";
            // 
            // menuContext_Combobox
            // 
            this.menuContext_Combobox.Index = -1;
            this.menuContext_Combobox.Text = "";
            // 
            // menuContext_Currency
            // 
            this.menuContext_Currency.Index = -1;
            this.menuContext_Currency.Text = "";
            // 
            // menuContext_DateTime
            // 
            this.menuContext_DateTime.Index = -1;
            this.menuContext_DateTime.Text = "";
            // 
            // menuContext_Text
            // 
            this.menuContext_Text.Index = -1;
            this.menuContext_Text.Text = "";
            // 
            // menuContext_Hyperlink
            // 
            this.menuContext_Hyperlink.Index = -1;
            this.menuContext_Hyperlink.Text = "";
            // 
            // menuContext_Label
            // 
            this.menuContext_Label.Index = -1;
            this.menuContext_Label.Text = "";
            // 
            // menuContext_Mask
            // 
            this.menuContext_Mask.Index = -1;
            this.menuContext_Mask.Text = "";
            // 
            // menuContext_Number
            // 
            this.menuContext_Number.Index = -1;
            this.menuContext_Number.Text = "";
            // 
            // menuContext_Percent
            // 
            this.menuContext_Percent.Index = -1;
            this.menuContext_Percent.Text = "";
            // 
            // menuContext_MultiOption
            // 
            this.menuContext_MultiOption.Index = -1;
            this.menuContext_MultiOption.Text = "";
            // 
            // menuContext_Progress
            // 
            this.menuContext_Progress.Index = -1;
            this.menuContext_Progress.Text = "";
            // 
            // menuContext_Slider
            // 
            this.menuContext_Slider.Index = -1;
            this.menuContext_Slider.Text = "";
            // 
            // menuContext_Image
            // 
            this.menuContext_Image.Index = -1;
            this.menuContext_Image.Text = "";
            // 
            // menuContext_General
            // 
            this.menuContext_General.Index = -1;
            this.menuContext_General.Text = "";
            // 
            // menuContext_GcTextBox
            // 
            this.menuContext_GcTextBox.Index = -1;
            this.menuContext_GcTextBox.Text = "";
            // 
            // menuContext_GcDate
            // 
            this.menuContext_GcDate.Index = -1;
            this.menuContext_GcDate.Text = "";
            // 
            // menuContext_Sep1
            // 
            this.menuContext_Sep1.Index = -1;
            this.menuContext_Sep1.Text = "";
            // 
            // menuContext_Cut
            // 
            this.menuContext_Cut.Index = -1;
            this.menuContext_Cut.Text = "";
            // 
            // menuContext_Copy
            // 
            this.menuContext_Copy.Index = -1;
            this.menuContext_Copy.Text = "";
            // 
            // menuContext_Paste
            // 
            this.menuContext_Paste.Index = -1;
            this.menuContext_Paste.Text = "";
            // 
            // menuContext_PasteAll
            // 
            this.menuContext_PasteAll.Index = -1;
            this.menuContext_PasteAll.Text = "";
            // 
            // menuContext_PasteData
            // 
            this.menuContext_PasteData.Index = -1;
            this.menuContext_PasteData.Text = "";
            // 
            // menuContext_PasteFormatting
            // 
            this.menuContext_PasteFormatting.Index = -1;
            this.menuContext_PasteFormatting.Text = "";
            // 
            // menuContext_PasteFormulas
            // 
            this.menuContext_PasteFormulas.Index = -1;
            this.menuContext_PasteFormulas.Text = "";
            // 
            // menuContext_PasteAsLink
            // 
            this.menuContext_PasteAsLink.Index = -1;
            this.menuContext_PasteAsLink.Text = "";
            // 
            // menuContext_PasteAsString
            // 
            this.menuContext_PasteAsString.Index = -1;
            this.menuContext_PasteAsString.Text = "";
            // 
            // menuContext_Sep2
            // 
            this.menuContext_Sep2.Index = -1;
            this.menuContext_Sep2.Text = "";
            // 
            // menuContext_Clear
            // 
            this.menuContext_Clear.Index = -1;
            this.menuContext_Clear.Text = "";
            // 
            // menuContext_SelectAll
            // 
            this.menuContext_SelectAll.Index = -1;
            this.menuContext_SelectAll.Text = "";
            // 
            // menuContext_SelectAllSheet
            // 
            this.menuContext_SelectAllSheet.Index = -1;
            this.menuContext_SelectAllSheet.Text = "";
            // 
            // menuContext_SelectAllCells
            // 
            this.menuContext_SelectAllCells.Index = -1;
            this.menuContext_SelectAllCells.Text = "";
            // 
            // menuContext_SelectAllData
            // 
            this.menuContext_SelectAllData.Index = -1;
            this.menuContext_SelectAllData.Text = "";
            // 
            // menuContext_Sep3
            // 
            this.menuContext_Sep3.Index = -1;
            this.menuContext_Sep3.Text = "";
            // 
            // menuContext_Sparkline
            // 
            this.menuContext_Sparkline.Index = -1;
            this.menuContext_Sparkline.Text = "";
            // 
            // menuContext_SparklineEditGroup
            // 
            this.menuContext_SparklineEditGroup.Index = -1;
            this.menuContext_SparklineEditGroup.Text = "";
            // 
            // menuContext_SparklineEditSingle
            // 
            this.menuContext_SparklineEditSingle.Index = -1;
            this.menuContext_SparklineEditSingle.Text = "";
            // 
            // menuContext_SparklineGroup
            // 
            this.menuContext_SparklineGroup.Index = -1;
            this.menuContext_SparklineGroup.Text = "";
            // 
            // menuContext_SparklineUngroup
            // 
            this.menuContext_SparklineUngroup.Index = -1;
            this.menuContext_SparklineUngroup.Text = "";
            // 
            // menuContext_SparklineClear
            // 
            this.menuContext_SparklineClear.Index = -1;
            this.menuContext_SparklineClear.Text = "";
            // 
            // menuContext_SparklineClearGroup
            // 
            this.menuContext_SparklineClearGroup.Index = -1;
            this.menuContext_SparklineClearGroup.Text = "";
            // 
            // menuContext_SparklineSwitch
            // 
            this.menuContext_SparklineSwitch.Index = -1;
            this.menuContext_SparklineSwitch.Text = "";
            // 
            // menuContext_Span
            // 
            this.menuContext_Span.Index = -1;
            this.menuContext_Span.Text = "";
            // 
            // menuContext_Lock
            // 
            this.menuContext_Lock.Index = -1;
            this.menuContext_Lock.Text = "";
            // 
            // menuContext_Borders
            // 
            this.menuContext_Borders.Index = -1;
            this.menuContext_Borders.Text = "";
            // 
            // menuContext_Insert
            // 
            this.menuContext_Insert.Index = -1;
            this.menuContext_Insert.Text = "";
            // 
            // menuContext_Delete
            // 
            this.menuContext_Delete.Index = -1;
            this.menuContext_Delete.Text = "";
            // 
            // menuContext_Headers
            // 
            this.menuContext_Headers.Index = -1;
            this.menuContext_Headers.Text = "";
            // 
            // menuContext_ColumnWidth
            // 
            this.menuContext_ColumnWidth.Index = -1;
            this.menuContext_ColumnWidth.Text = "";
            // 
            // menuContext_RowHeight
            // 
            this.menuContext_RowHeight.Index = -1;
            this.menuContext_RowHeight.Text = "";
            // 
            // menuContext_Hide
            // 
            this.menuContext_Hide.Index = -1;
            this.menuContext_Hide.Text = "";
            // 
            // menuContext_UnHide
            // 
            this.menuContext_UnHide.Index = -1;
            this.menuContext_UnHide.Text = "";
            // 
            // menuContext_UnHideSelection
            // 
            this.menuContext_UnHideSelection.Index = -1;
            this.menuContext_UnHideSelection.Text = "";
            // 
            // menuContext_UnHideAllHidden
            // 
            this.menuContext_UnHideAllHidden.Index = -1;
            this.menuContext_UnHideAllHidden.Text = "";
            // 
            // menuContext_UnHideSpecific
            // 
            this.menuContext_UnHideSpecific.Index = -1;
            this.menuContext_UnHideSpecific.Text = "";
            // 
            // menuContext_ClearCellType
            // 
            this.menuContext_ClearCellType.Index = -1;
            this.menuContext_ClearCellType.Text = "";
            // 
            // menuContext_Sep10
            // 
            this.menuContext_Sep10.Index = -1;
            this.menuContext_Sep10.Text = "";
            // 
            // menuContext_SheetSettings
            // 
            this.menuContext_SheetSettings.Index = -1;
            this.menuContext_SheetSettings.Text = "";
            // 
            // menuContext_SheetSkinDesigner
            // 
            this.menuContext_SheetSkinDesigner.Index = -1;
            this.menuContext_SheetSkinDesigner.Text = "";
            // 
            // menuContext_SpreadSkinDesigner
            // 
            this.menuContext_SpreadSkinDesigner.Index = -1;
            this.menuContext_SpreadSkinDesigner.Text = "";
            // 
            // menuContext_SheetPrintOptions
            // 
            this.menuContext_SheetPrintOptions.Index = -1;
            this.menuContext_SheetPrintOptions.Text = "";
            // 
            // menuContext_ListBox
            // 
            this.menuContext_ListBox.Index = -1;
            this.menuContext_ListBox.Text = "";
            // 
            // menuContext_MultiColumnComboBox
            // 
            this.menuContext_MultiColumnComboBox.Index = -1;
            this.menuContext_MultiColumnComboBox.Text = "";
            // 
            // menuContext_Barcode
            // 
            this.menuContext_Barcode.Index = -1;
            this.menuContext_Barcode.Text = "";
            // 
            // menuContext_ColorPicker
            // 
            this.menuContext_ColorPicker.Index = -1;
            this.menuContext_ColorPicker.Text = "";
            // 
            // menuContext_AutoFit
            // 
            this.menuContext_AutoFit.Index = -1;
            this.menuContext_AutoFit.Text = "";
            // 
            // shapeContextDelete
            // 
            this.shapeContextDelete.Index = -1;
            this.shapeContextDelete.Text = "";
            // 
            // shapeContextProperties
            // 
            this.shapeContextProperties.Index = -1;
            this.shapeContextProperties.Text = "";
            // 
            // shapeContextFormula
            // 
            this.shapeContextFormula.Index = -1;
            this.shapeContextFormula.Text = "";
            // 
            // shapeContextSep1
            // 
            this.shapeContextSep1.Index = -1;
            this.shapeContextSep1.Text = "";
            // 
            // shapeContextLocked
            // 
            this.shapeContextLocked.Index = -1;
            this.shapeContextLocked.Text = "";
            // 
            // shapeContextSep2
            // 
            this.shapeContextSep2.Index = -1;
            this.shapeContextSep2.Text = "";
            // 
            // shapeContextCut
            // 
            this.shapeContextCut.Index = -1;
            this.shapeContextCut.Text = "";
            // 
            // shapeContextCopy
            // 
            this.shapeContextCopy.Index = -1;
            this.shapeContextCopy.Text = "";
            // 
            // shapeContextPaste
            // 
            this.shapeContextPaste.Index = -1;
            this.shapeContextPaste.Text = "";
            // 
            // menuContext_RichText
            // 
            this.menuContext_RichText.Index = -1;
            this.menuContext_RichText.Text = "";
            // 
            // menuContext_RegEx
            // 
            this.menuContext_RegEx.Index = -1;
            this.menuContext_RegEx.Text = "";
            // 
            // menuContext_PasteShape
            // 
            this.menuContext_PasteShape.Index = -1;
            this.menuContext_PasteShape.Text = "";
            // 
            // menuContext_PasteChart
            // 
            this.menuContext_PasteChart.Index = -1;
            this.menuContext_PasteChart.Text = "";
            // 
            // sbpZoom
            // 
            this.sbpZoom.Name = "sbpZoom";
            // 
            // menuContext_EditNote
            // 
            this.menuContext_EditNote.Index = -1;
            this.menuContext_EditNote.Text = "";
            // 
            // menuContext_DeleteNote
            // 
            this.menuContext_DeleteNote.Index = -1;
            this.menuContext_DeleteNote.Text = "";
            // 
            // menuContext_Sep4
            // 
            this.menuContext_Sep4.Index = -1;
            this.menuContext_Sep4.Text = "";
            // 
            // menuContext_DeleteSheet
            // 
            this.menuContext_DeleteSheet.Index = -1;
            this.menuContext_DeleteSheet.Text = "";
            // 
            // c1StatusBar1
            // 
            this.c1StatusBar1.Location = new System.Drawing.Point(0, 561);
            this.c1StatusBar1.Name = "c1StatusBar1";
            this.c1StatusBar1.Size = new System.Drawing.Size(1285, 23);
            // 
            // RenameTextBox
            // 
            this.RenameTextBox.Location = new System.Drawing.Point(0, 0);
            this.RenameTextBox.Name = "RenameTextBox";
            this.RenameTextBox.Size = new System.Drawing.Size(279, 23);
            this.RenameTextBox.TabIndex = 8;
            this.RenameTextBox.TabStop = false;
            this.RenameTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RenameTextBox_KeyDown);
            this.RenameTextBox.LostFocus += new System.EventHandler(this.RenameTextBox_LostFocus);
            // 
            // ribbonTab1
            // 
            this.ribbonTab1.Groups.Add(this.ribbonGroup1);
            this.ribbonTab1.Name = "ribbonTab1";
            this.ribbonTab1.Text = "云存储";
            // 
            // ribbonGroup1
            // 
            this.ribbonGroup1.Items.Add(this.ribbonButton1List);
            this.ribbonGroup1.Items.Add(this.ribbonButton2SaveCloud);
            this.ribbonGroup1.Name = "ribbonGroup1";
            this.ribbonGroup1.Text = "分组";
            // 
            // ribbonButton1List
            // 
            this.ribbonButton1List.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton1List.LargeImage")));
            this.ribbonButton1List.Name = "ribbonButton1List";
            this.ribbonButton1List.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton1List.SmallImage")));
            this.ribbonButton1List.Text = "获得云端文件";
            this.ribbonButton1List.Click += new System.EventHandler(this.ribbonButton1List_Click);
            // 
            // ribbonButton2SaveCloud
            // 
            this.ribbonButton2SaveCloud.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton2SaveCloud.LargeImage")));
            this.ribbonButton2SaveCloud.Name = "ribbonButton2SaveCloud";
            this.ribbonButton2SaveCloud.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton2SaveCloud.SmallImage")));
            this.ribbonButton2SaveCloud.Text = "存储到云端";
            this.ribbonButton2SaveCloud.Click += new System.EventHandler(this.ribbonButton2SaveCloud_Click);
            // 
            // SpreadDesigner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1285, 584);
            this.Controls.Add(this.fpSpread1);
            this.Controls.Add(this.FormulaPanel);
            this.Controls.Add(this.c1Ribbon1);
            this.Controls.Add(this.c1StatusBar1);
            this.Controls.Add(this.RenameTextBox);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MinimumSize = new System.Drawing.Size(640, 428);
            this.Name = "SpreadDesigner";
            this.Text = "Spread Designer";
            this.VisualStyleHolder = C1.Win.C1Ribbon.VisualStyle.Office2010Silver;
            ((System.ComponentModel.ISupportInitialize)(this.c1Ribbon1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fpSpread1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fpSpread1_Sheet1)).EndInit();
            this.FormulaPanel.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sbpZoom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1StatusBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

    }

    #endregion

    private C1.Win.C1Ribbon.C1Ribbon c1Ribbon1;
    private C1.Win.C1Ribbon.RibbonApplicationMenu ribbonApplicationMenu1;
    private C1.Win.C1Ribbon.RibbonConfigToolBar ribbonConfigToolBar1;
    private C1.Win.C1Ribbon.RibbonTab rtHome;
    private C1.Win.C1Ribbon.RibbonGroup rpClipboard;
    private C1.Win.C1Ribbon.RibbonButton rbCut;
    private C1.Win.C1Ribbon.RibbonButton rbCopy;
    private C1.Win.C1Ribbon.RibbonGroup rpFont;
    private C1.Win.C1Ribbon.RibbonFontComboBox rcbFont;
    private C1.Win.C1Ribbon.RibbonComboBox rcbFontSize;
    private C1.Win.C1Ribbon.RibbonToolBar ribbonToolBar1;
    private C1.Win.C1Ribbon.RibbonToolBar ribbonToolBar2;
    private C1.Win.C1Ribbon.RibbonToggleButton rbBold;
    private C1.Win.C1Ribbon.RibbonToggleButton rbItalic;
    private C1.Win.C1Ribbon.RibbonToggleButton rbUnderLine;
    private C1.Win.C1Ribbon.RibbonToolBar ribbonToolBar3;
    private C1.Win.C1Ribbon.RibbonSeparator ribbonSeparator1;
    private C1.Win.C1Ribbon.RibbonButton rbIncreaseFontSize;
    private C1.Win.C1Ribbon.RibbonButton rbDecreaseFontSize;
    private C1.Win.C1Ribbon.RibbonToolBar ribbonToolBar5;
    private C1.Win.C1Ribbon.RibbonToolBar ribbonToolBar6;
    private C1.Win.C1Ribbon.RibbonGroup rpAlignment;
    private C1.Win.C1Ribbon.RibbonToolBar ribbonToolBar4;
    private C1.Win.C1Ribbon.RibbonToggleButton rbTopAlign;
    private C1.Win.C1Ribbon.RibbonToggleButton rbMiddleAlign;
    private C1.Win.C1Ribbon.RibbonToggleButton rbBottomAlign;
    private C1.Win.C1Ribbon.RibbonToolBar ribbonToolBar7;
    private C1.Win.C1Ribbon.RibbonToggleButton rbAlignTextLeft;
    private C1.Win.C1Ribbon.RibbonToggleButton rbAlignTextMiddle;
    private C1.Win.C1Ribbon.RibbonToggleButton rbAlignTextRight;
    private C1.Win.C1Ribbon.RibbonTab rtInsert;
    private C1.Win.C1Ribbon.RibbonGroup rplllustrations;
    private C1.Win.C1Ribbon.RibbonButton rbPicture;
    private C1.Win.C1Ribbon.RibbonSeparator ribbonSeparator2;
    private C1.Win.C1Ribbon.RibbonColorPicker rccFillColor;
    private C1.Win.C1Ribbon.RibbonColorPicker rccFontColor;
    private C1.Win.C1Ribbon.RibbonTab rtpageLayout;
    private C1.Win.C1Ribbon.RibbonGroup rpPageSetup;
    private C1.Win.C1Ribbon.RibbonTab rtData;
    private C1.Win.C1Ribbon.RibbonGroup rpOutline;
    private C1.Win.C1Ribbon.RibbonTab rtView;
    private C1.Win.C1Ribbon.RibbonGroup rpShowHide;
    private C1.Win.C1Ribbon.RibbonTab rtDrawingTools;
    private C1.Win.C1Ribbon.RibbonGroup RibbonPanel13;
    private C1.Win.C1Ribbon.RibbonGroup rpText;
    private C1.Win.C1Ribbon.RibbonButton rbSymbol;
    private C1.Win.C1Ribbon.RibbonButton rbWordArt;
    private C1.Win.C1Ribbon.RibbonButton rbAnnotactionMode;
    private C1.Win.C1Ribbon.RibbonGroup rpDelete;
    private C1.Win.C1Ribbon.RibbonSplitButton rbDeleteShape;
    private C1.Win.C1Ribbon.RibbonGroup rpSparkline;
    private C1.Win.C1Ribbon.RibbonButton rbSparklineLine;
    private C1.Win.C1Ribbon.RibbonGroup rpCameraShape;
    private C1.Win.C1Ribbon.RibbonButton rbSparklineColumn;
    private C1.Win.C1Ribbon.RibbonButton rbSparklineWinLoss;
    private C1.Win.C1Ribbon.RibbonButton rbCameraShape;
    private C1.Win.C1Ribbon.RibbonGroup rpChart;
    private C1.Win.C1Ribbon.RibbonSplitButton rbOrientation;
    private C1.Win.C1Ribbon.RibbonSplitButton RibbonButton263;
    private C1.Win.C1Ribbon.RibbonButton rbBackGround;
    private C1.Win.C1Ribbon.RibbonButton rbPrintTitles;
    private C1.Win.C1Ribbon.RibbonButton rbSmartPrint;
    private C1.Win.C1Ribbon.RibbonButton rbGroup;
    private C1.Win.C1Ribbon.RibbonButton rbUnGroup;
    private C1.Win.C1Ribbon.RibbonSeparator ribbonSeparator3;
    private C1.Win.C1Ribbon.RibbonButton rbExpandGroup;
    private C1.Win.C1Ribbon.RibbonButton rbCollapseGroup;
    private C1.Win.C1Ribbon.RibbonGroup rpSort;
    private C1.Win.C1Ribbon.RibbonGroup rpCustomName;
    private C1.Win.C1Ribbon.RibbonButton rbSortAZ;
    private C1.Win.C1Ribbon.RibbonButton rbSortZA;
    private C1.Win.C1Ribbon.RibbonButton rbSort;
    private C1.Win.C1Ribbon.RibbonButton rbAddCustomname;
    private C1.Win.C1Ribbon.RibbonCheckBox rbRowHeader;
    private C1.Win.C1Ribbon.RibbonCheckBox rbColumnHeader;
    private C1.Win.C1Ribbon.RibbonSeparator ribbonSeparator4;
    private C1.Win.C1Ribbon.RibbonCheckBox rbVerticalGridLine;
    private C1.Win.C1Ribbon.RibbonCheckBox rbHorizontalGridLine;
    private C1.Win.C1Ribbon.RibbonSeparator ribbonSeparator5;
    private C1.Win.C1Ribbon.RibbonCheckBox rbFormulaBar;
    private C1.Win.C1Ribbon.RibbonGroup rpZoom;
    private C1.Win.C1Ribbon.RibbonButton rbZoom;
    private C1.Win.C1Ribbon.RibbonButton rb100Percent;
    private C1.Win.C1Ribbon.RibbonGroup rpWindow;
    private C1.Win.C1Ribbon.RibbonButton rbZoomToSelection;
    private C1.Win.C1Ribbon.RibbonSplitButton rbFreezePanes;
    private C1.Win.C1Ribbon.RibbonGroup RibbonPanel14;
    private C1.Win.C1Ribbon.RibbonSplitButton rbPaste;
    private C1.Win.C1Ribbon.RibbonGroup rpCellType;
    private C1.Win.C1Ribbon.RibbonButton rbDecreaseIndent;
    private C1.Win.C1Ribbon.RibbonButton rbIncreaseIndent;
    private C1.Win.C1Ribbon.RibbonSeparator ribbonSeparator6;
    private C1.Win.C1Ribbon.RibbonToggleButton rbWrapText;
    private C1.Win.C1Ribbon.RibbonButton rbClearCellType;
    private C1.Win.C1Ribbon.RibbonGroup rpStyle;
    private C1.Win.C1Ribbon.RibbonSplitButton rbConditionalFormat;
    private C1.Win.C1Ribbon.RibbonGroup rpEditing;
    private C1.Win.C1Ribbon.RibbonToggleButton rbLock;
    private C1.Win.C1Ribbon.RibbonButton rbClearAll;
    private C1.Win.C1Ribbon.RibbonSplitButton rbSelectAll;
    private C1.Win.C1Ribbon.RibbonSeparator ribbonSeparator7;
    private C1.Win.C1Ribbon.RibbonSplitButton rbSortFilter;
    private C1.Win.C1Ribbon.RibbonSplitButton rbFind2;
    private FarPoint.Win.Spread.FpSpread fpSpread1;
    private FarPoint.Win.Spread.SheetView fpSpread1_Sheet1;
    private FarPoint.Win.Spread.FormulaTextBox formulaTextBox1;
    private C1.Win.C1Ribbon.RibbonQat ribbonQat1;
    private C1.Win.C1Ribbon.RibbonButton rbPasteAll;
    private C1.Win.C1Ribbon.RibbonButton rbPasteValues;
    private C1.Win.C1Ribbon.RibbonButton rbPasteFormatting;
    private C1.Win.C1Ribbon.RibbonButton rbPasteFormulas;
    private C1.Win.C1Ribbon.RibbonButton rbPasteAsLink;
    private C1.Win.C1Ribbon.RibbonButton rbPasteAsString;
    private C1.Win.C1Ribbon.RibbonButton rbPasteAsShape;
    private C1.Win.C1Ribbon.RibbonButton rbPasteAsChart;
    private C1.Win.C1Ribbon.RibbonSeparator ribbonSeparator8;
    private C1.Win.C1Ribbon.RibbonMenu rbCFHighLightCellsRules;
    private C1.Win.C1Ribbon.RibbonButton rbGreaterThan;
    private C1.Win.C1Ribbon.RibbonButton rbLessThan;
    private C1.Win.C1Ribbon.RibbonMenu rbCFTopBottomRules;
    private C1.Win.C1Ribbon.RibbonButton rbCFTop10Items;
    private C1.Win.C1Ribbon.RibbonButton rbCFTop10Per;
    private C1.Win.C1Ribbon.RibbonButton rbCFBottom10Items;
    private C1.Win.C1Ribbon.RibbonButton rbBetween;
    private C1.Win.C1Ribbon.RibbonButton rbCFEqualTo;
    private C1.Win.C1Ribbon.RibbonButton rbCFTextThatContains;
    private C1.Win.C1Ribbon.RibbonButton rbCFADateOccurring;
    private C1.Win.C1Ribbon.RibbonButton rbCFDuplicateValues;
    private C1.Win.C1Ribbon.RibbonButton rbCFBottom10Per;
    private C1.Win.C1Ribbon.RibbonButton rbCFAboveAverage;
    private C1.Win.C1Ribbon.RibbonButton rbCFBelowAverage;
    private C1.Win.C1Ribbon.RibbonSeparator ribbonSeparator9;
    private C1.Win.C1Ribbon.RibbonButton rbCFTopBottomMoreRules;
    private C1.Win.C1Ribbon.RibbonSeparator ribbonSeparator10;
    private C1.Win.C1Ribbon.RibbonButton rbCFNewRule;
    private C1.Win.C1Ribbon.RibbonMenu rbCFClearRules;
    private C1.Win.C1Ribbon.RibbonButton rbCFManagerRules;
    private C1.Win.C1Ribbon.RibbonGallery rbCFColorScales;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCFGreenYellowRed;
    private C1.Win.C1Ribbon.RibbonGalleryItem ItemRedYellowGreenScale;
    private C1.Win.C1Ribbon.RibbonGalleryItem ItemGreenWhiteRedScale;
    private C1.Win.C1Ribbon.RibbonGalleryItem ItemRedWhiteGreenScale;
    private C1.Win.C1Ribbon.RibbonGalleryItem ItemBlueWhiteRedScale;
    private C1.Win.C1Ribbon.RibbonGalleryItem ItemRedWhiteBlueScale;
    private C1.Win.C1Ribbon.RibbonGalleryItem ItemWhiteRedScale;
    private C1.Win.C1Ribbon.RibbonGalleryItem ItemRedWhiteScale;
    private C1.Win.C1Ribbon.RibbonGalleryItem ItemGreenWihte;
    private C1.Win.C1Ribbon.RibbonButton rbCFColorScaleMoreRule;
    private C1.Win.C1Ribbon.RibbonButton rbCFClearFromSelectedCell;
    private C1.Win.C1Ribbon.RibbonButton rbCFClearEntireSheet;
    private C1.Win.C1Ribbon.RibbonButton rbSelectAllSheet;
    private C1.Win.C1Ribbon.RibbonButton rbSelectAllCells;
    private C1.Win.C1Ribbon.RibbonButton rbSelectAllData;
    private C1.Win.C1Ribbon.RibbonButton rbSortAToZ;
    private C1.Win.C1Ribbon.RibbonButton rbSortZToA;
    private C1.Win.C1Ribbon.RibbonButton rbCustomSort;
    private C1.Win.C1Ribbon.RibbonButton rbFind;
    private C1.Win.C1Ribbon.RibbonButton rbGoto;
    private C1.Win.C1Ribbon.RibbonButton rbFreezeRowsAndColumns;
    private C1.Win.C1Ribbon.RibbonButton rbFreezeTopRow;
    private C1.Win.C1Ribbon.RibbonButton rbFreezeFirstColumn;
    private C1.Win.C1Ribbon.RibbonButton rbFreezeTrailingRowsAndColumns;
    private C1.Win.C1Ribbon.RibbonButton rbPrintAreaSet;
    private C1.Win.C1Ribbon.RibbonButton rbPrintAreaClear;
    private C1.Win.C1Ribbon.RibbonButton rbOrientationNormal;
    private C1.Win.C1Ribbon.RibbonButton rbOrientationPortraint;
    private C1.Win.C1Ribbon.RibbonButton rbOrientationLandscape;
    private C1.Win.C1Ribbon.RibbonTab rtSetting;
    private C1.Win.C1Ribbon.RibbonGroup rpSpreadSettings;
    private C1.Win.C1Ribbon.RibbonButton rbSpreadSettingGeneral;
    private C1.Win.C1Ribbon.RibbonButton rbSpreadSettingEdit;
    private C1.Win.C1Ribbon.RibbonButton rbSpreadSettingScrollBar;
    private C1.Win.C1Ribbon.RibbonButton rbSpreadSettingSplitBox;
    private C1.Win.C1Ribbon.RibbonButton rbSpreadSettingView;
    private C1.Win.C1Ribbon.RibbonButton rbSpreadSettingTitles;
    private C1.Win.C1Ribbon.RibbonGroup rpSheetSettings;
    private C1.Win.C1Ribbon.RibbonButton rbSheetSettingGeneral;
    private C1.Win.C1Ribbon.RibbonButton rbSheetSettingColor;
    private C1.Win.C1Ribbon.RibbonButton rbSheetSettingHeaders;
    private C1.Win.C1Ribbon.RibbonButton rbSheetSettingGridLines;
    private C1.Win.C1Ribbon.RibbonButton rbSheetSettingCalculation;
    private C1.Win.C1Ribbon.RibbonButton rbSheetSettingFonts;
    private C1.Win.C1Ribbon.RibbonGroup rpAppearanceSettings;
    private C1.Win.C1Ribbon.RibbonButton rbSpreadSkin;
    private C1.Win.C1Ribbon.RibbonButton rbSheetSkin;
    private C1.Win.C1Ribbon.RibbonButton rbFocusIndicator;
    private C1.Win.C1Ribbon.RibbonButton rbStyle;
    private C1.Win.C1Ribbon.RibbonButton rbTabStrip;
    private C1.Win.C1Ribbon.RibbonButton rbAlternatingRow;
    private C1.Win.C1Ribbon.RibbonGroup rpOtherSettings;
    private C1.Win.C1Ribbon.RibbonButton rbGroupInfo;
    private C1.Win.C1Ribbon.RibbonButton rbCellColumnsandRows;
    private C1.Win.C1Ribbon.RibbonButton rbInputMap;
    private C1.Win.C1Ribbon.RibbonGroup rpDesignerSettings;
    private C1.Win.C1Ribbon.RibbonSplitButton rbPreferences;
    private C1.Win.C1Ribbon.RibbonColorPicker RibbonColorChooser1;
    private C1.Win.C1Ribbon.RibbonColorPicker RibbonColorChooser2;
    private C1.Win.C1Ribbon.RibbonColorPicker RibbonColorChooser3;
    private C1.Win.C1Ribbon.RibbonSeparator ribbonSeparator12;
    private C1.Win.C1Ribbon.RibbonMenu RibbonButton110;
    private C1.Win.C1Ribbon.RibbonMenu RibbonButton111;
    private C1.Win.C1Ribbon.RibbonMenu RibbonButton137;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton138;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton139;
    private C1.Win.C1Ribbon.RibbonSeparator ribbonSeparator13;
    private C1.Win.C1Ribbon.RibbonMenu RibbonButton140;
    private C1.Win.C1Ribbon.RibbonMenu RibbonButton141;
    private C1.Win.C1Ribbon.RibbonMenu RibbonButton142;
    private C1.Win.C1Ribbon.RibbonSeparator ribbonSeparator14;
    private C1.Win.C1Ribbon.RibbonButton rbSetPicture;
    private C1.Win.C1Ribbon.RibbonButton rbClearPicture;
    private C1.Win.C1Ribbon.RibbonSeparator ribbonSeparator15;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton144;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton145;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton146;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton147;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton148;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton149;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton150;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton151;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton152;
    private C1.Win.C1Ribbon.RibbonToggleButton rbShowStart;
    private C1.Win.C1Ribbon.RibbonToggleButton rbAllowDrag;
    private C1.Win.C1Ribbon.RibbonToggleButton rbAutoLaunch;
    private C1.Win.C1Ribbon.RibbonToggleButton rbAutomticSelectContextMenu;
    private C1.Win.C1Ribbon.RibbonToggleButton rbShowPropertyGrid;
    private C1.Win.C1Ribbon.RibbonToggleButton rbShowAllConditionalFormat;
    private C1.Win.C1Ribbon.RibbonTab rtChartTools;
    private C1.Win.C1Ribbon.RibbonGroup rpChartType;
    private C1.Win.C1Ribbon.RibbonButton rbChartChangeChartType;
    private C1.Win.C1Ribbon.RibbonGroup rpChartData;
    private C1.Win.C1Ribbon.RibbonButton rbChartSwitchRowColumn;
    private C1.Win.C1Ribbon.RibbonButton rbChartSelectData;
    private C1.Win.C1Ribbon.RibbonGroup rpChartArrange;
    private C1.Win.C1Ribbon.RibbonButton rbChartSendToBack;
    private C1.Win.C1Ribbon.RibbonButton rbChartBringToFront;
    private C1.Win.C1Ribbon.RibbonGroup rpChartLocation;
    private C1.Win.C1Ribbon.RibbonButton rbMoveChart;
    private C1.Win.C1Ribbon.RibbonGroup rpChartMove;
    private C1.Win.C1Ribbon.RibbonComboBox rbChartMove;
    private C1.Win.C1Ribbon.RibbonComboBox rbChartSize;
    private C1.Win.C1Ribbon.RibbonSeparator ribbonSeparator16;
    private C1.Win.C1Ribbon.RibbonCheckBox rbChartMoveWithCell;
    private C1.Win.C1Ribbon.RibbonCheckBox rbChartSizeWithCell;
    private C1.Win.C1Ribbon.RibbonGallery RibbonButtonChartOthers;
    private C1.Win.C1Ribbon.RibbonGalleryItem HighLowCloseChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem OpenHighLowCloseChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem CandlestickChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem XYZSurfaceChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem ribbonGalleryItem19;
    private C1.Win.C1Ribbon.RibbonGalleryItem ribbonGalleryItem20;
    private C1.Win.C1Ribbon.RibbonGalleryItem DoughnutChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem ExplodedDoughnutChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem ribbonGalleryItem23;
    private C1.Win.C1Ribbon.RibbonGalleryItem XYBubbleIn2DChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem XYBubbleIn3DChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem ribbonGalleryItem26;
    private C1.Win.C1Ribbon.RibbonGalleryItem RadarLineChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem RadarLineWithMarkerChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem RadarAreaChart;
    private C1.Win.C1Ribbon.RibbonGallery RibbonButtonChartScatter;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton50;
    private C1.Win.C1Ribbon.RibbonGallery RibbonButtonChartColumn;
    private C1.Win.C1Ribbon.RibbonGallery RibbonButtonChartLine;
    private C1.Win.C1Ribbon.RibbonGallery RibbonButtonChartPie;
    private C1.Win.C1Ribbon.RibbonGalleryItem PieIn2DChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem ExplodedPieIn2DChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem PieIn3DChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem ExplodedPieIn3DChart;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton12;
    private C1.Win.C1Ribbon.RibbonGallery RibbonButtonChartBar;
    private C1.Win.C1Ribbon.RibbonGalleryItem ClusteredBarChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem StackedBarChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem Stacked100BarChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem ClusteredBarIn3DChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem StackedBarIn3DChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem Stacked100BarIn3DChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem ClusteredHorizontalCylinderChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem StackedHorizontalCylinderChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem Stacked100HorizontalCylinderChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem ClusteredHorizontalFullConeChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem StackedHorizontalFullConeChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem Stacked100HorizontalFullConeChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem ClusteredHorizontalFullPyramidChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem Stacked100HorizontalFullPyramidChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem HighLowBarFullPyramidChart;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton13;
    private C1.Win.C1Ribbon.RibbonGallery RibbonButtonChartArea;
    private C1.Win.C1Ribbon.RibbonGalleryItem AreaIn2DChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem StackedAreaChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem Stacked100AreaChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem AreaIn3DChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem StackedAreaIn3DChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem Stacked100AreaIn3DChart;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton14;
    private C1.Win.C1Ribbon.RibbonGalleryItem XYPointChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem XYLineChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem XYLineWithMarkerChart;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton15;
    private C1.Win.C1Ribbon.RibbonGalleryItem LineIn2DChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem StackedLineChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem Stacked100LineChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem LineWithMarkersChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem StackedLineWithMarkersChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem Stacked100LineWithMarkersChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem LineIn3DChart;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton11;
    private C1.Win.C1Ribbon.RibbonGalleryItem ClusteredColumnChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem StackedColumnChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem Stacked100ColumnChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem HighLowColumnChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem ClusteredColumnIn3DChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem StackedColumnIn3DChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem Stacked100ColumnIn3DChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem ColumnIn3DChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem ClusteredCylinderChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem StackedCylinderChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem Stacked100CylinderChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem CylinderIn3DChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem ClusteredFullConeChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem StackedFullConeChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem Stacked100FullConeChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem FullConeIn3DChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem ClusteredFullPyramidChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem StackedFullPyramidChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem Stacked100FullPyramidChart;
    private C1.Win.C1Ribbon.RibbonGalleryItem FullPyramidIn3DChart;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton10;
    private C1.Win.C1Ribbon.RibbonButton rbDeleteActiveShape;
    private C1.Win.C1Ribbon.RibbonButton rbDeleteAllShape;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton180;
    private C1.Win.C1Ribbon.RibbonButton btnRotate1;
    private C1.Win.C1Ribbon.RibbonButton btnRotate5;
    private C1.Win.C1Ribbon.RibbonButton btnRotate45;
    private C1.Win.C1Ribbon.RibbonButton btnRotate90;
    private C1.Win.C1Ribbon.RibbonButton btnRotate180;
    private C1.Win.C1Ribbon.RibbonButton btnRotate270;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton187;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton188;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton189;
    private C1.Win.C1Ribbon.RibbonButton btnScale10;
    private C1.Win.C1Ribbon.RibbonButton btnScale25;
    private C1.Win.C1Ribbon.RibbonButton btnScale50;
    private C1.Win.C1Ribbon.RibbonButton btnScale75;
    private C1.Win.C1Ribbon.RibbonButton btnScale150;
    private C1.Win.C1Ribbon.RibbonButton btnScale200;
    private C1.Win.C1Ribbon.RibbonButton btnScale300;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton197;
    private C1.Win.C1Ribbon.RibbonGallery rcbCellType;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbdwGeneral;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbdwText;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbdwNumber;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbdwPercent;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbdwCurrency;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbdwRegularExpression;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbdwBarCode;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbdwButton;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbdwCheckBox;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbdwMultiOption;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbdwComboBox;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbdwMultiColumnComboBox;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbdwListBox;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbdwRichText;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbdwDateTime;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbdwPicture;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbdwColorPicker;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbdwProgress;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbdwSlider;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbdwMask;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbdwHyperLink;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbdwGcDate;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbdwGcTextBox;
    private C1.Win.C1Ribbon.RibbonGallery rbMargins;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbMarginNormal;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbMarginNarrow;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbMarginWide;
    private C1.Win.C1Ribbon.RibbonButton btnCustomMargins;
    private C1.Win.C1Ribbon.RibbonGallery rbCFIconSet;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCF3ArrowsColored;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCF3ArrowsGray;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCF3Triangles;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCF4ArrowsGray;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCF4ArrowsColored;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCF5ArrowsGray;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCF5Colored;
    private C1.Win.C1Ribbon.RibbonGalleryItem ribbonGalleryItem113;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCF3TrafficLightsUnRimmmed;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCF3TrafficLightsRimmmed;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCF3Sign;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCF4TracfficLightsUnRimmed;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCFRedToBlack;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCF3SymbolsCircled;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCF3SymbolsUnCircled;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCF3Flags;
    private C1.Win.C1Ribbon.RibbonGalleryItem ribbonGalleryItem122;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCFStars;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCF4Ratings;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCFQuarsters;
    private C1.Win.C1Ribbon.RibbonGalleryItem ribbonGalleryItem126;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCF5Ratings;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCF5Boxs;
    private C1.Win.C1Ribbon.RibbonGallery rbCFDataBars;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCFGradientBlue;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCFGradientGreen;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCFGradientRed;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCFGradientOrange;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCFGradientLightBlue;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCFGradientPurple;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCFSolidBlue;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCFSolidGreen;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCFSolidRed;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCFSolidOrange;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCFSolidLightBlue;
    private C1.Win.C1Ribbon.RibbonGalleryItem rbCFSolidPurple;
    private C1.Win.C1Ribbon.RibbonButton rbCFDataBarMoreRules;
    private C1.Win.C1Ribbon.RibbonButton rbShapeThickNone;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton159;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton160;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton161;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton162;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton163;
    private C1.Win.C1Ribbon.RibbonButton RibbonButton164;
    private C1.Win.C1Ribbon.RibbonButton rbShapeThickCustom;
    private C1.Win.C1Ribbon.RibbonButton rbShapeOutlineSolid;
    private C1.Win.C1Ribbon.RibbonButton rbShapeOutlineDash;
    private C1.Win.C1Ribbon.RibbonButton rbShapeOutlineDot;
    private C1.Win.C1Ribbon.RibbonButton rbShapeOutlineDashDot;
    private C1.Win.C1Ribbon.RibbonButton rbShapeOutlineDashDotDot;
    private C1.Win.C1Ribbon.RibbonGallery rbShapes;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape11;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape12;
    private C1.Win.C1Ribbon.RibbonGalleryItem ribbonGalleryItem143;
    private C1.Win.C1Ribbon.RibbonGalleryItem ribbonGalleryItem144;
    private C1.Win.C1Ribbon.RibbonGalleryItem ribbonGalleryItem145;
    private C1.Win.C1Ribbon.RibbonGalleryItem ribbonGalleryItem146;
    private C1.Win.C1Ribbon.RibbonGalleryItem ribbonGalleryItem147;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape21;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape22;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape23;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape24;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape25;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape26;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape27;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape31;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape32;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape33;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape34;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape35;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape37;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape41;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape42;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape43;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape44;
    private C1.Win.C1Ribbon.RibbonGalleryItem ribbonGalleryItem165;
    private C1.Win.C1Ribbon.RibbonGalleryItem ribbonGalleryItem166;
    private C1.Win.C1Ribbon.RibbonGalleryItem ribbonGalleryItem167;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape51;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape52;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape53;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape54;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape55;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape56;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape57;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape61;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape62;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape63;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape64;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape65;
    private C1.Win.C1Ribbon.RibbonGalleryItem Shape36;
    private C1.Win.C1Ribbon.RibbonButton rbShapeShadowNone;
    private C1.Win.C1Ribbon.RibbonButton rbShapeShadowRight;
    private C1.Win.C1Ribbon.RibbonButton rbShapeShadowBottomRight;
    private C1.Win.C1Ribbon.RibbonButton rbShapeShadowBottom;
    private C1.Win.C1Ribbon.RibbonButton rbShapeShadowBottomLeft;
    private C1.Win.C1Ribbon.RibbonButton rbShapeShadowLeft;
    private C1.Win.C1Ribbon.RibbonButton rbShapeShadowTopLeft;
    private C1.Win.C1Ribbon.RibbonButton rbShapeShadowTop;
    private C1.Win.C1Ribbon.RibbonButton rbShapeShadowTopRight;
    private C1.Win.C1Ribbon.RibbonButton rbShapeShadowCustom;
    private C1.Win.C1Ribbon.RibbonGalleryItem ItemWhiteGreen;
    private C1.Win.C1Ribbon.RibbonGalleryItem ItemGreenYellowScale;
    private C1.Win.C1Ribbon.RibbonGalleryItem ItemYellowGreenScale;
    private C1.Win.C1Ribbon.RibbonButton ribbonButton19;
    private C1.Win.C1Ribbon.RibbonButton ribbonButton20;
    private C1.Win.C1Ribbon.RibbonButton ribbonButton21;
    private C1.Win.C1Ribbon.RibbonButton ribbonButton22;
    private C1.Win.C1Ribbon.RibbonButton ribbonButton23;
    private C1.Win.C1Ribbon.RibbonButton ribbonButton24;
    private C1.Win.C1Ribbon.RibbonButton rbChartMoveHV;
    private C1.Win.C1Ribbon.RibbonButton rbChartMoveH;
    private C1.Win.C1Ribbon.RibbonButton rbChartMoveV;
    private C1.Win.C1Ribbon.RibbonButton rbChartMoveNone;
    private C1.Win.C1Ribbon.RibbonButton rbChartSizeBoth;
    private C1.Win.C1Ribbon.RibbonButton rbChartSizeWidth;
    private C1.Win.C1Ribbon.RibbonButton rbChartSizeHeight;
    private C1.Win.C1Ribbon.RibbonButton rbChartSizeNone;
    private C1.Win.C1Ribbon.RibbonToggleButton rbVerticalJustify;
    private C1.Win.C1Ribbon.RibbonToggleButton rbVerticalDistributed;
    private C1.Win.C1Ribbon.RibbonToggleButton rbHorizontalJustify;
    private C1.Win.C1Ribbon.RibbonToggleButton rbHorizontalDistributed;
    private C1.Win.C1Ribbon.RibbonToggleButton rbMerge;
    private C1.Win.C1Ribbon.RibbonMenu rbCellBorder;
    private C1.Win.C1Ribbon.RibbonButton rbBottomBorder;
    private C1.Win.C1Ribbon.RibbonButton rbTopBorder;
    private C1.Win.C1Ribbon.RibbonButton rbLeftBorder;
    private C1.Win.C1Ribbon.RibbonButton rbRightBorder;
    private C1.Win.C1Ribbon.RibbonSeparator ribbonSeparator11;
    private C1.Win.C1Ribbon.RibbonButton rbNoBorder;
    private C1.Win.C1Ribbon.RibbonButton rbAllBorder;
    private C1.Win.C1Ribbon.RibbonButton rbOutsideBorder;
    private C1.Win.C1Ribbon.RibbonButton rbThickBoxBorder;
    private C1.Win.C1Ribbon.RibbonSeparator ribbonSeparator17;
    private C1.Win.C1Ribbon.RibbonButton rbBottomDoubleBorder;
    private C1.Win.C1Ribbon.RibbonButton rbThickBottomBorder;
    private C1.Win.C1Ribbon.RibbonButton rbTopAndBottomBorder;
    private C1.Win.C1Ribbon.RibbonButton rbTopAndThickBottomBorder;
    private C1.Win.C1Ribbon.RibbonButton rbTopAndDoubleBottomBorder;
    private C1.Win.C1Ribbon.RibbonSeparator ribbonSeparator18;
    private C1.Win.C1Ribbon.RibbonButton rbMoreBorder;
    private C1.Win.C1Ribbon.RibbonTab rtSparkline;
    private C1.Win.C1Ribbon.RibbonGroup rpSparklineEditData;
    private C1.Win.C1Ribbon.RibbonSplitButton rbSparklineEditData;
    private C1.Win.C1Ribbon.RibbonButton rbSparklineEditDataGroup;
    private C1.Win.C1Ribbon.RibbonButton rbSparklineEditDataSingle;
    private C1.Win.C1Ribbon.RibbonSeparator ribbonSeparator19;
    private C1.Win.C1Ribbon.RibbonButton rbSparklineEditDataHidden;
    private C1.Win.C1Ribbon.RibbonButton rbSparklineEditDataSwitch;
    private C1.Win.C1Ribbon.RibbonGroup rpSparklineType;
    private C1.Win.C1Ribbon.RibbonToggleButton rbSparklineTypeLine;
    private C1.Win.C1Ribbon.RibbonToggleButton rbSparklineTypeColumn;
    private C1.Win.C1Ribbon.RibbonToggleButton rbSparklineTypeWinLoss;
    private C1.Win.C1Ribbon.RibbonGroup rpSparklineShow;
    private C1.Win.C1Ribbon.RibbonCheckBox rbSparklineHighPoint;
    private C1.Win.C1Ribbon.RibbonCheckBox rbSparklineLowPoint;
    private C1.Win.C1Ribbon.RibbonCheckBox rbSparklineMarkerColorNegativePoint;
    private C1.Win.C1Ribbon.RibbonCheckBox rbSparklineFirstPoint;
    private C1.Win.C1Ribbon.RibbonCheckBox rbSparklineLastPoint;
    private C1.Win.C1Ribbon.RibbonCheckBox rbSparklineMarkerColorMarker;
    private C1.Win.C1Ribbon.RibbonGroup rpSparklineStyle;
    private C1.Win.C1Ribbon.RibbonColorPicker rbSparklineColor;
    private C1.Win.C1Ribbon.RibbonButton rbSparklineWeight;
    private C1.Win.C1Ribbon.RibbonGroup rpSparklineGroup;
    private C1.Win.C1Ribbon.RibbonSplitButton rbSparklineAxis;
    private C1.Win.C1Ribbon.RibbonLabel rbSparklineAxisSubCaption1;
    private C1.Win.C1Ribbon.RibbonToggleButton rbSparklineAxisSub1;
    private C1.Win.C1Ribbon.RibbonToggleButton rbSparklineAxisSub2;
    private C1.Win.C1Ribbon.RibbonToggleButton rbSparklineAxisSub3;
    private C1.Win.C1Ribbon.RibbonToggleButton rbSparklineAxisSub4;
    private C1.Win.C1Ribbon.RibbonLabel rbSparklineAxisSubCaption2;
    private C1.Win.C1Ribbon.RibbonToggleButton rbSparklineAxisSub5;
    private C1.Win.C1Ribbon.RibbonToggleButton rbSparklineAxisSub6;
    private C1.Win.C1Ribbon.RibbonToggleButton rbSparklineAxisSub7;
    private C1.Win.C1Ribbon.RibbonLabel rbSparklineAxisSubCaption3;
    private C1.Win.C1Ribbon.RibbonToggleButton rbSparklineAxisSub8;
    private C1.Win.C1Ribbon.RibbonToggleButton rbSparklineAxisSub9;
    private C1.Win.C1Ribbon.RibbonToggleButton rbSparklineAxisSub10;
    private C1.Win.C1Ribbon.RibbonButton rbSparklineGroup;
    private C1.Win.C1Ribbon.RibbonButton rbSparklineUngroup;
    private C1.Win.C1Ribbon.RibbonMenu rbSparklineClear;
    private C1.Win.C1Ribbon.RibbonButton rbSparklineClearSingle;
    private C1.Win.C1Ribbon.RibbonButton rbSparklineClearGroup;
    private C1.Win.C1Ribbon.RibbonMenu rbSparklineMarkerColor;
    private C1.Win.C1Ribbon.RibbonColorPicker rbSparklineNegativePoints;
    private C1.Win.C1Ribbon.RibbonColorPicker rbSparklineMarkers;
    private C1.Win.C1Ribbon.RibbonColorPicker rbSparklineMarkerColorHighPoint;
    private C1.Win.C1Ribbon.RibbonColorPicker rbSparklineMarkerColorLowPoint;
    private C1.Win.C1Ribbon.RibbonColorPicker rbSparklineMarkerColorFirstPoint;
    private C1.Win.C1Ribbon.RibbonColorPicker rbSparklineMarkerColorLastPoint;
    private System.Windows.Forms.Panel FormulaPanel;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.Button btnFormulaSet;
    private System.Windows.Forms.Button btnFormulaCancel;
    private System.Windows.Forms.Button btnFormulaEdit;
    private C1.Win.C1Ribbon.RibbonSeparator ribbonSeparator20;
    private C1.Win.C1Ribbon.RibbonButton rbCFHighLightMoreRules;
    private C1.Win.C1Ribbon.RibbonButton rbCFIconSetsMoreRule;
    private C1.Win.C1Ribbon.RibbonButton btnSheetSkins1;


    private System.Windows.Forms.MenuItem menuContext_Celltypes;
    private System.Windows.Forms.MenuItem menuContext_Button;
    private System.Windows.Forms.MenuItem menuContext_Checkbox;
    private System.Windows.Forms.MenuItem menuContext_Combobox;
    private System.Windows.Forms.MenuItem menuContext_Currency;
    private System.Windows.Forms.MenuItem menuContext_DateTime;
    private System.Windows.Forms.MenuItem menuContext_Text;
    private System.Windows.Forms.MenuItem menuContext_Hyperlink;
    private System.Windows.Forms.MenuItem menuContext_Label;
    private System.Windows.Forms.MenuItem menuContext_Mask;
    private System.Windows.Forms.MenuItem menuContext_Number;
    private System.Windows.Forms.MenuItem menuContext_Percent;
    private System.Windows.Forms.MenuItem menuContext_MultiOption;
    private System.Windows.Forms.MenuItem menuContext_Progress;
    private System.Windows.Forms.MenuItem menuContext_Slider;
    private System.Windows.Forms.MenuItem menuContext_Image;
    private System.Windows.Forms.MenuItem menuContext_General;

    private System.Windows.Forms.MenuItem menuContext_GcTextBox;
    private System.Windows.Forms.MenuItem menuContext_GcDate;

    private System.Windows.Forms.MenuItem menuContext_Sep1;
    private System.Windows.Forms.MenuItem menuContext_Cut;
    private System.Windows.Forms.MenuItem menuContext_Copy;
    private System.Windows.Forms.MenuItem menuContext_Paste;
    private System.Windows.Forms.MenuItem menuContext_PasteAll;
    private System.Windows.Forms.MenuItem menuContext_PasteData;
    private System.Windows.Forms.MenuItem menuContext_PasteFormatting;
    private System.Windows.Forms.MenuItem menuContext_PasteFormulas;
    private System.Windows.Forms.MenuItem menuContext_PasteAsLink;
    private System.Windows.Forms.MenuItem menuContext_PasteAsString;
    private System.Windows.Forms.MenuItem menuContext_Sep2;
    private System.Windows.Forms.MenuItem menuContext_Clear;
    private System.Windows.Forms.MenuItem menuContext_SelectAll;
    private System.Windows.Forms.MenuItem menuContext_SelectAllSheet;
    private System.Windows.Forms.MenuItem menuContext_SelectAllCells;
    private System.Windows.Forms.MenuItem menuContext_SelectAllData;
    private System.Windows.Forms.MenuItem menuContext_Sep3;

    private System.Windows.Forms.MenuItem menuContext_Sparkline;
    private System.Windows.Forms.MenuItem menuContext_SparklineEditGroup;
    private System.Windows.Forms.MenuItem menuContext_SparklineEditSingle;
    private System.Windows.Forms.MenuItem menuContext_SparklineGroup;
    private System.Windows.Forms.MenuItem menuContext_SparklineUngroup;
    private System.Windows.Forms.MenuItem menuContext_SparklineClear;
    private System.Windows.Forms.MenuItem menuContext_SparklineClearGroup;
    private System.Windows.Forms.MenuItem menuContext_SparklineSwitch;

    private System.Windows.Forms.MenuItem menuContext_Span;
    private System.Windows.Forms.MenuItem menuContext_Lock;
    private System.Windows.Forms.MenuItem menuContext_Borders;

    protected System.Windows.Forms.ContextMenu PropGridContextMenu;
    protected System.Windows.Forms.MenuItem PGContextReset;

    private System.Windows.Forms.MenuItem menuContext_Insert;
    private System.Windows.Forms.MenuItem menuContext_Delete;
    private System.Windows.Forms.MenuItem menuContext_Headers;
    private System.Windows.Forms.MenuItem menuContext_ColumnWidth;
    private System.Windows.Forms.MenuItem menuContext_RowHeight;
    private System.Windows.Forms.MenuItem menuContext_Hide;
    private System.Windows.Forms.MenuItem menuContext_UnHide;
    private System.Windows.Forms.MenuItem menuContext_UnHideSelection;
    private System.Windows.Forms.MenuItem menuContext_UnHideAllHidden;
    private System.Windows.Forms.MenuItem menuContext_UnHideSpecific;
    private System.Windows.Forms.MenuItem menuContext_ClearCellType;
    private System.Windows.Forms.MenuItem menuContext_Sep10;
    private System.Windows.Forms.MenuItem menuContext_SheetSettings;
    private System.Windows.Forms.MenuItem menuContext_SheetSkinDesigner;
    private System.Windows.Forms.MenuItem menuContext_SpreadSkinDesigner;
    private System.Windows.Forms.MenuItem menuContext_SheetPrintOptions;
    private System.Windows.Forms.MenuItem menuContext_ListBox;
    private System.Windows.Forms.MenuItem menuContext_MultiColumnComboBox;
    private System.Windows.Forms.MenuItem menuContext_Barcode;
    private System.Windows.Forms.MenuItem menuContext_ColorPicker;
    //private System.Windows.Forms.TextBox RenameTextBox;
    private System.Windows.Forms.MenuItem menuContext_AutoFit;
    private System.Windows.Forms.ContextMenu shapeContextMenu;
    private System.Windows.Forms.MenuItem shapeContextDelete;
    private System.Windows.Forms.MenuItem shapeContextProperties;
    private System.Windows.Forms.MenuItem shapeContextFormula;
    private System.Windows.Forms.MenuItem shapeContextSep1;
    private System.Windows.Forms.MenuItem shapeContextLocked;
    private System.Windows.Forms.MenuItem shapeContextSep2;
    private System.Windows.Forms.MenuItem shapeContextCut;
    private System.Windows.Forms.MenuItem shapeContextCopy;
    private System.Windows.Forms.MenuItem shapeContextPaste;
    private System.Windows.Forms.MenuItem menuContext_RichText;
    private System.Windows.Forms.MenuItem menuContext_RegEx;
    private System.Windows.Forms.MenuItem menuContext_PasteShape;
    private System.Windows.Forms.MenuItem menuContext_PasteChart;
    private System.Windows.Forms.StatusBarPanel sbpZoom;

    private System.Windows.Forms.ContextMenu noteContextMenu;
    //private System.Windows.Forms.MenuItem noteContextProperties;
    private System.Windows.Forms.MenuItem menuContext_EditNote;
    private System.Windows.Forms.MenuItem menuContext_DeleteNote;
    private System.Windows.Forms.MenuItem menuContext_Sep4;
    private System.Windows.Forms.MenuItem menuContext_DeleteSheet;
    private System.Windows.Forms.ContextMenu contextMenu1;
//tableview
    private System.Windows.Forms.ContextMenu tableContextMenu = new System.Windows.Forms.ContextMenu();
    private System.Windows.Forms.MenuItem menuContext_Table = new System.Windows.Forms.MenuItem();
    private FarPoint.Win.Ribbon.CustomMenuItem menuContext_TableTotalsRow = new FarPoint.Win.Ribbon.CustomMenuItem();
    private System.Windows.Forms.MenuItem menuContext_TableConvertToRange = new System.Windows.Forms.MenuItem();
    //Table insert context menu element
    private System.Windows.Forms.MenuItem menuContext_TableInsert = new System.Windows.Forms.MenuItem();
    private FarPoint.Win.Ribbon.CustomMenuItem menuContext_TableInsertColumnToLeft = new FarPoint.Win.Ribbon.CustomMenuItem();
    private FarPoint.Win.Ribbon.CustomMenuItem menuContext_TableInsertColumnToRight = new FarPoint.Win.Ribbon.CustomMenuItem();
    private FarPoint.Win.Ribbon.CustomMenuItem menuContext_TableInsertRowToAbove = new FarPoint.Win.Ribbon.CustomMenuItem();
    private FarPoint.Win.Ribbon.CustomMenuItem menuContext_TableInsertRowToBelow = new FarPoint.Win.Ribbon.CustomMenuItem();
    //Table Delete context menu element
    private System.Windows.Forms.MenuItem menuContext_TableDelete = new System.Windows.Forms.MenuItem();
    private FarPoint.Win.Ribbon.CustomMenuItem menuContext_TableDeleteColumn = new FarPoint.Win.Ribbon.CustomMenuItem();
    private FarPoint.Win.Ribbon.CustomMenuItem menuContext_TableDeleteRow = new FarPoint.Win.Ribbon.CustomMenuItem();
    //Table selection context menu element
    private System.Windows.Forms.MenuItem menuContext_TableSelect = new System.Windows.Forms.MenuItem();
    private System.Windows.Forms.MenuItem menuContext_TableSelectColumnData = new System.Windows.Forms.MenuItem();
    private System.Windows.Forms.MenuItem menuContext_TableSelectEntryColumn = new System.Windows.Forms.MenuItem();
    private System.Windows.Forms.MenuItem menuContext_TableSelectRow = new System.Windows.Forms.MenuItem();
    private System.Windows.Forms.MenuItem menuContext_Sep_Table = new System.Windows.Forms.MenuItem();
    private System.Windows.Forms.MenuItem menuContext_Sep_Table1 = new System.Windows.Forms.MenuItem();
//tableview
    private System.Windows.Forms.ContextMenu sheetTabContextMenu;
    private System.Windows.Forms.TextBox RenameTextBox;

    private C1.Win.C1Ribbon.RibbonButton tbnUndo;
    private C1.Win.C1Ribbon.RibbonButton tbnRedo;
    private C1.Win.C1Ribbon.RibbonButton tbnNew;
    private C1.Win.C1Ribbon.RibbonButton tbnOpen;
    private C1.Win.C1Ribbon.RibbonButton tbnSave;
    private C1.Win.C1Ribbon.RibbonSeparator ribbonSeparator21;
    private C1.Win.C1Ribbon.RibbonButton tbnPrint;
    private C1.Win.C1Ribbon.RibbonSeparator ribbonSeparator22;
    private C1.Win.C1Ribbon.C1StatusBar c1StatusBar1;
    private C1.Win.C1Ribbon.RibbonGroup rpTables;
    private C1.Win.C1Ribbon.RibbonButton rbTable;
    private C1.Win.C1Ribbon.RibbonTab rtTable;
    private C1.Win.C1Ribbon.RibbonGroup rgProperties;
    private C1.Win.C1Ribbon.RibbonLabel rlTableName;
    private C1.Win.C1Ribbon.RibbonTextBox rtbTableName;
    private C1.Win.C1Ribbon.RibbonButton rbTableResize;
    private C1.Win.C1Ribbon.RibbonGroup rgTools;
    private C1.Win.C1Ribbon.RibbonButton rbTableConvertToRange;
    private C1.Win.C1Ribbon.RibbonGroup rgTableStyleOptions;
    private C1.Win.C1Ribbon.RibbonGroup rgTableStyles;
    private C1.Win.C1Ribbon.RibbonCheckBox rcTableHeaderRow;
    private C1.Win.C1Ribbon.RibbonCheckBox rcTableTotalRow;
    private C1.Win.C1Ribbon.RibbonCheckBox rcTableBandedRow;
    private C1.Win.C1Ribbon.RibbonCheckBox rcTableFirstColumn;
    private C1.Win.C1Ribbon.RibbonCheckBox rcTableLastColumn;
    private C1.Win.C1Ribbon.RibbonCheckBox rcTableBandedColumn;
    private C1.Win.C1Ribbon.RibbonCheckBox rcTableFilterButton;
    private C1.Win.C1Ribbon.RibbonGallery rglTableStyles;
    private C1.Win.C1Ribbon.RibbonButton rbTableStyleClear;
    private FarPoint.Win.Spread.NameBox nameBox1;
    private C1.Win.C1Ribbon.RibbonButton rbMarginsTemp;
    private C1.Win.C1Ribbon.RibbonTab ribbonTab1;
    private C1.Win.C1Ribbon.RibbonGroup ribbonGroup1;
    private C1.Win.C1Ribbon.RibbonButton ribbonButton1List;
    private C1.Win.C1Ribbon.RibbonButton ribbonButton2SaveCloud;
  }
}
